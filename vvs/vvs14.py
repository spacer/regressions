import z3


def bcp (x):
    if not z3.is_and(x): return x
    sub = list()
    ztrue = z3.BoolVal(True)
    zfalse = z3.BoolVal(False)

    for k in x.children():
        neg = z3.is_not (k)
        if neg: k = k.arg(0)
        if not z3.is_const (k):
            continue

        if neg:
            val = zfalse
        else:
            val = ztrue
        sub.append ((k, val))

    print sub
    if len(sub) > 1:
        x = z3.substitute (x, sub)
        x = z3.simplify (x)
    return x
    

x = z3.parse_smt2_file ('vvs14.smt2')

num_args = x.num_args() + 1
print x.num_args()
while x.num_args() < num_args:
    num_args = x.num_args()
    x = bcp(x)
    print x.num_args()


solver = z3.Solver()
solver.assert_exprs (x.children())

with open('vvs14.min.smt2', 'w') as f:
    f.write(solver.to_smt2())

# n = x.num_args() - 1
# while n > 0:
#     print x.arg(n)
#     n = n - 1

