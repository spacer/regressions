import z3
import sys
import argparse

def bcp (x):
    if not z3.is_and(x): return x
    sub = list()
    ztrue = z3.BoolVal(True)
    zfalse = z3.BoolVal(False)

    for k in x.children():
        neg = z3.is_not (k)
        if neg: k = k.arg(0)
        if not z3.is_const (k):
            continue

        if neg:
            val = zfalse
        else:
            val = ztrue
        sub.append ((k, val))

    print sub

    # leave one bcp variable to play with sat-under-assumption
    if len(sub) > 1:
        x = z3.substitute (x, sub)
        x = z3.simplify (x)
    return x
    
def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument('in_file', help='SMT2 file')
    p.add_argument('-o', dest='out_file', help='Output file', default='out.smt2')
    
    return p.parse_args()

def main():
    args = parse_args()

    x = z3.parse_smt2_file (args.in_file)

    num_args = x.num_args() + 1
    print 'Initial size:', x.num_args()
    while x.num_args() < num_args:
        num_args = x.num_args()
        x = bcp(x)
        print 'After one round of bcp', x.num_args()

    solver = z3.Solver()
    solver.assert_exprs (x.children())

    with open(args.out_file, 'w') as f:
        f.write(solver.to_smt2())
        
    return 0

if __name__ == '__main__':
    sys.exit(main())
