; Statically linked libraries
; divid
(declare-var divid.num Real)
(declare-var divid.den Real)
(declare-var divid.res Real)
(declare-var divid.out Real)
(declare-var divid.abs_den Real)
(declare-var divid.abs_num Real)
(declare-var divid.abs_res Real)
(declare-rel divid_fun (Real Real Real Real))
; Stateless step rule with Assertions 
(rule (=> 
  (and 
  (and (= divid.out divid.res)
       (and (or (not (= (< divid.res 0.0) true))
               (= divid.abs_res (- divid.res)))
            (or (not (= (< divid.res 0.0) false))
               (= divid.abs_res divid.res))
       )
       (and (or (not (= (< divid.num 0.0) true))
               (= divid.abs_num (- divid.num)))
            (or (not (= (< divid.num 0.0) false))
               (= divid.abs_num divid.num))
       )
       (and (or (not (= (< divid.den 0.0) true))
               (= divid.abs_den (- divid.den)))
            (or (not (= (< divid.den 0.0) false))
               (= divid.abs_den divid.den))
       )
       )
 (and (= (= divid.res (- 1.0)) (= divid.num (- divid.den)))
      (= (= divid.res 1.0) (= divid.num divid.den))
      (= (<= divid.abs_res divid.abs_num) (or (>= divid.abs_den 1.0) (= divid.num 0.0)))
      (= (>= divid.abs_res divid.abs_num) (or (<= divid.abs_den 1.0) (= divid.num 0.0)))
      (= (< divid.res 0.0) (or (and (> divid.num 0.0) (< divid.den 0.0)) (and (< divid.num 0.0) (> divid.den 0.0))))
      (= (> divid.res 0.0) (or (and (> divid.num 0.0) (> divid.den 0.0)) (and (< divid.num 0.0) (< divid.den 0.0))))
      (= (= divid.num 0.0) (= divid.res 0.0))
      (= (= divid.res (- divid.num)) (or (= divid.den (- 1.0)) (= divid.num 0.0)))
      (= (= divid.res divid.num) (or (= divid.den 1.0) (= divid.num 0.0)))
      (not (= divid.den 0.0)) ))
(divid_fun divid.num divid.den divid.res divid.out)
))

; times
(declare-var times.x Real)
(declare-var times.y Real)
(declare-var times.z Real)
(declare-var times.out Real)
(declare-var times.abs_x Real)
(declare-var times.abs_y Real)
(declare-var times.abs_z Real)
(declare-rel times_fun (Real Real Real Real))
; Stateless step rule with Assertions 
(rule (=> 
  (and 
  (and (= times.out times.z)
       (and (or (not (= (< times.z 0.0) true))
               (= times.abs_z (- times.z)))
            (or (not (= (< times.z 0.0) false))
               (= times.abs_z times.z))
       )
       (and (or (not (= (< times.y 0.0) true))
               (= times.abs_y (- times.y)))
            (or (not (= (< times.y 0.0) false))
               (= times.abs_y times.y))
       )
       (and (or (not (= (< times.x 0.0) true))
               (= times.abs_x (- times.x)))
            (or (not (= (< times.x 0.0) false))
               (= times.abs_x times.x))
       )
       )
 (and (= (<= times.abs_z times.abs_x) (or (<= times.abs_y 1.0) (= times.x 0.0)))
      (= (<= times.abs_z times.abs_y) (or (<= times.abs_x 1.0) (= times.y 0.0)))
      (= (>= times.abs_z times.abs_x) (or (>= times.abs_y 1.0) (= times.x 0.0)))
      (= (>= times.abs_z times.abs_y) (or (>= times.abs_x 1.0) (= times.y 0.0)))
      (= (< times.z 0.0) (or (and (> times.x 0.0) (< times.y 0.0)) (and (< times.x 0.0) (> times.y 0.0))))
      (= (> times.z 0.0) (or (and (> times.x 0.0) (> times.y 0.0)) (and (< times.x 0.0) (< times.y 0.0))))
      (= (= times.z 0.0) (or (= times.x 0.0) (= times.y 0.0)))
      (= (= times.z times.x) (or (= times.y 1.0) (= times.x 0.0)))
      (= (= times.z times.y) (or (= times.x 1.0) (= times.y 0.0))) ))
(times_fun times.x times.y times.z times.out)
))

; LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.up_lim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.SigIn_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Lo_lim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Out1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforcelowlim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforceuplim_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation_fun (Real Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (and (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Lo_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.SigIn_1_1) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Lo_lim_1_1))
            (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Lo_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.SigIn_1_1) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.SigIn_1_1))
       )
       (and (or (not (= (<= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.up_lim_1_1) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforceuplim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforcelowlim_1_1))
            (or (not (= (<= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.up_lim_1_1) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforceuplim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.up_lim_1_1))
       )
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Out1_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Enforceuplim_1_1)
       )
  (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation_fun 
  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.up_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.SigIn_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Lo_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation.Out1_1_1)
))

; LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.In1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.x1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.x2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.y1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.y2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.t_out Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.d_out Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.Out1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.Divide_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0_fun (Real Real Real Real Real Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (divid_fun (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.In1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.x1_1_1)
                  (+ (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.x1_1_1) LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.x2_1_1)
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.d_out
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0_1)
       (times_fun LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0_1
                  (+ (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.y1_1_1) LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.y2_1_1)
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.t_out
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.Divide_1_1)
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.Out1_1_1 (+ LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.Divide_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.y1_1_1))
       )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0_fun 
  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.In1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.x1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.x2_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.y1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.y2_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.t_out LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.d_out LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0.Out1_1_1)
))

; LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.In1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.x1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.x2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.y1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.y2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.t_out Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.d_out Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.Out1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.Divide_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1_fun (Real Real Real Real Real Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (divid_fun (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.In1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.x1_1_1)
                  (+ (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.x1_1_1) LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.x2_1_1)
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.d_out
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1_1)
       (times_fun LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1_1
                  (+ (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.y1_1_1) LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.y2_1_1)
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.t_out
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.Divide_1_1)
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.Out1_1_1 (+ LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.Divide_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.y1_1_1))
       )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1_fun 
  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.In1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.x1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.x2_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.y1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.y2_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.t_out LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.d_out LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1.Out1_1_1)
))

; LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.In1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.x1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.x2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.y1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.y2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.t_out Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.d_out Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.Out1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.Divide_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2_fun (Real Real Real Real Real Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (divid_fun (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.In1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.x1_1_1)
                  (+ (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.x1_1_1) LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.x2_1_1)
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.d_out
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2_1)
       (times_fun LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2_1
                  (+ (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.y1_1_1) LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.y2_1_1)
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.t_out
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.Divide_1_1)
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.Out1_1_1 (+ LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.Divide_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.y1_1_1))
       )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2_fun 
  LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.In1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.x1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.x2_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.y1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.y2_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.t_out LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.d_out LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2.Out1_1_1)
))

; LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.SigIn_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Out1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforcelowlim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforceuplim_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1_fun (Real Real))
; Stateless step rule 
(rule (=> 
  (and (and (or (not (= (>= 0.0001000000 LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.SigIn_1_1) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforcelowlim_1_1 0.0001000000))
            (or (not (= (>= 0.0001000000 LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.SigIn_1_1) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.SigIn_1_1))
       )
       (and (or (not (= (<= LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforcelowlim_1_1 1000.0000000000) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforceuplim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforcelowlim_1_1))
            (or (not (= (<= LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforcelowlim_1_1 1000.0000000000) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforceuplim_1_1 1000.0000000000))
       )
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Out1_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Enforceuplim_1_1)
       )
  (LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1_fun LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.SigIn_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1.Out1_1_1)
))

; LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.up_lim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.SigIn_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Lo_lim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Out1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforcelowlim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforceuplim_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation_fun (Real Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (and (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Lo_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.SigIn_1_1) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Lo_lim_1_1))
            (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Lo_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.SigIn_1_1) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.SigIn_1_1))
       )
       (and (or (not (= (<= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.up_lim_1_1) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforceuplim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforcelowlim_1_1))
            (or (not (= (<= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforcelowlim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.up_lim_1_1) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforceuplim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.up_lim_1_1))
       )
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Out1_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Enforceuplim_1_1)
       )
  (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation_fun 
  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.up_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.SigIn_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Lo_lim_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation.Out1_1_1)
))

; LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ratelim_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.input_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ICtrig_1_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.IC_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.output_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.VariableLimitSaturation_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_2 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Sum3_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_3_1_1 Bool)
(declare-rel LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_reset (Bool Real Real Bool Bool Real Real Bool))
(declare-rel LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_step (Real Real Bool Real Real Bool Real Real Bool Bool Real Real Bool))

(rule (=> 
  (and 
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c)
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c)
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c)
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m true)
  )
  (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_reset LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                                                   LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                                                   LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                                                   LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                                                   LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                                                   LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                                                   LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                                                   LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c)
       (and (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 (ite LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m true false))
            (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x false))
       (and (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_2_1_1 0.0000000000))
            (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_2_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c))
       )
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.output_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_2_1_1)
       (and (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_3_1_1 false))
            (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_3_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_3_1_1 true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_1 1.0))
            (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_3_1_1 false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_1 0.0))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ICtrig_1_1 true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_2 1.0))
            (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ICtrig_1_1 false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_2 0.0))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_1_1_1 1.0000000000))
            (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_1_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c))
       )
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Sum3_1_1 (+ (- LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_2_1_1) LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.IC_1_1))
       (and (or (not (= (and (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_2 0.0000000000) (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_1 0.0000000000))) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product2_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Sum3_1_1))
            (or (not (= (and (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_2 0.0000000000) (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_1 0.0000000000))) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product2_1_1 0.0))
       )
       (and (or (not (= (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_2 0.0000000000)) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product1_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Sum3_1_1))
            (or (not (= (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_2 0.0000000000)) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product1_1_1 0.0))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.IC_1_1))
            (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_3 false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product_1_1 0.0))
       )
       (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_VariableLimitSaturation_fun 
       LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ratelim_1_1
       (* 20.0000000000 (- LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.input_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_2_1_1))
       (* (- 1.0000000000) LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ratelim_1_1)
       LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.VariableLimitSaturation_1_1)
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x 0.0000000000)
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x (+ (* 1.0000000000 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.VariableLimitSaturation_1_1) (+ (+ LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_delay_sharp_2_1_1 (+ LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product1_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product2_1_1)) LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.integrator_reset_Product_1_1)))
       (= LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ICtrig_1_1)
       )
  (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_step LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ratelim_1_1
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.input_1_1
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ICtrig_1_1
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.IC_1_1
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.output_1_1
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
                                                                  LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x)
))

; divid_bounded_num
(declare-var divid_bounded_num.num Real)
(declare-var divid_bounded_num.den Real)
(declare-var divid_bounded_num.res Real)
(declare-var divid_bounded_num.l_bound Real)
(declare-var divid_bounded_num.u_bound Real)
(declare-var divid_bounded_num.out Real)
(declare-var divid_bounded_num.abs_den Real)
(declare-var divid_bounded_num.abs_num Real)
(declare-var divid_bounded_num.abs_res Real)
(declare-rel divid_bounded_num_fun (Real Real Real Real Real Real))
; Stateless step rule with Assertions 
(rule (=> 
  (and 
  (and (= divid_bounded_num.out divid_bounded_num.res)
       (and (or (not (= (< divid_bounded_num.res 0.0) true))
               (= divid_bounded_num.abs_res (- divid_bounded_num.res)))
            (or (not (= (< divid_bounded_num.res 0.0) false))
               (= divid_bounded_num.abs_res divid_bounded_num.res))
       )
       (and (or (not (= (< divid_bounded_num.num 0.0) true))
               (= divid_bounded_num.abs_num (- divid_bounded_num.num)))
            (or (not (= (< divid_bounded_num.num 0.0) false))
               (= divid_bounded_num.abs_num divid_bounded_num.num))
       )
       (and (or (not (= (< divid_bounded_num.den 0.0) true))
               (= divid_bounded_num.abs_den (- divid_bounded_num.den)))
            (or (not (= (< divid_bounded_num.den 0.0) false))
               (= divid_bounded_num.abs_den divid_bounded_num.den))
       )
       )
 (and (=> (and (and (< divid_bounded_num.num 0.0) (> divid_bounded_num.u_bound 0.0)) (< divid_bounded_num.l_bound 0.0)) (or (<= divid_bounded_num.res (div divid_bounded_num.num divid_bounded_num.u_bound)) (<= (div divid_bounded_num.num divid_bounded_num.l_bound) divid_bounded_num.res)))
      (=> (and (and (>= divid_bounded_num.num 0.0) (> divid_bounded_num.u_bound 0.0)) (< divid_bounded_num.l_bound 0.0)) (or (<= divid_bounded_num.res (div divid_bounded_num.num divid_bounded_num.l_bound)) (<= (div divid_bounded_num.num divid_bounded_num.u_bound) divid_bounded_num.res)))
      (=> (and (< divid_bounded_num.num 0.0) (< divid_bounded_num.u_bound 0.0)) (and (<= (div divid_bounded_num.num divid_bounded_num.l_bound) divid_bounded_num.res) (<= divid_bounded_num.res (div divid_bounded_num.num divid_bounded_num.u_bound))))
      (=> (and (< divid_bounded_num.num 0.0) (> divid_bounded_num.l_bound 0.0)) (and (<= (div divid_bounded_num.num divid_bounded_num.l_bound) divid_bounded_num.res) (<= divid_bounded_num.res (div divid_bounded_num.num divid_bounded_num.u_bound))))
      (=> (and (>= divid_bounded_num.num 0.0) (< divid_bounded_num.u_bound 0.0)) (and (<= (div divid_bounded_num.num divid_bounded_num.u_bound) divid_bounded_num.res) (<= divid_bounded_num.res (div divid_bounded_num.num divid_bounded_num.l_bound))))
      (=> (and (>= divid_bounded_num.num 0.0) (> divid_bounded_num.l_bound 0.0)) (and (<= (div divid_bounded_num.num divid_bounded_num.u_bound) divid_bounded_num.res) (<= divid_bounded_num.res (div divid_bounded_num.num divid_bounded_num.l_bound))))
      (= (= divid_bounded_num.res (- 1.0)) (= divid_bounded_num.num (- divid_bounded_num.den)))
      (= (= divid_bounded_num.res 1.0) (= divid_bounded_num.num divid_bounded_num.den))
      (= (<= divid_bounded_num.abs_res divid_bounded_num.abs_num) (or (>= divid_bounded_num.abs_den 1.0) (= divid_bounded_num.num 0.0)))
      (= (>= divid_bounded_num.abs_res divid_bounded_num.abs_num) (or (<= divid_bounded_num.abs_den 1.0) (= divid_bounded_num.num 0.0)))
      (= (< divid_bounded_num.res 0.0) (or (and (> divid_bounded_num.num 0.0) (< divid_bounded_num.den 0.0)) (and (< divid_bounded_num.num 0.0) (> divid_bounded_num.den 0.0))))
      (= (> divid_bounded_num.res 0.0) (or (and (> divid_bounded_num.num 0.0) (> divid_bounded_num.den 0.0)) (and (< divid_bounded_num.num 0.0) (< divid_bounded_num.den 0.0))))
      (= (= divid_bounded_num.num 0.0) (= divid_bounded_num.res 0.0))
      (= (= divid_bounded_num.res (- divid_bounded_num.num)) (or (= divid_bounded_num.den (- 1.0)) (= divid_bounded_num.num 0.0)))
      (= (= divid_bounded_num.res divid_bounded_num.num) (or (= divid_bounded_num.den 1.0) (= divid_bounded_num.num 0.0)))
      (<= divid_bounded_num.den divid_bounded_num.u_bound)
      (<= divid_bounded_num.l_bound divid_bounded_num.den)
      (<= divid_bounded_num.l_bound divid_bounded_num.u_bound)
      (not (= divid_bounded_num.u_bound 0.0))
      (not (= divid_bounded_num.l_bound 0.0))
      (not (= divid_bounded_num.den 0.0)) ))
(divid_bounded_num_fun divid_bounded_num.num divid_bounded_num.den divid_bounded_num.res divid_bounded_num.l_bound divid_bounded_num.u_bound divid_bounded_num.out)
))

; LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.up_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.u_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.lo_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.y1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation_fun (Real Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (and (or (not (= (< LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.u_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.lo_1_1) true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.lo_1_1))
            (or (not (= (< LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.u_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.lo_1_1) false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.u_1_1))
       )
       (and (or (not (= (> LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.u_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.up_1_1) true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch2_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.up_1_1))
            (or (not (= (> LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.u_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.up_1_1) false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch2_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch_1_1))
       )
       (= LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.y1_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.Switch2_1_1)
       )
  (LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation_fun LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.up_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.u_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.lo_1_1 LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation.y1_1_1)
))

; LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.f_lpar_x_rpar__1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.reset_level_1_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.x0_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.F_lpar_x_rpar__1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Sum3_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_2 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_3_1_1 Bool)
(declare-rel LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_reset (Bool Real Real Bool Bool Real Real Bool))
(declare-rel LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_step (Real Bool Real Real Bool Real Real Bool Bool Real Real Bool))

(rule (=> 
  (and 
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c)
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c)
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c)
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m true)
  )
  (LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_reset LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                                             LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                                             LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                                             LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                                             LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                                             LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                                             LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                                             LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c)
       (and (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 (ite LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m true false))
            (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x false))
       (and (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_3_1_1 false))
            (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_3_1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_3_1_1 true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_1 1.0))
            (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_3_1_1 false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_1 0.0))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.reset_level_1_1 true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_2 1.0))
            (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.reset_level_1_1 false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_2 0.0))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 false))
               (and (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_2_1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c)
                    (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_1_1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c)
                    ))
            (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 true))
               (and (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_2_1_1 0.0000000000)
                    (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_1_1_1 1.0000000000)
                    ))
       )
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x 0.0000000000)
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Sum3_1_1 (+ (- LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_2_1_1) LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.x0_1_1))
       (and (or (not (= (and (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_2 0.0000000000) (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_1 0.0000000000))) true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product2_1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Sum3_1_1))
            (or (not (= (and (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_2 0.0000000000) (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_1 0.0000000000))) false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product2_1_1 0.0))
       )
       (and (or (not (= (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_2 0.0000000000)) true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product1_1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Sum3_1_1))
            (or (not (= (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_2 0.0000000000)) false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product1_1_1 0.0))
       )
       (and (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 true))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product_1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.x0_1_1))
            (or (not (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_3 false))
               (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product_1_1 0.0))
       )
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x (+ (* 1.0000000000 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.f_lpar_x_rpar__1_1) (+ (+ LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_2_1_1 (+ LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product1_1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product2_1_1)) LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.Product_1_1)))
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.reset_level_1_1)
       (= LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.F_lpar_x_rpar__1_1 LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.delay_sharp_2_1_1)
       )
  (LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_step LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.f_lpar_x_rpar__1_1
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.reset_level_1_1
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.x0_1_1
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.F_lpar_x_rpar__1_1
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
                                                            LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x)
))

; LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.u_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.y_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.a_sharp_1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.delay_sharp_1_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_reset (Real Bool Real Bool))
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_step (Real Real Real Bool Real Bool))

(rule (=> 
  (and 
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c)
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m true)
  )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_reset LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                                             LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                                             LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                                             LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c)
       (and (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_1 (ite LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m true false))
            (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x false))
       (and (or (not (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_1 true))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.delay_sharp_1_1_1 0.0000000000))
            (or (not (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_1 false))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.delay_sharp_1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c))
       )
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.a_sharp_1_1_1 (* 0.0625000000 LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.delay_sharp_1_1_1))
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.y_1_1 (+ (* 1.0000000000 LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.u_1_1) (* (- 1.0000000000) LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.a_sharp_1_1_1)))
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x (+ (* 0.0200000000 (- LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.u_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.a_sharp_1_1_1)) LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.delay_sharp_1_1_1))
       )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_step LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.u_1_1
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.y_1_1
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x)
))

; LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out2 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out2 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out3 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out3 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.Out1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_3_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_0_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_2_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_fun (Real Real Real Real Real Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_2_fun 
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1
       350.0000000000
       500.0000000000
       0.3000000000
       0.3000000000
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out3
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out3
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_2_1_1)
       (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_1_fun 
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1
       120.0000000000
       350.0000000000
       1.0000000000
       0.3000000000
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out2
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out2
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_1_1_1)
       (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_interpolate1d_sharp_0_fun 
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1
       0.0000000000
       120.0000000000
       1.0000000000
       1.0000000000
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out1
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out1
       LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_0_1_1)
       (and (or (not (= (and (> LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 350.0000000000) (<= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 500.0000000000)) true))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_3_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_2_1_1))
            (or (not (= (and (> LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 350.0000000000) (<= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 500.0000000000)) false))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_3_1_1 0.0))
       )
       (and (or (not (= (and (> LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 120.0000000000) (<= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 350.0000000000)) true))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_2_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_1_1_1))
            (or (not (= (and (> LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 120.0000000000) (<= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 350.0000000000)) false))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_2_1_1 0.0))
       )
       (and (or (not (= (and (>= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 0.0000000000) (<= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 120.0000000000)) true))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.interpolate1d_sharp_0_1_1))
            (or (not (= (and (>= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 0.0000000000) (<= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 120.0000000000)) false))
               (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_1_1_1 0.0))
       )
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.Out1_1_1 (+ (+ LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_2_1_1) LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.branch_sharp_3_1_1))
       )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_fun LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.In1_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out1 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out2 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out2 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.t_out3 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.d_out3 LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d.Out1_1_1)
))

; abs_real
(declare-var abs_real.in Real)
(declare-var abs_real.out Real)
(declare-rel abs_real_fun (Real Real))
; Stateless step rule 
(rule (=> 
  (and (or (not (= (< abs_real.in 0.0) true))
          (= abs_real.out (- abs_real.in)))
       (or (not (= (< abs_real.in 0.0) false))
          (= abs_real.out abs_real.in))
  )
  (abs_real_fun abs_real.in abs_real.out)
))

; duration
(declare-var duration.in Bool)
(declare-var duration.out Int)
(declare-var duration.__duration_2_c Int)
(declare-var duration.__duration_4_c Bool)
(declare-var duration.ni_81._arrow._first_c Bool)
(declare-var duration.__duration_2_m Int)
(declare-var duration.__duration_4_m Bool)
(declare-var duration.ni_81._arrow._first_m Bool)
(declare-var duration.__duration_2_x Int)
(declare-var duration.__duration_4_x Bool)
(declare-var duration.ni_81._arrow._first_x Bool)
(declare-var duration.__duration_1 Bool)
(declare-var duration.__duration_3 Int)
(declare-var duration.always_true Bool)
(declare-rel duration_reset (Int Bool Bool Int Bool Bool))
(declare-rel duration_step (Bool Int Int Bool Bool Int Bool Bool))

(rule (=> 
  (and 
       (= duration.__duration_2_m duration.__duration_2_c)
       (= duration.__duration_4_m duration.__duration_4_c)
       (= duration.ni_81._arrow._first_m true)
  )
  (duration_reset duration.__duration_2_c
                  duration.__duration_4_c
                  duration.ni_81._arrow._first_c
                  duration.__duration_2_m
                  duration.__duration_4_m
                  duration.ni_81._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= duration.ni_81._arrow._first_m duration.ni_81._arrow._first_c)
       (and (= duration.__duration_1 (ite duration.ni_81._arrow._first_m true false))
            (= duration.ni_81._arrow._first_x false))
       (and (or (not (= duration.__duration_1 false))
               (and (= duration.always_true (and duration.in duration.__duration_4_c))
                    (= duration.__duration_3 (+ duration.__duration_2_c 1))
                    ))
            (or (not (= duration.__duration_1 true))
               (and (= duration.always_true duration.in)
                    (= duration.__duration_3 1)
                    ))
       )
       (and (or (not (= duration.always_true true))
               (= duration.out duration.__duration_3))
            (or (not (= duration.always_true false))
               (= duration.out 0))
       )
       (= duration.__duration_4_x duration.always_true)
       (= duration.__duration_2_x duration.out)
       )
  (duration_step duration.in
                 duration.out
                 duration.__duration_2_c
                 duration.__duration_4_c
                 duration.ni_81._arrow._first_c
                 duration.__duration_2_x
                 duration.__duration_4_x
                 duration.ni_81._arrow._first_x)
))

; fall
(declare-var fall.in Bool)
(declare-var fall.out Bool)
(declare-var fall.__fall_2_c Bool)
(declare-var fall.ni_80._arrow._first_c Bool)
(declare-var fall.__fall_2_m Bool)
(declare-var fall.ni_80._arrow._first_m Bool)
(declare-var fall.__fall_2_x Bool)
(declare-var fall.ni_80._arrow._first_x Bool)
(declare-var fall.__fall_1 Bool)
(declare-rel fall_reset (Bool Bool Bool Bool))
(declare-rel fall_step (Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       (= fall.__fall_2_m fall.__fall_2_c)
       (= fall.ni_80._arrow._first_m true)
  )
  (fall_reset fall.__fall_2_c
              fall.ni_80._arrow._first_c
              fall.__fall_2_m
              fall.ni_80._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= fall.ni_80._arrow._first_m fall.ni_80._arrow._first_c)(and (= fall.__fall_1 (ite fall.ni_80._arrow._first_m true false))
                                                                    (= fall.ni_80._arrow._first_x false))
       (and (or (not (= fall.__fall_1 true))
               (= fall.out false))
            (or (not (= fall.__fall_1 false))
               (= fall.out (and (not fall.in) fall.__fall_2_c)))
       )
       (= fall.__fall_2_x fall.in)
       )
  (fall_step fall.in
             fall.out
             fall.__fall_2_c
             fall.ni_80._arrow._first_c
             fall.__fall_2_x
             fall.ni_80._arrow._first_x)
))

; has_been_true_reset
(declare-var has_been_true_reset.in Bool)
(declare-var has_been_true_reset.reset Bool)
(declare-var has_been_true_reset.out Bool)
(declare-var has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var has_been_true_reset.__has_been_true_reset_1 Bool)
(declare-var has_been_true_reset.__has_been_true_reset_3 Bool)
(declare-rel has_been_true_reset_reset (Bool Bool Bool Bool))
(declare-rel has_been_true_reset_step (Bool Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       (= has_been_true_reset.__has_been_true_reset_2_m has_been_true_reset.__has_been_true_reset_2_c)
       (= has_been_true_reset.ni_79._arrow._first_m true)
  )
  (has_been_true_reset_reset has_been_true_reset.__has_been_true_reset_2_c
                             has_been_true_reset.ni_79._arrow._first_c
                             has_been_true_reset.__has_been_true_reset_2_m
                             has_been_true_reset.ni_79._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= has_been_true_reset.ni_79._arrow._first_m has_been_true_reset.ni_79._arrow._first_c)
       (and (= has_been_true_reset.__has_been_true_reset_1 (ite has_been_true_reset.ni_79._arrow._first_m true false))
            (= has_been_true_reset.ni_79._arrow._first_x false))
       (and (or (not (= has_been_true_reset.__has_been_true_reset_1 true))
               (= has_been_true_reset.__has_been_true_reset_3 has_been_true_reset.in))
            (or (not (= has_been_true_reset.__has_been_true_reset_1 false))
               (= has_been_true_reset.__has_been_true_reset_3 (or has_been_true_reset.in has_been_true_reset.__has_been_true_reset_2_c)))
       )
       (= has_been_true_reset.out (and has_been_true_reset.__has_been_true_reset_3 (not has_been_true_reset.reset)))
       (= has_been_true_reset.__has_been_true_reset_2_x has_been_true_reset.out)
       )
  (has_been_true_reset_step has_been_true_reset.in
                            has_been_true_reset.reset
                            has_been_true_reset.out
                            has_been_true_reset.__has_been_true_reset_2_c
                            has_been_true_reset.ni_79._arrow._first_c
                            has_been_true_reset.__has_been_true_reset_2_x
                            has_been_true_reset.ni_79._arrow._first_x)
))

; since
(declare-var since.in Bool)
(declare-var since.out Int)
(declare-var since.__since_2_c Int)
(declare-var since.ni_78._arrow._first_c Bool)
(declare-var since.__since_2_m Int)
(declare-var since.ni_78._arrow._first_m Bool)
(declare-var since.__since_2_x Int)
(declare-var since.ni_78._arrow._first_x Bool)
(declare-var since.__since_1 Bool)
(declare-rel since_reset (Int Bool Int Bool))
(declare-rel since_step (Bool Int Int Bool Int Bool))

(rule (=> 
  (and 
       (= since.__since_2_m since.__since_2_c)
       (= since.ni_78._arrow._first_m true)
  )
  (since_reset since.__since_2_c
               since.ni_78._arrow._first_c
               since.__since_2_m
               since.ni_78._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= since.ni_78._arrow._first_m since.ni_78._arrow._first_c)(and (= since.__since_1 (ite since.ni_78._arrow._first_m true false))
                                                                    (= since.ni_78._arrow._first_x false))
       (and (or (not (= since.__since_1 true))
               (and (or (not (= since.in true))
                       (= since.out 1))
                    (or (not (= since.in false))
                       (= since.out 0))
               ))
            (or (not (= since.__since_1 false))
               (and (or (not (= since.in true))
                       (= since.out (+ since.__since_2_c 1)))
                    (or (not (= since.in false))
                       (= since.out 0))
               ))
       )
       (= since.__since_2_x since.out)
       )
  (since_step since.in
              since.out
              since.__since_2_c
              since.ni_78._arrow._first_c
              since.__since_2_x
              since.ni_78._arrow._first_x)
))

; LONGITUDINAL_CONTROLLER_AltitudeControl
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.engage_1_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.AltCmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.Altitude_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.gskts_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.hdot_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.d_out1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.d_out2 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.altgamcmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.Abs_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.Divide_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.Saturation1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.Sum3_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.Switch_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.VariableLimitSaturation_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.VariableRateLimit_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_3 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_4 Real)
(declare-var LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_5 Real)
(declare-rel LONGITUDINAL_CONTROLLER_AltitudeControl_reset (Bool Real Real Bool Bool Real Real Bool))
(declare-rel LONGITUDINAL_CONTROLLER_AltitudeControl_step (Bool Real Real Real Real Real Real Real Bool Real Real Bool Bool Real Real Bool))

(rule (=> 
  (and 
       
       (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_reset 
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m)
  )
  (LONGITUDINAL_CONTROLLER_AltitudeControl_reset LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                                 LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                                 LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                                 LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                                 LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                                 LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                                 LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                                 LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (and (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl.hdot_1_1 0.0) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.Abs_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl.hdot_1_1))
            (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl.hdot_1_1 0.0) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.Abs_1_1 (- LONGITUDINAL_CONTROLLER_AltitudeControl.hdot_1_1)))
       )
       (= LONGITUDINAL_CONTROLLER_AltitudeControl.Sum3_1_1 (+ LONGITUDINAL_CONTROLLER_AltitudeControl.Abs_1_1 10.0000000000))
       (and (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl.engage_1_1 true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.Switch_1_1 (* 0.0800000000 (- LONGITUDINAL_CONTROLLER_AltitudeControl.AltCmd_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl.Altitude_1_1))))
            (or (not (= LONGITUDINAL_CONTROLLER_AltitudeControl.engage_1_1 false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.Switch_1_1 0.0000000000))
       )
       (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableLimitSaturation_fun 
       LONGITUDINAL_CONTROLLER_AltitudeControl.Sum3_1_1
       LONGITUDINAL_CONTROLLER_AltitudeControl.Switch_1_1
       (* (- 1.0000000000) LONGITUDINAL_CONTROLLER_AltitudeControl.Sum3_1_1)
       LONGITUDINAL_CONTROLLER_AltitudeControl.VariableLimitSaturation_1_1)
       (and (= LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c)
            (= LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c)
            (= LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c)
            (= LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c)
            )
       (LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_step 
       3.2000000000
       LONGITUDINAL_CONTROLLER_AltitudeControl.VariableLimitSaturation_1_1
       (not LONGITUDINAL_CONTROLLER_AltitudeControl.engage_1_1)
       LONGITUDINAL_CONTROLLER_AltitudeControl.hdot_1_1
       LONGITUDINAL_CONTROLLER_AltitudeControl.VariableRateLimit_1_1
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
       LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x)
       (LONGITUDINAL_CONTROLLER_AltitudeControl_Saturation1_fun (* 1.6878000000 LONGITUDINAL_CONTROLLER_AltitudeControl.gskts_1_1)
                                                                LONGITUDINAL_CONTROLLER_AltitudeControl.Saturation1_1_1)
       (divid_bounded_num_fun LONGITUDINAL_CONTROLLER_AltitudeControl.VariableRateLimit_1_1
                              LONGITUDINAL_CONTROLLER_AltitudeControl.Saturation1_1_1
                              LONGITUDINAL_CONTROLLER_AltitudeControl.d_out2
                              0.0001
                              1000.0
                              LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_5)
       (and (or (not (= (< LONGITUDINAL_CONTROLLER_AltitudeControl.gskts_1_1 100.0) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_3 200.0))
            (or (not (= (< LONGITUDINAL_CONTROLLER_AltitudeControl.gskts_1_1 100.0) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_3 LONGITUDINAL_CONTROLLER_AltitudeControl.Saturation1_1_1))
       )
       (divid_bounded_num_fun LONGITUDINAL_CONTROLLER_AltitudeControl.VariableRateLimit_1_1
                              LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_3
                              LONGITUDINAL_CONTROLLER_AltitudeControl.d_out1
                              168.78
                              1000.0
                              LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_4)
       (and (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl.gskts_1_1 100.0) true))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.Divide_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_4))
            (or (not (= (>= LONGITUDINAL_CONTROLLER_AltitudeControl.gskts_1_1 100.0) false))
               (= LONGITUDINAL_CONTROLLER_AltitudeControl.Divide_1_1 LONGITUDINAL_CONTROLLER_AltitudeControl.__LONGITUDINAL_CONTROLLER_AltitudeControl_5))
       )
       (= LONGITUDINAL_CONTROLLER_AltitudeControl.altgamcmd_1_1 (* 57.2958000000 LONGITUDINAL_CONTROLLER_AltitudeControl.Divide_1_1))
       )
  (LONGITUDINAL_CONTROLLER_AltitudeControl_step LONGITUDINAL_CONTROLLER_AltitudeControl.engage_1_1
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.AltCmd_1_1
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.Altitude_1_1
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.gskts_1_1
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.hdot_1_1
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.d_out1
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.d_out2
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.altgamcmd_1_1
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
                                                LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x)
))

; LONGITUDINAL_CONTROLLER_EngageORzero
(declare-var LONGITUDINAL_CONTROLLER_EngageORzero.Engage_1_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER_EngageORzero.In_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_EngageORzero.Out_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_EngageORzero.Switch1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_4 Bool)
(declare-rel LONGITUDINAL_CONTROLLER_EngageORzero_fun (Bool Real Real))
; Stateless step rule 
(rule (=> 
  (and (and (or (not (= (= 1.0000000000 0.0) true))
               (= LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_4 false))
            (or (not (= (= 1.0000000000 0.0) false))
               (= LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_4 true))
       )
       (and (or (not (= (and LONGITUDINAL_CONTROLLER_EngageORzero.Engage_1_1 LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_4) true))
               (= LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_1 1.0))
            (or (not (= (and LONGITUDINAL_CONTROLLER_EngageORzero.Engage_1_1 LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_4) false))
               (= LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_1 0.0))
       )
       (and (or (not (= (>= LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_1 0.5000000000) true))
               (= LONGITUDINAL_CONTROLLER_EngageORzero.Switch1_1_1 LONGITUDINAL_CONTROLLER_EngageORzero.In_1_1))
            (or (not (= (>= LONGITUDINAL_CONTROLLER_EngageORzero.__LONGITUDINAL_CONTROLLER_EngageORzero_1 0.5000000000) false))
               (= LONGITUDINAL_CONTROLLER_EngageORzero.Switch1_1_1 0.0000000000))
       )
       (= LONGITUDINAL_CONTROLLER_EngageORzero.Out_1_1 LONGITUDINAL_CONTROLLER_EngageORzero.Switch1_1_1)
       )
  (LONGITUDINAL_CONTROLLER_EngageORzero_fun LONGITUDINAL_CONTROLLER_EngageORzero.Engage_1_1 LONGITUDINAL_CONTROLLER_EngageORzero.In_1_1 LONGITUDINAL_CONTROLLER_EngageORzero.Out_1_1)
))

; LONGITUDINAL_CONTROLLER_FPAControl
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.engage_1_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.gamcmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.gamma_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.thetadeg_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.VT_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.PitchCmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.DynamicSaturation_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.Kgamerr_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.Product1_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.Sum2_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_FPAControl.integrator_reset_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_FPAControl_reset (Bool Real Real Bool Bool Real Real Bool))
(declare-rel LONGITUDINAL_CONTROLLER_FPAControl_step (Bool Real Real Real Real Real Bool Real Real Bool Bool Real Real Bool))

(rule (=> 
  (and 
       
       (LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_reset LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                                                  LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                                                  LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                                                  LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                                                  LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                                                  LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                                                  LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                                                  LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m)
  )
  (LONGITUDINAL_CONTROLLER_FPAControl_reset LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                            LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                            LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                            LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                            LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                            LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                            LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                            LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= LONGITUDINAL_CONTROLLER_FPAControl.Product1_1_1 (* (* 4.83 57.2958) (* 1.6878000000 LONGITUDINAL_CONTROLLER_FPAControl.VT_1_1)))
       (= LONGITUDINAL_CONTROLLER_FPAControl.Kgamerr_1_1 (* 1.4000000000 LONGITUDINAL_CONTROLLER_FPAControl.gamma_1_1))
       (= LONGITUDINAL_CONTROLLER_FPAControl.Sum2_1_1 (- LONGITUDINAL_CONTROLLER_FPAControl.gamcmd_1_1 LONGITUDINAL_CONTROLLER_FPAControl.gamma_1_1))
       (LONGITUDINAL_CONTROLLER_FPAControl_DynamicSaturation_fun LONGITUDINAL_CONTROLLER_FPAControl.Product1_1_1
                                                                 (* 1.0000000000 LONGITUDINAL_CONTROLLER_FPAControl.Sum2_1_1)
                                                                 (* (- 1.0000000000) LONGITUDINAL_CONTROLLER_FPAControl.Product1_1_1)
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.DynamicSaturation_1_1)
       (and (= LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c)
            (= LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c)
            (= LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c)
            (= LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c)
            )
       (LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_step LONGITUDINAL_CONTROLLER_FPAControl.DynamicSaturation_1_1
                                                                 (not LONGITUDINAL_CONTROLLER_FPAControl.engage_1_1)
                                                                 (+ LONGITUDINAL_CONTROLLER_FPAControl.thetadeg_1_1 LONGITUDINAL_CONTROLLER_FPAControl.Kgamerr_1_1)
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.integrator_reset_1_1
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
                                                                 LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x)
       (= LONGITUDINAL_CONTROLLER_FPAControl.PitchCmd_1_1 (- LONGITUDINAL_CONTROLLER_FPAControl.integrator_reset_1_1 LONGITUDINAL_CONTROLLER_FPAControl.Kgamerr_1_1))
       )
  (LONGITUDINAL_CONTROLLER_FPAControl_step LONGITUDINAL_CONTROLLER_FPAControl.engage_1_1
                                           LONGITUDINAL_CONTROLLER_FPAControl.gamcmd_1_1
                                           LONGITUDINAL_CONTROLLER_FPAControl.gamma_1_1
                                           LONGITUDINAL_CONTROLLER_FPAControl.thetadeg_1_1
                                           LONGITUDINAL_CONTROLLER_FPAControl.VT_1_1
                                           LONGITUDINAL_CONTROLLER_FPAControl.PitchCmd_1_1
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
                                           LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x)
))

; LONGITUDINAL_CONTROLLER_ManualOverride
(declare-var LONGITUDINAL_CONTROLLER_ManualOverride.Man_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_ManualOverride.Auto_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_ManualOverride.Out_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_ManualOverride.Switch1_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_ManualOverride_fun (Real Real Real))
; Stateless step rule 
(rule (=> 
  (and (and (or (not (= (not (= LONGITUDINAL_CONTROLLER_ManualOverride.Man_1_1 0.0)) true))
               (= LONGITUDINAL_CONTROLLER_ManualOverride.Switch1_1_1 LONGITUDINAL_CONTROLLER_ManualOverride.Man_1_1))
            (or (not (= (not (= LONGITUDINAL_CONTROLLER_ManualOverride.Man_1_1 0.0)) false))
               (= LONGITUDINAL_CONTROLLER_ManualOverride.Switch1_1_1 LONGITUDINAL_CONTROLLER_ManualOverride.Auto_1_1))
       )
       (= LONGITUDINAL_CONTROLLER_ManualOverride.Out_1_1 LONGITUDINAL_CONTROLLER_ManualOverride.Switch1_1_1)
       )
  (LONGITUDINAL_CONTROLLER_ManualOverride_fun LONGITUDINAL_CONTROLLER_ManualOverride.Man_1_1 LONGITUDINAL_CONTROLLER_ManualOverride.Auto_1_1 LONGITUDINAL_CONTROLLER_ManualOverride.Out_1_1)
))

; LONGITUDINAL_CONTROLLER_PitchInnerLoop
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.PitchCmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.Pitch_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.qdeg_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.CAS_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out2 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out2 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out3 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out3 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out4 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out4 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.ElevCmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.Product_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.TransferFunc_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER_PitchInnerLoop.lookup1d_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_reset (Real Bool Real Bool))
(declare-rel LONGITUDINAL_CONTROLLER_PitchInnerLoop_step (Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Real Bool))

(rule (=> 
  (and 
       
       (LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_reset LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                                                  LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                                                  LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                                                  LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m)
  )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_reset LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                                LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                                LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                                LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (LONGITUDINAL_CONTROLLER_PitchInnerLoop_lookup1d_fun LONGITUDINAL_CONTROLLER_PitchInnerLoop.CAS_1_1
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out2
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out2
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out3
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out3
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out4
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out4
                                                            LONGITUDINAL_CONTROLLER_PitchInnerLoop.lookup1d_1_1)
       (and (= LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c)
            (= LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c)
            )
       (LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_step LONGITUDINAL_CONTROLLER_PitchInnerLoop.qdeg_1_1
                                                                 LONGITUDINAL_CONTROLLER_PitchInnerLoop.TransferFunc_1_1
                                                                 LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                                                 LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                                                 LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
                                                                 LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x)
       (times_fun (+ (- (* 2.0000000000 (- LONGITUDINAL_CONTROLLER_PitchInnerLoop.PitchCmd_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop.Pitch_1_1))) (* 0.5000000000 LONGITUDINAL_CONTROLLER_PitchInnerLoop.TransferFunc_1_1))
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop.lookup1d_1_1
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out1
                  LONGITUDINAL_CONTROLLER_PitchInnerLoop.Product_1_1)
       (= LONGITUDINAL_CONTROLLER_PitchInnerLoop.ElevCmd_1_1 LONGITUDINAL_CONTROLLER_PitchInnerLoop.Product_1_1)
       )
  (LONGITUDINAL_CONTROLLER_PitchInnerLoop_step LONGITUDINAL_CONTROLLER_PitchInnerLoop.PitchCmd_1_1
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.Pitch_1_1
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.qdeg_1_1
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.CAS_1_1
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out1
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out2
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out2
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out3
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out3
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.t_out4
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.d_out4
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.ElevCmd_1_1
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
                                               LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x)
))

; MODE_LOGIC_AltAndFPAMode
(declare-var MODE_LOGIC_AltAndFPAMode.ActiavteFPA_1_1 Real)
(declare-var MODE_LOGIC_AltAndFPAMode.Deactivate_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.Altitude_1_1 Real)
(declare-var MODE_LOGIC_AltAndFPAMode.AltCmd_1_1 Real)
(declare-var MODE_LOGIC_AltAndFPAMode.ActiavteAlt_1_1 Real)
(declare-var MODE_LOGIC_AltAndFPAMode.AltEng_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.FPAEng_2_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.Abs_1_1 Real)
(declare-var MODE_LOGIC_AltAndFPAMode.Add_1_1 Real)
(declare-var MODE_LOGIC_AltAndFPAMode.CompareToConstant2_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.CompareToConstant_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_LogicalOperator1_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay1_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay_1_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_1 Bool)
(declare-var MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_6 Real)
(declare-rel MODE_LOGIC_AltAndFPAMode_reset (Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool))
(declare-rel MODE_LOGIC_AltAndFPAMode_step (Real Bool Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c)
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c)
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c)
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c)
       (= MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m true)
  )
  (MODE_LOGIC_AltAndFPAMode_reset MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                                  MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                                  MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                                  MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                                  MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                                  MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                                  MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                                  MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                                  MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                                  MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= MODE_LOGIC_AltAndFPAMode.Add_1_1 (- MODE_LOGIC_AltAndFPAMode.Altitude_1_1 MODE_LOGIC_AltAndFPAMode.AltCmd_1_1))
       (and (or (not (= MODE_LOGIC_AltAndFPAMode.Deactivate_1_1 true))
               (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_6 1.0))
            (or (not (= MODE_LOGIC_AltAndFPAMode.Deactivate_1_1 false))
               (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_6 0.0))
       )
       (= MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c)
       (and (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_1 (ite MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m true false))
            (= MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x false))
       (and (or (not (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_1 false))
               (and (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay_1_1 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c)
                    (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay1_1_1 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c)
                    ))
            (or (not (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_1 true))
               (and (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay_1_1 false)
                    (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay1_1_1 true)
                    ))
       )
       (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_LogicalOperator1_1_1 (not (and (not (not (= MODE_LOGIC_AltAndFPAMode.ActiavteAlt_1_1 0.0000000000))) MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay1_1_1)))
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_LogicalOperator1_1_1)
       (= MODE_LOGIC_AltAndFPAMode.CompareToConstant_1_1 (not (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_6 0.0000000000)))
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x (not (and MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_UnitDelay_1_1 (not MODE_LOGIC_AltAndFPAMode.CompareToConstant_1_1))))
       (and (or (not (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_1 false))
               (and (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay_1_1 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c)
                    (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c)
                    ))
            (or (not (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_1 true))
               (and (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay_1_1 false)
                    (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 true)
                    ))
       )
       (= MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1 (not (and (not (not (= MODE_LOGIC_AltAndFPAMode.ActiavteFPA_1_1 0.0000000000))) MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay1_1_1)))
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1)
       (= MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x (not (and MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__UnitDelay_1_1 (not MODE_LOGIC_AltAndFPAMode.CompareToConstant_1_1))))
       (and (or (not (= (>= MODE_LOGIC_AltAndFPAMode.Add_1_1 0.0) true))
               (= MODE_LOGIC_AltAndFPAMode.Abs_1_1 MODE_LOGIC_AltAndFPAMode.Add_1_1))
            (or (not (= (>= MODE_LOGIC_AltAndFPAMode.Add_1_1 0.0) false))
               (= MODE_LOGIC_AltAndFPAMode.Abs_1_1 (- MODE_LOGIC_AltAndFPAMode.Add_1_1)))
       )
       (= MODE_LOGIC_AltAndFPAMode.CompareToConstant2_1_1 (<= MODE_LOGIC_AltAndFPAMode.Abs_1_1 200.0000000000))
       (= MODE_LOGIC_AltAndFPAMode.FPAEng_2_1 (and MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1 (not (and MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_LogicalOperator1_1_1 MODE_LOGIC_AltAndFPAMode.CompareToConstant2_1_1))))
       (= MODE_LOGIC_AltAndFPAMode.AltEng_1_1 (and MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot_1_LogicalOperator1_1_1 (or (not MODE_LOGIC_AltAndFPAMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1) MODE_LOGIC_AltAndFPAMode.CompareToConstant2_1_1)))
       )
  (MODE_LOGIC_AltAndFPAMode_step MODE_LOGIC_AltAndFPAMode.ActiavteFPA_1_1
                                 MODE_LOGIC_AltAndFPAMode.Deactivate_1_1
                                 MODE_LOGIC_AltAndFPAMode.Altitude_1_1
                                 MODE_LOGIC_AltAndFPAMode.AltCmd_1_1
                                 MODE_LOGIC_AltAndFPAMode.ActiavteAlt_1_1
                                 MODE_LOGIC_AltAndFPAMode.AltEng_1_1
                                 MODE_LOGIC_AltAndFPAMode.FPAEng_2_1
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                                 MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x
                                 MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x
                                 MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
))

; MODE_LOGIC_HeadingMode
(declare-var MODE_LOGIC_HeadingMode.Actiavte_1_1 Real)
(declare-var MODE_LOGIC_HeadingMode.Deactivate_1_1 Bool)
(declare-var MODE_LOGIC_HeadingMode.HeadEng_1_1 Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c Bool)
(declare-var MODE_LOGIC_HeadingMode.ni_73._arrow._first_c Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m Bool)
(declare-var MODE_LOGIC_HeadingMode.ni_73._arrow._first_m Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x Bool)
(declare-var MODE_LOGIC_HeadingMode.ni_73._arrow._first_x Bool)
(declare-var MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1 Bool)
(declare-var MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 Bool)
(declare-var MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay_1_1 Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_1 Bool)
(declare-var MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_4 Real)
(declare-rel MODE_LOGIC_HeadingMode_reset (Bool Bool Bool Bool Bool Bool))
(declare-rel MODE_LOGIC_HeadingMode_step (Real Bool Bool Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c)
       (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c)
       (= MODE_LOGIC_HeadingMode.ni_73._arrow._first_m true)
  )
  (MODE_LOGIC_HeadingMode_reset MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                                MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                                MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                                MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                                MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                                MODE_LOGIC_HeadingMode.ni_73._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (and (or (not (= MODE_LOGIC_HeadingMode.Deactivate_1_1 true))
               (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_4 1.0))
            (or (not (= MODE_LOGIC_HeadingMode.Deactivate_1_1 false))
               (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_4 0.0))
       )
       (= MODE_LOGIC_HeadingMode.ni_73._arrow._first_m MODE_LOGIC_HeadingMode.ni_73._arrow._first_c)
       (and (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_1 (ite MODE_LOGIC_HeadingMode.ni_73._arrow._first_m true false))
            (= MODE_LOGIC_HeadingMode.ni_73._arrow._first_x false))
       (and (or (not (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_1 false))
               (and (= MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay_1_1 MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c)
                    (= MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c)
                    ))
            (or (not (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_1 true))
               (and (= MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay_1_1 false)
                    (= MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 true)
                    ))
       )
       (= MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1 (not (and (not (not (= MODE_LOGIC_HeadingMode.Actiavte_1_1 0.0000000000))) MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay1_1_1)))
       (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1)
       (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x (not (and MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__UnitDelay_1_1 (not (not (= MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_4 0.0000000000))))))
       (= MODE_LOGIC_HeadingMode.HeadEng_1_1 MODE_LOGIC_HeadingMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1)
       )
  (MODE_LOGIC_HeadingMode_step MODE_LOGIC_HeadingMode.Actiavte_1_1
                               MODE_LOGIC_HeadingMode.Deactivate_1_1
                               MODE_LOGIC_HeadingMode.HeadEng_1_1
                               MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                               MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                               MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                               MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x
                               MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x
                               MODE_LOGIC_HeadingMode.ni_73._arrow._first_x)
))

; MODE_LOGIC_SpeedMode
(declare-var MODE_LOGIC_SpeedMode.Actiavte_1_1 Real)
(declare-var MODE_LOGIC_SpeedMode.Deactivate_1_1 Real)
(declare-var MODE_LOGIC_SpeedMode.AltEng_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.CAS_1_1 Real)
(declare-var MODE_LOGIC_SpeedMode.CASCmdMCP_1_1 Real)
(declare-var MODE_LOGIC_SpeedMode.ATEng_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.CASCmd_2_1 Real)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c Real)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c Bool)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c Bool)
(declare-var MODE_LOGIC_SpeedMode.ni_72._arrow._first_c Bool)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m Real)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m Bool)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m Bool)
(declare-var MODE_LOGIC_SpeedMode.ni_72._arrow._first_m Bool)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x Real)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x Bool)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x Bool)
(declare-var MODE_LOGIC_SpeedMode.ni_72._arrow._first_x Bool)
(declare-var MODE_LOGIC_SpeedMode.Add_1_1 Int)
(declare-var MODE_LOGIC_SpeedMode.CompareToConstant1_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.LogicalOperator_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay_1_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.Switch1_1_1 Real)
(declare-var MODE_LOGIC_SpeedMode.Switch_1_1 Real)
(declare-var MODE_LOGIC_SpeedMode.UnitDelay2_1_1 Real)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 Bool)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_7 Int)
(declare-var MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_8 Int)
(declare-rel MODE_LOGIC_SpeedMode_reset (Real Bool Bool Bool Real Bool Bool Bool))
(declare-rel MODE_LOGIC_SpeedMode_step (Real Real Bool Real Real Bool Real Real Bool Bool Bool Real Bool Bool Bool))

(rule (=> 
  (and 
       (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c)
       (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c)
       (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c)
       (= MODE_LOGIC_SpeedMode.ni_72._arrow._first_m true)
  )
  (MODE_LOGIC_SpeedMode_reset MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                              MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                              MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                              MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                              MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                              MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                              MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                              MODE_LOGIC_SpeedMode.ni_72._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= MODE_LOGIC_SpeedMode.CompareToConstant1_1_1 (not (= MODE_LOGIC_SpeedMode.Actiavte_1_1 0.0000000000)))
       (and (or (not (= MODE_LOGIC_SpeedMode.CompareToConstant1_1_1 true))
               (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_8 1))
            (or (not (= MODE_LOGIC_SpeedMode.CompareToConstant1_1_1 false))
               (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_8 0))
       )
       (= MODE_LOGIC_SpeedMode.ni_72._arrow._first_m MODE_LOGIC_SpeedMode.ni_72._arrow._first_c)
       (and (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 (ite MODE_LOGIC_SpeedMode.ni_72._arrow._first_m true false))
            (= MODE_LOGIC_SpeedMode.ni_72._arrow._first_x false))
       (and (or (not (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 true))
               (= MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay_1_1 false))
            (or (not (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 false))
               (= MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay_1_1 MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c))
       )
       (= MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator_1_1 (not (and MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay_1_1 (not (not (= MODE_LOGIC_SpeedMode.Deactivate_1_1 0.0000000000))))))
       (= MODE_LOGIC_SpeedMode.LogicalOperator_1_1 (and MODE_LOGIC_SpeedMode.AltEng_1_1 MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator_1_1))
       (and (or (not (= MODE_LOGIC_SpeedMode.LogicalOperator_1_1 true))
               (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_7 1))
            (or (not (= MODE_LOGIC_SpeedMode.LogicalOperator_1_1 false))
               (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_7 0))
       )
       (= MODE_LOGIC_SpeedMode.Add_1_1 (+ MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_8 MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_7))
       (and (or (not (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 true))
               (= MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 true))
            (or (not (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 false))
               (= MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay1_1_1 MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c))
       )
       (= MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1 (not (and (not (or MODE_LOGIC_SpeedMode.CompareToConstant1_1_1 MODE_LOGIC_SpeedMode.LogicalOperator_1_1)) MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__UnitDelay1_1_1)))
       (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1)
       (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator_1_1)
       (and (or (not (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 true))
               (= MODE_LOGIC_SpeedMode.UnitDelay2_1_1 0.0000000000))
            (or (not (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_1 false))
               (= MODE_LOGIC_SpeedMode.UnitDelay2_1_1 MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c))
       )
       (and (or (not (= (> MODE_LOGIC_SpeedMode.Add_1_1 1) true))
               (= MODE_LOGIC_SpeedMode.Switch1_1_1 MODE_LOGIC_SpeedMode.CASCmdMCP_1_1))
            (or (not (= (> MODE_LOGIC_SpeedMode.Add_1_1 1) false))
               (= MODE_LOGIC_SpeedMode.Switch1_1_1 MODE_LOGIC_SpeedMode.CAS_1_1))
       )
       (and (or (not (= (>= MODE_LOGIC_SpeedMode.Add_1_1 0) true))
               (= MODE_LOGIC_SpeedMode.Switch_1_1 MODE_LOGIC_SpeedMode.UnitDelay2_1_1))
            (or (not (= (>= MODE_LOGIC_SpeedMode.Add_1_1 0) false))
               (= MODE_LOGIC_SpeedMode.Switch_1_1 MODE_LOGIC_SpeedMode.Switch1_1_1))
       )
       (= MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x MODE_LOGIC_SpeedMode.Switch_1_1)
       (= MODE_LOGIC_SpeedMode.CASCmd_2_1 MODE_LOGIC_SpeedMode.UnitDelay2_1_1)
       (= MODE_LOGIC_SpeedMode.ATEng_1_1 MODE_LOGIC_SpeedMode.SRFlipFlopRepl_dot__LogicalOperator1_1_1)
       )
  (MODE_LOGIC_SpeedMode_step MODE_LOGIC_SpeedMode.Actiavte_1_1
                             MODE_LOGIC_SpeedMode.Deactivate_1_1
                             MODE_LOGIC_SpeedMode.AltEng_1_1
                             MODE_LOGIC_SpeedMode.CAS_1_1
                             MODE_LOGIC_SpeedMode.CASCmdMCP_1_1
                             MODE_LOGIC_SpeedMode.ATEng_1_1
                             MODE_LOGIC_SpeedMode.CASCmd_2_1
                             MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                             MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                             MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                             MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                             MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x
                             MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x
                             MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x
                             MODE_LOGIC_SpeedMode.ni_72._arrow._first_x)
))

; logic_alt_fpa
(declare-var logic_alt_fpa.in_alt_engage Bool)
(declare-var logic_alt_fpa.in_fpa_engage Bool)
(declare-var logic_alt_fpa.in_disengage Bool)
(declare-var logic_alt_fpa.altitude Real)
(declare-var logic_alt_fpa.altitude_target Real)
(declare-var logic_alt_fpa.out_alt_engaged Bool)
(declare-var logic_alt_fpa.out_fpa_engaged Bool)
(declare-var logic_alt_fpa.alt_wag_170 Bool)
(declare-var logic_alt_fpa.alt_tag_210 Bool)
(declare-var logic_alt_fpa.fpa_wag_180 Bool)
(declare-var logic_alt_fpa.fpa_tag_210 Bool)
(declare-var logic_alt_fpa.OK4 Bool)
(declare-var logic_alt_fpa.OK5 Bool)
(declare-var logic_alt_fpa.OK6 Bool)
(declare-var logic_alt_fpa.OK7 Bool)
(declare-var logic_alt_fpa.OK8 Bool)
(declare-var logic_alt_fpa.OK9 Bool)
(declare-var logic_alt_fpa.OK10 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_12_c Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_17_c Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_22_c Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_27_c Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_32_c Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_4_c Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_7_c Int)
(declare-var logic_alt_fpa.ni_46.duration.__duration_2_c Int)
(declare-var logic_alt_fpa.ni_46.duration.__duration_4_c Bool)
(declare-var logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_47.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_47.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_48._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_49.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_49.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_50.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_50.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_51._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_52.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_52.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_53.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_53.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_54._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_55.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_55.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_56.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_56.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_57._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_58.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_58.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_59.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_59.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_60._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_61.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_61.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_62.fall.__fall_2_c Bool)
(declare-var logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_63.fall.__fall_2_c Bool)
(declare-var logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_64.fall.__fall_2_c Bool)
(declare-var logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_65.fall.__fall_2_c Bool)
(declare-var logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_66.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_66.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_67._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_68.since.__since_2_c Int)
(declare-var logic_alt_fpa.ni_68.since.ni_78._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic_alt_fpa.ni_71._arrow._first_c Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_12_m Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_17_m Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_22_m Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_27_m Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_32_m Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_4_m Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_7_m Int)
(declare-var logic_alt_fpa.ni_46.duration.__duration_2_m Int)
(declare-var logic_alt_fpa.ni_46.duration.__duration_4_m Bool)
(declare-var logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_47.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_47.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_48._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_49.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_49.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_50.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_50.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_51._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_52.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_52.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_53.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_53.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_54._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_55.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_55.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_56.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_56.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_57._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_58.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_58.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_59.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_59.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_60._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_61.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_61.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_62.fall.__fall_2_m Bool)
(declare-var logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_63.fall.__fall_2_m Bool)
(declare-var logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_64.fall.__fall_2_m Bool)
(declare-var logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_65.fall.__fall_2_m Bool)
(declare-var logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_66.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_66.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_67._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_68.since.__since_2_m Int)
(declare-var logic_alt_fpa.ni_68.since.ni_78._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic_alt_fpa.ni_71._arrow._first_m Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_12_x Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_17_x Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_22_x Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_27_x Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_32_x Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_4_x Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_7_x Int)
(declare-var logic_alt_fpa.ni_46.duration.__duration_2_x Int)
(declare-var logic_alt_fpa.ni_46.duration.__duration_4_x Bool)
(declare-var logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_47.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_47.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_48._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_49.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_49.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_50.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_50.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_51._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_52.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_52.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_53.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_53.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_54._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_55.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_55.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_56.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_56.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_57._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_58.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_58.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_59.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_59.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_60._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_61.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_61.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_62.fall.__fall_2_x Bool)
(declare-var logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_63.fall.__fall_2_x Bool)
(declare-var logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_64.fall.__fall_2_x Bool)
(declare-var logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_65.fall.__fall_2_x Bool)
(declare-var logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_66.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_66.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_67._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_68.since.__since_2_x Int)
(declare-var logic_alt_fpa.ni_68.since.ni_78._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic_alt_fpa.ni_71._arrow._first_x Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_1 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_10 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_11 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_13 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_14 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_15 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_16 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_18 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_19 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_2 Real)
(declare-var logic_alt_fpa.__logic_alt_fpa_20 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_21 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_23 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_24 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_25 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_26 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_28 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_29 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_3 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_30 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_31 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_33 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_34 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_35 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_36 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_37 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_38 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_5 Bool)
(declare-var logic_alt_fpa.__logic_alt_fpa_6 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_8 Int)
(declare-var logic_alt_fpa.__logic_alt_fpa_9 Bool)
(declare-var logic_alt_fpa.alt_requested Bool)
(declare-var logic_alt_fpa.deactivation Bool)
(declare-var logic_alt_fpa.fpa_requested Bool)
(declare-var logic_alt_fpa.tiny_altitude_gap Bool)
(declare-var logic_alt_fpa.wide_altitude_gap Bool)
(declare-rel logic_alt_fpa_reset (Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool))
(declare-rel logic_alt_fpa_step (Bool Bool Bool Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       (= logic_alt_fpa.__logic_alt_fpa_12_m logic_alt_fpa.__logic_alt_fpa_12_c)
       (= logic_alt_fpa.__logic_alt_fpa_17_m logic_alt_fpa.__logic_alt_fpa_17_c)
       (= logic_alt_fpa.__logic_alt_fpa_22_m logic_alt_fpa.__logic_alt_fpa_22_c)
       (= logic_alt_fpa.__logic_alt_fpa_27_m logic_alt_fpa.__logic_alt_fpa_27_c)
       (= logic_alt_fpa.__logic_alt_fpa_32_m logic_alt_fpa.__logic_alt_fpa_32_c)
       (= logic_alt_fpa.__logic_alt_fpa_4_m logic_alt_fpa.__logic_alt_fpa_4_c)
       (= logic_alt_fpa.__logic_alt_fpa_7_m logic_alt_fpa.__logic_alt_fpa_7_c)
       (= logic_alt_fpa.ni_71._arrow._first_m true)
       (has_been_true_reset_reset logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
                                  logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
                                  logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                                  logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m)
       (has_been_true_reset_reset logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
                                  logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
                                  logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                                  logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m)
       (since_reset logic_alt_fpa.ni_68.since.__since_2_c
                    logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_68.since.__since_2_m
                    logic_alt_fpa.ni_68.since.ni_78._arrow._first_m)
       (= logic_alt_fpa.ni_67._arrow._first_m true)
       (since_reset logic_alt_fpa.ni_66.since.__since_2_c
                    logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_66.since.__since_2_m
                    logic_alt_fpa.ni_66.since.ni_78._arrow._first_m)
       (fall_reset logic_alt_fpa.ni_65.fall.__fall_2_c
                   logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
                   logic_alt_fpa.ni_65.fall.__fall_2_m
                   logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m)
       (fall_reset logic_alt_fpa.ni_64.fall.__fall_2_c
                   logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
                   logic_alt_fpa.ni_64.fall.__fall_2_m
                   logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m)
       (fall_reset logic_alt_fpa.ni_63.fall.__fall_2_c
                   logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
                   logic_alt_fpa.ni_63.fall.__fall_2_m
                   logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m)
       (fall_reset logic_alt_fpa.ni_62.fall.__fall_2_c
                   logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
                   logic_alt_fpa.ni_62.fall.__fall_2_m
                   logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m)
       (since_reset logic_alt_fpa.ni_61.since.__since_2_c
                    logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_61.since.__since_2_m
                    logic_alt_fpa.ni_61.since.ni_78._arrow._first_m)
       (= logic_alt_fpa.ni_60._arrow._first_m true)
       (since_reset logic_alt_fpa.ni_59.since.__since_2_c
                    logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_59.since.__since_2_m
                    logic_alt_fpa.ni_59.since.ni_78._arrow._first_m)
       (since_reset logic_alt_fpa.ni_58.since.__since_2_c
                    logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_58.since.__since_2_m
                    logic_alt_fpa.ni_58.since.ni_78._arrow._first_m)
       (= logic_alt_fpa.ni_57._arrow._first_m true)
       (since_reset logic_alt_fpa.ni_56.since.__since_2_c
                    logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_56.since.__since_2_m
                    logic_alt_fpa.ni_56.since.ni_78._arrow._first_m)
       (since_reset logic_alt_fpa.ni_55.since.__since_2_c
                    logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_55.since.__since_2_m
                    logic_alt_fpa.ni_55.since.ni_78._arrow._first_m)
       (= logic_alt_fpa.ni_54._arrow._first_m true)
       (since_reset logic_alt_fpa.ni_53.since.__since_2_c
                    logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_53.since.__since_2_m
                    logic_alt_fpa.ni_53.since.ni_78._arrow._first_m)
       (since_reset logic_alt_fpa.ni_52.since.__since_2_c
                    logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_52.since.__since_2_m
                    logic_alt_fpa.ni_52.since.ni_78._arrow._first_m)
       (= logic_alt_fpa.ni_51._arrow._first_m true)
       (since_reset logic_alt_fpa.ni_50.since.__since_2_c
                    logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_50.since.__since_2_m
                    logic_alt_fpa.ni_50.since.ni_78._arrow._first_m)
       (since_reset logic_alt_fpa.ni_49.since.__since_2_c
                    logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_49.since.__since_2_m
                    logic_alt_fpa.ni_49.since.ni_78._arrow._first_m)
       (= logic_alt_fpa.ni_48._arrow._first_m true)
       (since_reset logic_alt_fpa.ni_47.since.__since_2_c
                    logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
                    logic_alt_fpa.ni_47.since.__since_2_m
                    logic_alt_fpa.ni_47.since.ni_78._arrow._first_m)
       (duration_reset logic_alt_fpa.ni_46.duration.__duration_2_c
                       logic_alt_fpa.ni_46.duration.__duration_4_c
                       logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
                       logic_alt_fpa.ni_46.duration.__duration_2_m
                       logic_alt_fpa.ni_46.duration.__duration_4_m
                       logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m)
  )
  (logic_alt_fpa_reset logic_alt_fpa.__logic_alt_fpa_12_c
                       logic_alt_fpa.__logic_alt_fpa_17_c
                       logic_alt_fpa.__logic_alt_fpa_22_c
                       logic_alt_fpa.__logic_alt_fpa_27_c
                       logic_alt_fpa.__logic_alt_fpa_32_c
                       logic_alt_fpa.__logic_alt_fpa_4_c
                       logic_alt_fpa.__logic_alt_fpa_7_c
                       logic_alt_fpa.ni_46.duration.__duration_2_c
                       logic_alt_fpa.ni_46.duration.__duration_4_c
                       logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
                       logic_alt_fpa.ni_47.since.__since_2_c
                       logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_48._arrow._first_c
                       logic_alt_fpa.ni_49.since.__since_2_c
                       logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_50.since.__since_2_c
                       logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_51._arrow._first_c
                       logic_alt_fpa.ni_52.since.__since_2_c
                       logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_53.since.__since_2_c
                       logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_54._arrow._first_c
                       logic_alt_fpa.ni_55.since.__since_2_c
                       logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_56.since.__since_2_c
                       logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_57._arrow._first_c
                       logic_alt_fpa.ni_58.since.__since_2_c
                       logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_59.since.__since_2_c
                       logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_60._arrow._first_c
                       logic_alt_fpa.ni_61.since.__since_2_c
                       logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_62.fall.__fall_2_c
                       logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
                       logic_alt_fpa.ni_63.fall.__fall_2_c
                       logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
                       logic_alt_fpa.ni_64.fall.__fall_2_c
                       logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
                       logic_alt_fpa.ni_65.fall.__fall_2_c
                       logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
                       logic_alt_fpa.ni_66.since.__since_2_c
                       logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_67._arrow._first_c
                       logic_alt_fpa.ni_68.since.__since_2_c
                       logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
                       logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
                       logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
                       logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
                       logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
                       logic_alt_fpa.ni_71._arrow._first_c
                       logic_alt_fpa.__logic_alt_fpa_12_m
                       logic_alt_fpa.__logic_alt_fpa_17_m
                       logic_alt_fpa.__logic_alt_fpa_22_m
                       logic_alt_fpa.__logic_alt_fpa_27_m
                       logic_alt_fpa.__logic_alt_fpa_32_m
                       logic_alt_fpa.__logic_alt_fpa_4_m
                       logic_alt_fpa.__logic_alt_fpa_7_m
                       logic_alt_fpa.ni_46.duration.__duration_2_m
                       logic_alt_fpa.ni_46.duration.__duration_4_m
                       logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                       logic_alt_fpa.ni_47.since.__since_2_m
                       logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_48._arrow._first_m
                       logic_alt_fpa.ni_49.since.__since_2_m
                       logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_50.since.__since_2_m
                       logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_51._arrow._first_m
                       logic_alt_fpa.ni_52.since.__since_2_m
                       logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_53.since.__since_2_m
                       logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_54._arrow._first_m
                       logic_alt_fpa.ni_55.since.__since_2_m
                       logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_56.since.__since_2_m
                       logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_57._arrow._first_m
                       logic_alt_fpa.ni_58.since.__since_2_m
                       logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_59.since.__since_2_m
                       logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_60._arrow._first_m
                       logic_alt_fpa.ni_61.since.__since_2_m
                       logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_62.fall.__fall_2_m
                       logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                       logic_alt_fpa.ni_63.fall.__fall_2_m
                       logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                       logic_alt_fpa.ni_64.fall.__fall_2_m
                       logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                       logic_alt_fpa.ni_65.fall.__fall_2_m
                       logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                       logic_alt_fpa.ni_66.since.__since_2_m
                       logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_67._arrow._first_m
                       logic_alt_fpa.ni_68.since.__since_2_m
                       logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                       logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                       logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                       logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                       logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                       logic_alt_fpa.ni_71._arrow._first_m)
))

; Step rule with Assertions 
(rule (=> 
  (and 
  (and (abs_real_fun (- logic_alt_fpa.altitude logic_alt_fpa.altitude_target)
                     logic_alt_fpa.__logic_alt_fpa_2)
       (= logic_alt_fpa.wide_altitude_gap (> logic_alt_fpa.__logic_alt_fpa_2 200.0))
       (= logic_alt_fpa.tiny_altitude_gap (not logic_alt_fpa.wide_altitude_gap))
       (= logic_alt_fpa.ni_71._arrow._first_m logic_alt_fpa.ni_71._arrow._first_c)
       (and (= logic_alt_fpa.__logic_alt_fpa_3 (ite logic_alt_fpa.ni_71._arrow._first_m true false))
            (= logic_alt_fpa.ni_71._arrow._first_x false))
       (and (or (not (= logic_alt_fpa.__logic_alt_fpa_3 true))
               (= logic_alt_fpa.deactivation false))
            (or (not (= logic_alt_fpa.__logic_alt_fpa_3 false))
               (= logic_alt_fpa.deactivation logic_alt_fpa.__logic_alt_fpa_4_c))
       )
       (and (= logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c)
            )
       (has_been_true_reset_step logic_alt_fpa.in_fpa_engage
                                 (and logic_alt_fpa.deactivation (not logic_alt_fpa.in_fpa_engage))
                                 logic_alt_fpa.fpa_requested
                                 logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                                 logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                                 logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x
                                 logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x)
       (= logic_alt_fpa.fpa_wag_180 (and logic_alt_fpa.wide_altitude_gap logic_alt_fpa.fpa_requested))
       (and (= logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c)
            )
       (has_been_true_reset_step logic_alt_fpa.in_alt_engage
                                 (and logic_alt_fpa.deactivation (not logic_alt_fpa.in_alt_engage))
                                 logic_alt_fpa.alt_requested
                                 logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                                 logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                                 logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x
                                 logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x)
       (= logic_alt_fpa.fpa_tag_210 (and (and logic_alt_fpa.tiny_altitude_gap logic_alt_fpa.fpa_requested) (not logic_alt_fpa.alt_requested)))
       (= logic_alt_fpa.alt_wag_170 (and (and logic_alt_fpa.wide_altitude_gap logic_alt_fpa.alt_requested) (not logic_alt_fpa.fpa_requested)))
       (= logic_alt_fpa.alt_tag_210 (and logic_alt_fpa.tiny_altitude_gap logic_alt_fpa.alt_requested))
       (and (= logic_alt_fpa.ni_68.since.__since_2_m logic_alt_fpa.ni_68.since.__since_2_c)
            (= logic_alt_fpa.ni_68.since.ni_78._arrow._first_m logic_alt_fpa.ni_68.since.ni_78._arrow._first_c)
            )
       (since_step logic_alt_fpa.in_alt_engage
                   logic_alt_fpa.__logic_alt_fpa_8
                   logic_alt_fpa.ni_68.since.__since_2_m
                   logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_68.since.__since_2_x
                   logic_alt_fpa.ni_68.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.ni_67._arrow._first_m logic_alt_fpa.ni_67._arrow._first_c)
       (and (= logic_alt_fpa.__logic_alt_fpa_5 (ite logic_alt_fpa.ni_67._arrow._first_m true false))
            (= logic_alt_fpa.ni_67._arrow._first_x false))
       (and (or (not (= logic_alt_fpa.__logic_alt_fpa_5 true))
               (= logic_alt_fpa.__logic_alt_fpa_9 true))
            (or (not (= logic_alt_fpa.__logic_alt_fpa_5 false))
               (= logic_alt_fpa.__logic_alt_fpa_9 (=> (= logic_alt_fpa.__logic_alt_fpa_8 1) (> logic_alt_fpa.__logic_alt_fpa_7_c 1))))
       )
       (and (= logic_alt_fpa.ni_66.since.__since_2_m logic_alt_fpa.ni_66.since.__since_2_c)
            (= logic_alt_fpa.ni_66.since.ni_78._arrow._first_m logic_alt_fpa.ni_66.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_alt_fpa.in_alt_engage)
                   logic_alt_fpa.__logic_alt_fpa_6
                   logic_alt_fpa.ni_66.since.__since_2_m
                   logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_66.since.__since_2_x
                   logic_alt_fpa.ni_66.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.__logic_alt_fpa_7_x logic_alt_fpa.__logic_alt_fpa_6)
       (= logic_alt_fpa.__logic_alt_fpa_4_x logic_alt_fpa.in_disengage)
       (and (= logic_alt_fpa.ni_65.fall.__fall_2_m logic_alt_fpa.ni_65.fall.__fall_2_c)
            (= logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c)
            )
       (fall_step logic_alt_fpa.in_fpa_engage
                  logic_alt_fpa.__logic_alt_fpa_38
                  logic_alt_fpa.ni_65.fall.__fall_2_m
                  logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                  logic_alt_fpa.ni_65.fall.__fall_2_x
                  logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x)
       (and (= logic_alt_fpa.ni_64.fall.__fall_2_m logic_alt_fpa.ni_64.fall.__fall_2_c)
            (= logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c)
            )
       (fall_step logic_alt_fpa.in_disengage
                  logic_alt_fpa.__logic_alt_fpa_37
                  logic_alt_fpa.ni_64.fall.__fall_2_m
                  logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                  logic_alt_fpa.ni_64.fall.__fall_2_x
                  logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x)
       (and (= logic_alt_fpa.ni_63.fall.__fall_2_m logic_alt_fpa.ni_63.fall.__fall_2_c)
            (= logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c)
            )
       (fall_step logic_alt_fpa.in_alt_engage
                  logic_alt_fpa.__logic_alt_fpa_36
                  logic_alt_fpa.ni_63.fall.__fall_2_m
                  logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                  logic_alt_fpa.ni_63.fall.__fall_2_x
                  logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x)
       (and (= logic_alt_fpa.ni_62.fall.__fall_2_m logic_alt_fpa.ni_62.fall.__fall_2_c)
            (= logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c)
            )
       (fall_step logic_alt_fpa.in_disengage
                  logic_alt_fpa.__logic_alt_fpa_35
                  logic_alt_fpa.ni_62.fall.__fall_2_m
                  logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                  logic_alt_fpa.ni_62.fall.__fall_2_x
                  logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x)
       (and (= logic_alt_fpa.ni_61.since.__since_2_m logic_alt_fpa.ni_61.since.__since_2_c)
            (= logic_alt_fpa.ni_61.since.ni_78._arrow._first_m logic_alt_fpa.ni_61.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_alt_fpa.in_disengage)
                   logic_alt_fpa.__logic_alt_fpa_33
                   logic_alt_fpa.ni_61.since.__since_2_m
                   logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_61.since.__since_2_x
                   logic_alt_fpa.ni_61.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.ni_60._arrow._first_m logic_alt_fpa.ni_60._arrow._first_c)
       (and (= logic_alt_fpa.__logic_alt_fpa_30 (ite logic_alt_fpa.ni_60._arrow._first_m true false))
            (= logic_alt_fpa.ni_60._arrow._first_x false))
       (and (or (not (= logic_alt_fpa.__logic_alt_fpa_30 true))
               (= logic_alt_fpa.__logic_alt_fpa_34 true))
            (or (not (= logic_alt_fpa.__logic_alt_fpa_30 false))
               (= logic_alt_fpa.__logic_alt_fpa_34 (=> (= logic_alt_fpa.__logic_alt_fpa_33 1) (> logic_alt_fpa.__logic_alt_fpa_32_c 1))))
       )
       (and (= logic_alt_fpa.ni_59.since.__since_2_m logic_alt_fpa.ni_59.since.__since_2_c)
            (= logic_alt_fpa.ni_59.since.ni_78._arrow._first_m logic_alt_fpa.ni_59.since.ni_78._arrow._first_c)
            )
       (since_step logic_alt_fpa.in_disengage
                   logic_alt_fpa.__logic_alt_fpa_31
                   logic_alt_fpa.ni_59.since.__since_2_m
                   logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_59.since.__since_2_x
                   logic_alt_fpa.ni_59.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.__logic_alt_fpa_32_x logic_alt_fpa.__logic_alt_fpa_31)
       (and (= logic_alt_fpa.ni_58.since.__since_2_m logic_alt_fpa.ni_58.since.__since_2_c)
            (= logic_alt_fpa.ni_58.since.ni_78._arrow._first_m logic_alt_fpa.ni_58.since.ni_78._arrow._first_c)
            )
       (since_step logic_alt_fpa.in_disengage
                   logic_alt_fpa.__logic_alt_fpa_28
                   logic_alt_fpa.ni_58.since.__since_2_m
                   logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_58.since.__since_2_x
                   logic_alt_fpa.ni_58.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.ni_57._arrow._first_m logic_alt_fpa.ni_57._arrow._first_c)
       (and (= logic_alt_fpa.__logic_alt_fpa_25 (ite logic_alt_fpa.ni_57._arrow._first_m true false))
            (= logic_alt_fpa.ni_57._arrow._first_x false))
       (and (or (not (= logic_alt_fpa.__logic_alt_fpa_25 true))
               (= logic_alt_fpa.__logic_alt_fpa_29 true))
            (or (not (= logic_alt_fpa.__logic_alt_fpa_25 false))
               (= logic_alt_fpa.__logic_alt_fpa_29 (=> (= logic_alt_fpa.__logic_alt_fpa_28 1) (> logic_alt_fpa.__logic_alt_fpa_27_c 1))))
       )
       (and (= logic_alt_fpa.ni_56.since.__since_2_m logic_alt_fpa.ni_56.since.__since_2_c)
            (= logic_alt_fpa.ni_56.since.ni_78._arrow._first_m logic_alt_fpa.ni_56.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_alt_fpa.in_disengage)
                   logic_alt_fpa.__logic_alt_fpa_26
                   logic_alt_fpa.ni_56.since.__since_2_m
                   logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_56.since.__since_2_x
                   logic_alt_fpa.ni_56.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.__logic_alt_fpa_27_x logic_alt_fpa.__logic_alt_fpa_26)
       (and (= logic_alt_fpa.ni_55.since.__since_2_m logic_alt_fpa.ni_55.since.__since_2_c)
            (= logic_alt_fpa.ni_55.since.ni_78._arrow._first_m logic_alt_fpa.ni_55.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_alt_fpa.in_fpa_engage)
                   logic_alt_fpa.__logic_alt_fpa_23
                   logic_alt_fpa.ni_55.since.__since_2_m
                   logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_55.since.__since_2_x
                   logic_alt_fpa.ni_55.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.ni_54._arrow._first_m logic_alt_fpa.ni_54._arrow._first_c)
       (and (= logic_alt_fpa.__logic_alt_fpa_20 (ite logic_alt_fpa.ni_54._arrow._first_m true false))
            (= logic_alt_fpa.ni_54._arrow._first_x false))
       (and (or (not (= logic_alt_fpa.__logic_alt_fpa_20 true))
               (= logic_alt_fpa.__logic_alt_fpa_24 true))
            (or (not (= logic_alt_fpa.__logic_alt_fpa_20 false))
               (= logic_alt_fpa.__logic_alt_fpa_24 (=> (= logic_alt_fpa.__logic_alt_fpa_23 1) (> logic_alt_fpa.__logic_alt_fpa_22_c 1))))
       )
       (and (= logic_alt_fpa.ni_53.since.__since_2_m logic_alt_fpa.ni_53.since.__since_2_c)
            (= logic_alt_fpa.ni_53.since.ni_78._arrow._first_m logic_alt_fpa.ni_53.since.ni_78._arrow._first_c)
            )
       (since_step logic_alt_fpa.in_fpa_engage
                   logic_alt_fpa.__logic_alt_fpa_21
                   logic_alt_fpa.ni_53.since.__since_2_m
                   logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_53.since.__since_2_x
                   logic_alt_fpa.ni_53.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.__logic_alt_fpa_22_x logic_alt_fpa.__logic_alt_fpa_21)
       (and (= logic_alt_fpa.ni_52.since.__since_2_m logic_alt_fpa.ni_52.since.__since_2_c)
            (= logic_alt_fpa.ni_52.since.ni_78._arrow._first_m logic_alt_fpa.ni_52.since.ni_78._arrow._first_c)
            )
       (since_step logic_alt_fpa.in_fpa_engage
                   logic_alt_fpa.__logic_alt_fpa_18
                   logic_alt_fpa.ni_52.since.__since_2_m
                   logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_52.since.__since_2_x
                   logic_alt_fpa.ni_52.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.ni_51._arrow._first_m logic_alt_fpa.ni_51._arrow._first_c)
       (and (= logic_alt_fpa.__logic_alt_fpa_15 (ite logic_alt_fpa.ni_51._arrow._first_m true false))
            (= logic_alt_fpa.ni_51._arrow._first_x false))
       (and (or (not (= logic_alt_fpa.__logic_alt_fpa_15 true))
               (= logic_alt_fpa.__logic_alt_fpa_19 true))
            (or (not (= logic_alt_fpa.__logic_alt_fpa_15 false))
               (= logic_alt_fpa.__logic_alt_fpa_19 (=> (= logic_alt_fpa.__logic_alt_fpa_18 1) (> logic_alt_fpa.__logic_alt_fpa_17_c 1))))
       )
       (and (= logic_alt_fpa.ni_50.since.__since_2_m logic_alt_fpa.ni_50.since.__since_2_c)
            (= logic_alt_fpa.ni_50.since.ni_78._arrow._first_m logic_alt_fpa.ni_50.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_alt_fpa.in_fpa_engage)
                   logic_alt_fpa.__logic_alt_fpa_16
                   logic_alt_fpa.ni_50.since.__since_2_m
                   logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_50.since.__since_2_x
                   logic_alt_fpa.ni_50.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.__logic_alt_fpa_17_x logic_alt_fpa.__logic_alt_fpa_16)
       (and (= logic_alt_fpa.ni_49.since.__since_2_m logic_alt_fpa.ni_49.since.__since_2_c)
            (= logic_alt_fpa.ni_49.since.ni_78._arrow._first_m logic_alt_fpa.ni_49.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_alt_fpa.in_alt_engage)
                   logic_alt_fpa.__logic_alt_fpa_13
                   logic_alt_fpa.ni_49.since.__since_2_m
                   logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_49.since.__since_2_x
                   logic_alt_fpa.ni_49.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.ni_48._arrow._first_m logic_alt_fpa.ni_48._arrow._first_c)
       (and (= logic_alt_fpa.__logic_alt_fpa_10 (ite logic_alt_fpa.ni_48._arrow._first_m true false))
            (= logic_alt_fpa.ni_48._arrow._first_x false))
       (and (or (not (= logic_alt_fpa.__logic_alt_fpa_10 true))
               (= logic_alt_fpa.__logic_alt_fpa_14 true))
            (or (not (= logic_alt_fpa.__logic_alt_fpa_10 false))
               (= logic_alt_fpa.__logic_alt_fpa_14 (=> (= logic_alt_fpa.__logic_alt_fpa_13 1) (> logic_alt_fpa.__logic_alt_fpa_12_c 1))))
       )
       (and (= logic_alt_fpa.ni_47.since.__since_2_m logic_alt_fpa.ni_47.since.__since_2_c)
            (= logic_alt_fpa.ni_47.since.ni_78._arrow._first_m logic_alt_fpa.ni_47.since.ni_78._arrow._first_c)
            )
       (since_step logic_alt_fpa.in_alt_engage
                   logic_alt_fpa.__logic_alt_fpa_11
                   logic_alt_fpa.ni_47.since.__since_2_m
                   logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                   logic_alt_fpa.ni_47.since.__since_2_x
                   logic_alt_fpa.ni_47.since.ni_78._arrow._first_x)
       (= logic_alt_fpa.__logic_alt_fpa_12_x logic_alt_fpa.__logic_alt_fpa_11)
       (and (= logic_alt_fpa.ni_46.duration.__duration_2_m logic_alt_fpa.ni_46.duration.__duration_2_c)
            (= logic_alt_fpa.ni_46.duration.__duration_4_m logic_alt_fpa.ni_46.duration.__duration_4_c)
            (= logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c)
            )
       (duration_step (not (or logic_alt_fpa.in_alt_engage logic_alt_fpa.in_fpa_engage))
                      logic_alt_fpa.__logic_alt_fpa_1
                      logic_alt_fpa.ni_46.duration.__duration_2_m
                      logic_alt_fpa.ni_46.duration.__duration_4_m
                      logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                      logic_alt_fpa.ni_46.duration.__duration_2_x
                      logic_alt_fpa.ni_46.duration.__duration_4_x
                      logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x)
       (= logic_alt_fpa.OK9 (=> logic_alt_fpa.fpa_tag_210 logic_alt_fpa.out_fpa_engaged))
       (= logic_alt_fpa.OK8 (=> logic_alt_fpa.fpa_wag_180 logic_alt_fpa.out_fpa_engaged))
       (= logic_alt_fpa.OK7 (=> logic_alt_fpa.alt_wag_170 logic_alt_fpa.out_alt_engaged))
       (= logic_alt_fpa.OK6 (=> (and (and (not (> logic_alt_fpa.__logic_alt_fpa_1 0)) (not logic_alt_fpa.alt_requested)) (not logic_alt_fpa.fpa_requested)) (not (or logic_alt_fpa.out_alt_engaged logic_alt_fpa.out_fpa_engaged))))
       (= logic_alt_fpa.OK5 (=> (> logic_alt_fpa.__logic_alt_fpa_1 0) (not (or logic_alt_fpa.out_alt_engaged logic_alt_fpa.out_fpa_engaged))))
       (= logic_alt_fpa.OK4 (not (and logic_alt_fpa.out_alt_engaged logic_alt_fpa.out_fpa_engaged)))
       (= logic_alt_fpa.OK10 (=> logic_alt_fpa.alt_tag_210 logic_alt_fpa.out_alt_engaged))
       )
 (and (=> logic_alt_fpa.__logic_alt_fpa_38 (not logic_alt_fpa.__logic_alt_fpa_37))
      (=> logic_alt_fpa.__logic_alt_fpa_36 (not logic_alt_fpa.__logic_alt_fpa_35))
      logic_alt_fpa.__logic_alt_fpa_34
      logic_alt_fpa.__logic_alt_fpa_29
      logic_alt_fpa.__logic_alt_fpa_24
      logic_alt_fpa.__logic_alt_fpa_19
      logic_alt_fpa.__logic_alt_fpa_14
      logic_alt_fpa.__logic_alt_fpa_9 ))
(logic_alt_fpa_step logic_alt_fpa.in_alt_engage logic_alt_fpa.in_fpa_engage logic_alt_fpa.in_disengage logic_alt_fpa.altitude logic_alt_fpa.altitude_target logic_alt_fpa.out_alt_engaged logic_alt_fpa.out_fpa_engaged logic_alt_fpa.alt_wag_170 logic_alt_fpa.alt_tag_210 logic_alt_fpa.fpa_wag_180 logic_alt_fpa.fpa_tag_210 logic_alt_fpa.OK4 logic_alt_fpa.OK5 logic_alt_fpa.OK6 logic_alt_fpa.OK7 logic_alt_fpa.OK8 logic_alt_fpa.OK9 logic_alt_fpa.OK10 logic_alt_fpa.__logic_alt_fpa_12_c logic_alt_fpa.__logic_alt_fpa_17_c logic_alt_fpa.__logic_alt_fpa_22_c logic_alt_fpa.__logic_alt_fpa_27_c logic_alt_fpa.__logic_alt_fpa_32_c logic_alt_fpa.__logic_alt_fpa_4_c logic_alt_fpa.__logic_alt_fpa_7_c logic_alt_fpa.ni_46.duration.__duration_2_c logic_alt_fpa.ni_46.duration.__duration_4_c logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c logic_alt_fpa.ni_47.since.__since_2_c logic_alt_fpa.ni_47.since.ni_78._arrow._first_c logic_alt_fpa.ni_48._arrow._first_c logic_alt_fpa.ni_49.since.__since_2_c logic_alt_fpa.ni_49.since.ni_78._arrow._first_c logic_alt_fpa.ni_50.since.__since_2_c logic_alt_fpa.ni_50.since.ni_78._arrow._first_c logic_alt_fpa.ni_51._arrow._first_c logic_alt_fpa.ni_52.since.__since_2_c logic_alt_fpa.ni_52.since.ni_78._arrow._first_c logic_alt_fpa.ni_53.since.__since_2_c logic_alt_fpa.ni_53.since.ni_78._arrow._first_c logic_alt_fpa.ni_54._arrow._first_c logic_alt_fpa.ni_55.since.__since_2_c logic_alt_fpa.ni_55.since.ni_78._arrow._first_c logic_alt_fpa.ni_56.since.__since_2_c logic_alt_fpa.ni_56.since.ni_78._arrow._first_c logic_alt_fpa.ni_57._arrow._first_c logic_alt_fpa.ni_58.since.__since_2_c logic_alt_fpa.ni_58.since.ni_78._arrow._first_c logic_alt_fpa.ni_59.since.__since_2_c logic_alt_fpa.ni_59.since.ni_78._arrow._first_c logic_alt_fpa.ni_60._arrow._first_c logic_alt_fpa.ni_61.since.__since_2_c logic_alt_fpa.ni_61.since.ni_78._arrow._first_c logic_alt_fpa.ni_62.fall.__fall_2_c logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c logic_alt_fpa.ni_63.fall.__fall_2_c logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c logic_alt_fpa.ni_64.fall.__fall_2_c logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c logic_alt_fpa.ni_65.fall.__fall_2_c logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c logic_alt_fpa.ni_66.since.__since_2_c logic_alt_fpa.ni_66.since.ni_78._arrow._first_c logic_alt_fpa.ni_67._arrow._first_c logic_alt_fpa.ni_68.since.__since_2_c logic_alt_fpa.ni_68.since.ni_78._arrow._first_c logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c logic_alt_fpa.ni_71._arrow._first_c logic_alt_fpa.__logic_alt_fpa_12_x logic_alt_fpa.__logic_alt_fpa_17_x logic_alt_fpa.__logic_alt_fpa_22_x logic_alt_fpa.__logic_alt_fpa_27_x logic_alt_fpa.__logic_alt_fpa_32_x logic_alt_fpa.__logic_alt_fpa_4_x logic_alt_fpa.__logic_alt_fpa_7_x logic_alt_fpa.ni_46.duration.__duration_2_x logic_alt_fpa.ni_46.duration.__duration_4_x logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x logic_alt_fpa.ni_47.since.__since_2_x logic_alt_fpa.ni_47.since.ni_78._arrow._first_x logic_alt_fpa.ni_48._arrow._first_x logic_alt_fpa.ni_49.since.__since_2_x logic_alt_fpa.ni_49.since.ni_78._arrow._first_x logic_alt_fpa.ni_50.since.__since_2_x logic_alt_fpa.ni_50.since.ni_78._arrow._first_x logic_alt_fpa.ni_51._arrow._first_x logic_alt_fpa.ni_52.since.__since_2_x logic_alt_fpa.ni_52.since.ni_78._arrow._first_x logic_alt_fpa.ni_53.since.__since_2_x logic_alt_fpa.ni_53.since.ni_78._arrow._first_x logic_alt_fpa.ni_54._arrow._first_x logic_alt_fpa.ni_55.since.__since_2_x logic_alt_fpa.ni_55.since.ni_78._arrow._first_x logic_alt_fpa.ni_56.since.__since_2_x logic_alt_fpa.ni_56.since.ni_78._arrow._first_x logic_alt_fpa.ni_57._arrow._first_x logic_alt_fpa.ni_58.since.__since_2_x logic_alt_fpa.ni_58.since.ni_78._arrow._first_x logic_alt_fpa.ni_59.since.__since_2_x logic_alt_fpa.ni_59.since.ni_78._arrow._first_x logic_alt_fpa.ni_60._arrow._first_x logic_alt_fpa.ni_61.since.__since_2_x logic_alt_fpa.ni_61.since.ni_78._arrow._first_x logic_alt_fpa.ni_62.fall.__fall_2_x logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x logic_alt_fpa.ni_63.fall.__fall_2_x logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x logic_alt_fpa.ni_64.fall.__fall_2_x logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x logic_alt_fpa.ni_65.fall.__fall_2_x logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x logic_alt_fpa.ni_66.since.__since_2_x logic_alt_fpa.ni_66.since.ni_78._arrow._first_x logic_alt_fpa.ni_67._arrow._first_x logic_alt_fpa.ni_68.since.__since_2_x logic_alt_fpa.ni_68.since.ni_78._arrow._first_x logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x logic_alt_fpa.ni_71._arrow._first_x)
))

; logic_heading
(declare-var logic_heading.in_engage Bool)
(declare-var logic_heading.in_disengage Bool)
(declare-var logic_heading.out_engaged Bool)
(declare-var logic_heading.OK1 Bool)
(declare-var logic_heading.OK2 Bool)
(declare-var logic_heading.OK3 Bool)
(declare-var logic_heading.__logic_heading_11_c Int)
(declare-var logic_heading.__logic_heading_14_c Int)
(declare-var logic_heading.__logic_heading_3_c Bool)
(declare-var logic_heading.__logic_heading_5_c Int)
(declare-var logic_heading.__logic_heading_8_c Int)
(declare-var logic_heading.ni_33.duration.__duration_2_c Int)
(declare-var logic_heading.ni_33.duration.__duration_4_c Bool)
(declare-var logic_heading.ni_33.duration.ni_81._arrow._first_c Bool)
(declare-var logic_heading.ni_34._arrow._first_c Bool)
(declare-var logic_heading.ni_35._arrow._first_c Bool)
(declare-var logic_heading.ni_36._arrow._first_c Bool)
(declare-var logic_heading.ni_37._arrow._first_c Bool)
(declare-var logic_heading.ni_38.fall.__fall_2_c Bool)
(declare-var logic_heading.ni_38.fall.ni_80._arrow._first_c Bool)
(declare-var logic_heading.ni_39.fall.__fall_2_c Bool)
(declare-var logic_heading.ni_39.fall.ni_80._arrow._first_c Bool)
(declare-var logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic_heading.ni_41._arrow._first_c Bool)
(declare-var logic_heading.ni_42.since.__since_2_c Int)
(declare-var logic_heading.ni_42.since.ni_78._arrow._first_c Bool)
(declare-var logic_heading.ni_43.since.__since_2_c Int)
(declare-var logic_heading.ni_43.since.ni_78._arrow._first_c Bool)
(declare-var logic_heading.ni_44.since.__since_2_c Int)
(declare-var logic_heading.ni_44.since.ni_78._arrow._first_c Bool)
(declare-var logic_heading.ni_45.since.__since_2_c Int)
(declare-var logic_heading.ni_45.since.ni_78._arrow._first_c Bool)
(declare-var logic_heading.__logic_heading_11_m Int)
(declare-var logic_heading.__logic_heading_14_m Int)
(declare-var logic_heading.__logic_heading_3_m Bool)
(declare-var logic_heading.__logic_heading_5_m Int)
(declare-var logic_heading.__logic_heading_8_m Int)
(declare-var logic_heading.ni_33.duration.__duration_2_m Int)
(declare-var logic_heading.ni_33.duration.__duration_4_m Bool)
(declare-var logic_heading.ni_33.duration.ni_81._arrow._first_m Bool)
(declare-var logic_heading.ni_34._arrow._first_m Bool)
(declare-var logic_heading.ni_35._arrow._first_m Bool)
(declare-var logic_heading.ni_36._arrow._first_m Bool)
(declare-var logic_heading.ni_37._arrow._first_m Bool)
(declare-var logic_heading.ni_38.fall.__fall_2_m Bool)
(declare-var logic_heading.ni_38.fall.ni_80._arrow._first_m Bool)
(declare-var logic_heading.ni_39.fall.__fall_2_m Bool)
(declare-var logic_heading.ni_39.fall.ni_80._arrow._first_m Bool)
(declare-var logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic_heading.ni_41._arrow._first_m Bool)
(declare-var logic_heading.ni_42.since.__since_2_m Int)
(declare-var logic_heading.ni_42.since.ni_78._arrow._first_m Bool)
(declare-var logic_heading.ni_43.since.__since_2_m Int)
(declare-var logic_heading.ni_43.since.ni_78._arrow._first_m Bool)
(declare-var logic_heading.ni_44.since.__since_2_m Int)
(declare-var logic_heading.ni_44.since.ni_78._arrow._first_m Bool)
(declare-var logic_heading.ni_45.since.__since_2_m Int)
(declare-var logic_heading.ni_45.since.ni_78._arrow._first_m Bool)
(declare-var logic_heading.__logic_heading_11_x Int)
(declare-var logic_heading.__logic_heading_14_x Int)
(declare-var logic_heading.__logic_heading_3_x Bool)
(declare-var logic_heading.__logic_heading_5_x Int)
(declare-var logic_heading.__logic_heading_8_x Int)
(declare-var logic_heading.ni_33.duration.__duration_2_x Int)
(declare-var logic_heading.ni_33.duration.__duration_4_x Bool)
(declare-var logic_heading.ni_33.duration.ni_81._arrow._first_x Bool)
(declare-var logic_heading.ni_34._arrow._first_x Bool)
(declare-var logic_heading.ni_35._arrow._first_x Bool)
(declare-var logic_heading.ni_36._arrow._first_x Bool)
(declare-var logic_heading.ni_37._arrow._first_x Bool)
(declare-var logic_heading.ni_38.fall.__fall_2_x Bool)
(declare-var logic_heading.ni_38.fall.ni_80._arrow._first_x Bool)
(declare-var logic_heading.ni_39.fall.__fall_2_x Bool)
(declare-var logic_heading.ni_39.fall.ni_80._arrow._first_x Bool)
(declare-var logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic_heading.ni_41._arrow._first_x Bool)
(declare-var logic_heading.ni_42.since.__since_2_x Int)
(declare-var logic_heading.ni_42.since.ni_78._arrow._first_x Bool)
(declare-var logic_heading.ni_43.since.__since_2_x Int)
(declare-var logic_heading.ni_43.since.ni_78._arrow._first_x Bool)
(declare-var logic_heading.ni_44.since.__since_2_x Int)
(declare-var logic_heading.ni_44.since.ni_78._arrow._first_x Bool)
(declare-var logic_heading.ni_45.since.__since_2_x Int)
(declare-var logic_heading.ni_45.since.ni_78._arrow._first_x Bool)
(declare-var logic_heading.__logic_heading_1 Int)
(declare-var logic_heading.__logic_heading_10 Bool)
(declare-var logic_heading.__logic_heading_12 Bool)
(declare-var logic_heading.__logic_heading_13 Bool)
(declare-var logic_heading.__logic_heading_15 Bool)
(declare-var logic_heading.__logic_heading_2 Bool)
(declare-var logic_heading.__logic_heading_4 Bool)
(declare-var logic_heading.__logic_heading_6 Bool)
(declare-var logic_heading.__logic_heading_7 Bool)
(declare-var logic_heading.__logic_heading_9 Bool)
(declare-var logic_heading.fall1 Bool)
(declare-var logic_heading.fall2 Bool)
(declare-var logic_heading.head_deactivation Bool)
(declare-var logic_heading.head_req Bool)
(declare-var logic_heading.since3 Int)
(declare-var logic_heading.since4 Int)
(declare-var logic_heading.since7 Int)
(declare-var logic_heading.since8 Int)
(declare-rel logic_heading_reset (Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool))
(declare-rel logic_heading_step (Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool))

(rule (=> 
  (and 
       (= logic_heading.__logic_heading_11_m logic_heading.__logic_heading_11_c)
       (= logic_heading.__logic_heading_14_m logic_heading.__logic_heading_14_c)
       (= logic_heading.__logic_heading_3_m logic_heading.__logic_heading_3_c)
       (= logic_heading.__logic_heading_5_m logic_heading.__logic_heading_5_c)
       (= logic_heading.__logic_heading_8_m logic_heading.__logic_heading_8_c)
       (since_reset logic_heading.ni_45.since.__since_2_c
                    logic_heading.ni_45.since.ni_78._arrow._first_c
                    logic_heading.ni_45.since.__since_2_m
                    logic_heading.ni_45.since.ni_78._arrow._first_m)
       (since_reset logic_heading.ni_44.since.__since_2_c
                    logic_heading.ni_44.since.ni_78._arrow._first_c
                    logic_heading.ni_44.since.__since_2_m
                    logic_heading.ni_44.since.ni_78._arrow._first_m)
       (since_reset logic_heading.ni_43.since.__since_2_c
                    logic_heading.ni_43.since.ni_78._arrow._first_c
                    logic_heading.ni_43.since.__since_2_m
                    logic_heading.ni_43.since.ni_78._arrow._first_m)
       (since_reset logic_heading.ni_42.since.__since_2_c
                    logic_heading.ni_42.since.ni_78._arrow._first_c
                    logic_heading.ni_42.since.__since_2_m
                    logic_heading.ni_42.since.ni_78._arrow._first_m)
       (= logic_heading.ni_41._arrow._first_m true)
       (has_been_true_reset_reset logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
                                  logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
                                  logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                                  logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m)
       (fall_reset logic_heading.ni_39.fall.__fall_2_c
                   logic_heading.ni_39.fall.ni_80._arrow._first_c
                   logic_heading.ni_39.fall.__fall_2_m
                   logic_heading.ni_39.fall.ni_80._arrow._first_m)
       (fall_reset logic_heading.ni_38.fall.__fall_2_c
                   logic_heading.ni_38.fall.ni_80._arrow._first_c
                   logic_heading.ni_38.fall.__fall_2_m
                   logic_heading.ni_38.fall.ni_80._arrow._first_m)
       (= logic_heading.ni_37._arrow._first_m true)
       (= logic_heading.ni_36._arrow._first_m true)
       (= logic_heading.ni_35._arrow._first_m true)
       (= logic_heading.ni_34._arrow._first_m true)
       (duration_reset logic_heading.ni_33.duration.__duration_2_c
                       logic_heading.ni_33.duration.__duration_4_c
                       logic_heading.ni_33.duration.ni_81._arrow._first_c
                       logic_heading.ni_33.duration.__duration_2_m
                       logic_heading.ni_33.duration.__duration_4_m
                       logic_heading.ni_33.duration.ni_81._arrow._first_m)
  )
  (logic_heading_reset logic_heading.__logic_heading_11_c
                       logic_heading.__logic_heading_14_c
                       logic_heading.__logic_heading_3_c
                       logic_heading.__logic_heading_5_c
                       logic_heading.__logic_heading_8_c
                       logic_heading.ni_33.duration.__duration_2_c
                       logic_heading.ni_33.duration.__duration_4_c
                       logic_heading.ni_33.duration.ni_81._arrow._first_c
                       logic_heading.ni_34._arrow._first_c
                       logic_heading.ni_35._arrow._first_c
                       logic_heading.ni_36._arrow._first_c
                       logic_heading.ni_37._arrow._first_c
                       logic_heading.ni_38.fall.__fall_2_c
                       logic_heading.ni_38.fall.ni_80._arrow._first_c
                       logic_heading.ni_39.fall.__fall_2_c
                       logic_heading.ni_39.fall.ni_80._arrow._first_c
                       logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
                       logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
                       logic_heading.ni_41._arrow._first_c
                       logic_heading.ni_42.since.__since_2_c
                       logic_heading.ni_42.since.ni_78._arrow._first_c
                       logic_heading.ni_43.since.__since_2_c
                       logic_heading.ni_43.since.ni_78._arrow._first_c
                       logic_heading.ni_44.since.__since_2_c
                       logic_heading.ni_44.since.ni_78._arrow._first_c
                       logic_heading.ni_45.since.__since_2_c
                       logic_heading.ni_45.since.ni_78._arrow._first_c
                       logic_heading.__logic_heading_11_m
                       logic_heading.__logic_heading_14_m
                       logic_heading.__logic_heading_3_m
                       logic_heading.__logic_heading_5_m
                       logic_heading.__logic_heading_8_m
                       logic_heading.ni_33.duration.__duration_2_m
                       logic_heading.ni_33.duration.__duration_4_m
                       logic_heading.ni_33.duration.ni_81._arrow._first_m
                       logic_heading.ni_34._arrow._first_m
                       logic_heading.ni_35._arrow._first_m
                       logic_heading.ni_36._arrow._first_m
                       logic_heading.ni_37._arrow._first_m
                       logic_heading.ni_38.fall.__fall_2_m
                       logic_heading.ni_38.fall.ni_80._arrow._first_m
                       logic_heading.ni_39.fall.__fall_2_m
                       logic_heading.ni_39.fall.ni_80._arrow._first_m
                       logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                       logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                       logic_heading.ni_41._arrow._first_m
                       logic_heading.ni_42.since.__since_2_m
                       logic_heading.ni_42.since.ni_78._arrow._first_m
                       logic_heading.ni_43.since.__since_2_m
                       logic_heading.ni_43.since.ni_78._arrow._first_m
                       logic_heading.ni_44.since.__since_2_m
                       logic_heading.ni_44.since.ni_78._arrow._first_m
                       logic_heading.ni_45.since.__since_2_m
                       logic_heading.ni_45.since.ni_78._arrow._first_m)
))

; Step rule with Assertions 
(rule (=> 
  (and 
  (and (and (= logic_heading.ni_45.since.__since_2_m logic_heading.ni_45.since.__since_2_c)
            (= logic_heading.ni_45.since.ni_78._arrow._first_m logic_heading.ni_45.since.ni_78._arrow._first_c)
            )
       (since_step logic_heading.in_disengage
                   logic_heading.since8
                   logic_heading.ni_45.since.__since_2_m
                   logic_heading.ni_45.since.ni_78._arrow._first_m
                   logic_heading.ni_45.since.__since_2_x
                   logic_heading.ni_45.since.ni_78._arrow._first_x)
       (and (= logic_heading.ni_44.since.__since_2_m logic_heading.ni_44.since.__since_2_c)
            (= logic_heading.ni_44.since.ni_78._arrow._first_m logic_heading.ni_44.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_heading.in_disengage)
                   logic_heading.since7
                   logic_heading.ni_44.since.__since_2_m
                   logic_heading.ni_44.since.ni_78._arrow._first_m
                   logic_heading.ni_44.since.__since_2_x
                   logic_heading.ni_44.since.ni_78._arrow._first_x)
       (and (= logic_heading.ni_43.since.__since_2_m logic_heading.ni_43.since.__since_2_c)
            (= logic_heading.ni_43.since.ni_78._arrow._first_m logic_heading.ni_43.since.ni_78._arrow._first_c)
            )
       (since_step logic_heading.in_engage
                   logic_heading.since4
                   logic_heading.ni_43.since.__since_2_m
                   logic_heading.ni_43.since.ni_78._arrow._first_m
                   logic_heading.ni_43.since.__since_2_x
                   logic_heading.ni_43.since.ni_78._arrow._first_x)
       (and (= logic_heading.ni_42.since.__since_2_m logic_heading.ni_42.since.__since_2_c)
            (= logic_heading.ni_42.since.ni_78._arrow._first_m logic_heading.ni_42.since.ni_78._arrow._first_c)
            )
       (since_step (not logic_heading.in_engage)
                   logic_heading.since3
                   logic_heading.ni_42.since.__since_2_m
                   logic_heading.ni_42.since.ni_78._arrow._first_m
                   logic_heading.ni_42.since.__since_2_x
                   logic_heading.ni_42.since.ni_78._arrow._first_x)
       (= logic_heading.ni_41._arrow._first_m logic_heading.ni_41._arrow._first_c)
       (and (= logic_heading.__logic_heading_2 (ite logic_heading.ni_41._arrow._first_m true false))
            (= logic_heading.ni_41._arrow._first_x false))
       (and (or (not (= logic_heading.__logic_heading_2 true))
               (= logic_heading.head_deactivation false))
            (or (not (= logic_heading.__logic_heading_2 false))
               (= logic_heading.head_deactivation logic_heading.__logic_heading_3_c))
       )
       (and (= logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c)
            )
       (has_been_true_reset_step logic_heading.in_engage
                                 (and logic_heading.head_deactivation (not logic_heading.in_engage))
                                 logic_heading.head_req
                                 logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                                 logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                                 logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x
                                 logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x)
       (and (= logic_heading.ni_39.fall.__fall_2_m logic_heading.ni_39.fall.__fall_2_c)
            (= logic_heading.ni_39.fall.ni_80._arrow._first_m logic_heading.ni_39.fall.ni_80._arrow._first_c)
            )
       (fall_step logic_heading.in_disengage
                  logic_heading.fall2
                  logic_heading.ni_39.fall.__fall_2_m
                  logic_heading.ni_39.fall.ni_80._arrow._first_m
                  logic_heading.ni_39.fall.__fall_2_x
                  logic_heading.ni_39.fall.ni_80._arrow._first_x)
       (and (= logic_heading.ni_38.fall.__fall_2_m logic_heading.ni_38.fall.__fall_2_c)
            (= logic_heading.ni_38.fall.ni_80._arrow._first_m logic_heading.ni_38.fall.ni_80._arrow._first_c)
            )
       (fall_step logic_heading.in_engage
                  logic_heading.fall1
                  logic_heading.ni_38.fall.__fall_2_m
                  logic_heading.ni_38.fall.ni_80._arrow._first_m
                  logic_heading.ni_38.fall.__fall_2_x
                  logic_heading.ni_38.fall.ni_80._arrow._first_x)
       (= logic_heading.ni_37._arrow._first_m logic_heading.ni_37._arrow._first_c)
       (and (= logic_heading.__logic_heading_7 (ite logic_heading.ni_37._arrow._first_m true false))
            (= logic_heading.ni_37._arrow._first_x false))
       (and (or (not (= logic_heading.__logic_heading_7 true))
               (= logic_heading.__logic_heading_9 true))
            (or (not (= logic_heading.__logic_heading_7 false))
               (= logic_heading.__logic_heading_9 (=> (= logic_heading.since3 1) (> logic_heading.__logic_heading_8_c 1))))
       )
       (= logic_heading.__logic_heading_8_x logic_heading.since4)
       (= logic_heading.ni_36._arrow._first_m logic_heading.ni_36._arrow._first_c)
       (and (= logic_heading.__logic_heading_4 (ite logic_heading.ni_36._arrow._first_m true false))
            (= logic_heading.ni_36._arrow._first_x false))
       (and (or (not (= logic_heading.__logic_heading_4 true))
               (= logic_heading.__logic_heading_6 true))
            (or (not (= logic_heading.__logic_heading_4 false))
               (= logic_heading.__logic_heading_6 (=> (= logic_heading.since4 1) (> logic_heading.__logic_heading_5_c 1))))
       )
       (= logic_heading.__logic_heading_5_x logic_heading.since3)
       (= logic_heading.__logic_heading_3_x logic_heading.in_disengage)
       (= logic_heading.ni_35._arrow._first_m logic_heading.ni_35._arrow._first_c)
       (and (= logic_heading.__logic_heading_13 (ite logic_heading.ni_35._arrow._first_m true false))
            (= logic_heading.ni_35._arrow._first_x false))
       (and (or (not (= logic_heading.__logic_heading_13 true))
               (= logic_heading.__logic_heading_15 true))
            (or (not (= logic_heading.__logic_heading_13 false))
               (= logic_heading.__logic_heading_15 (=> (= logic_heading.since7 1) (> logic_heading.__logic_heading_14_c 1))))
       )
       (= logic_heading.__logic_heading_14_x logic_heading.since8)
       (= logic_heading.ni_34._arrow._first_m logic_heading.ni_34._arrow._first_c)
       (and (= logic_heading.__logic_heading_10 (ite logic_heading.ni_34._arrow._first_m true false))
            (= logic_heading.ni_34._arrow._first_x false))
       (and (or (not (= logic_heading.__logic_heading_10 true))
               (= logic_heading.__logic_heading_12 true))
            (or (not (= logic_heading.__logic_heading_10 false))
               (= logic_heading.__logic_heading_12 (=> (= logic_heading.since8 1) (> logic_heading.__logic_heading_11_c 1))))
       )
       (= logic_heading.__logic_heading_11_x logic_heading.since7)
       (and (= logic_heading.ni_33.duration.__duration_2_m logic_heading.ni_33.duration.__duration_2_c)
            (= logic_heading.ni_33.duration.__duration_4_m logic_heading.ni_33.duration.__duration_4_c)
            (= logic_heading.ni_33.duration.ni_81._arrow._first_m logic_heading.ni_33.duration.ni_81._arrow._first_c)
            )
       (duration_step (not logic_heading.in_engage)
                      logic_heading.__logic_heading_1
                      logic_heading.ni_33.duration.__duration_2_m
                      logic_heading.ni_33.duration.__duration_4_m
                      logic_heading.ni_33.duration.ni_81._arrow._first_m
                      logic_heading.ni_33.duration.__duration_2_x
                      logic_heading.ni_33.duration.__duration_4_x
                      logic_heading.ni_33.duration.ni_81._arrow._first_x)
       (= logic_heading.OK3 (=> logic_heading.head_req logic_heading.out_engaged))
       (= logic_heading.OK2 (=> (and (and (not (> logic_heading.__logic_heading_1 0)) (not logic_heading.head_req)) (not logic_heading.in_engage)) (not logic_heading.out_engaged)))
       (= logic_heading.OK1 (=> (> logic_heading.__logic_heading_1 0) (not logic_heading.out_engaged)))
       )
 (and (not (and logic_heading.fall1 logic_heading.fall2))
      logic_heading.__logic_heading_15
      logic_heading.__logic_heading_12
      logic_heading.__logic_heading_9
      logic_heading.__logic_heading_6 ))
(logic_heading_step logic_heading.in_engage logic_heading.in_disengage logic_heading.out_engaged logic_heading.OK1 logic_heading.OK2 logic_heading.OK3 logic_heading.__logic_heading_11_c logic_heading.__logic_heading_14_c logic_heading.__logic_heading_3_c logic_heading.__logic_heading_5_c logic_heading.__logic_heading_8_c logic_heading.ni_33.duration.__duration_2_c logic_heading.ni_33.duration.__duration_4_c logic_heading.ni_33.duration.ni_81._arrow._first_c logic_heading.ni_34._arrow._first_c logic_heading.ni_35._arrow._first_c logic_heading.ni_36._arrow._first_c logic_heading.ni_37._arrow._first_c logic_heading.ni_38.fall.__fall_2_c logic_heading.ni_38.fall.ni_80._arrow._first_c logic_heading.ni_39.fall.__fall_2_c logic_heading.ni_39.fall.ni_80._arrow._first_c logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c logic_heading.ni_41._arrow._first_c logic_heading.ni_42.since.__since_2_c logic_heading.ni_42.since.ni_78._arrow._first_c logic_heading.ni_43.since.__since_2_c logic_heading.ni_43.since.ni_78._arrow._first_c logic_heading.ni_44.since.__since_2_c logic_heading.ni_44.since.ni_78._arrow._first_c logic_heading.ni_45.since.__since_2_c logic_heading.ni_45.since.ni_78._arrow._first_c logic_heading.__logic_heading_11_x logic_heading.__logic_heading_14_x logic_heading.__logic_heading_3_x logic_heading.__logic_heading_5_x logic_heading.__logic_heading_8_x logic_heading.ni_33.duration.__duration_2_x logic_heading.ni_33.duration.__duration_4_x logic_heading.ni_33.duration.ni_81._arrow._first_x logic_heading.ni_34._arrow._first_x logic_heading.ni_35._arrow._first_x logic_heading.ni_36._arrow._first_x logic_heading.ni_37._arrow._first_x logic_heading.ni_38.fall.__fall_2_x logic_heading.ni_38.fall.ni_80._arrow._first_x logic_heading.ni_39.fall.__fall_2_x logic_heading.ni_39.fall.ni_80._arrow._first_x logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x logic_heading.ni_41._arrow._first_x logic_heading.ni_42.since.__since_2_x logic_heading.ni_42.since.ni_78._arrow._first_x logic_heading.ni_43.since.__since_2_x logic_heading.ni_43.since.ni_78._arrow._first_x logic_heading.ni_44.since.__since_2_x logic_heading.ni_44.since.ni_78._arrow._first_x logic_heading.ni_45.since.__since_2_x logic_heading.ni_45.since.ni_78._arrow._first_x)
))

; PseudoContinuous
(declare-var PseudoContinuous.in Real)
(declare-var PseudoContinuous.inc Real)
(declare-var PseudoContinuous.out Bool)
(declare-var PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var PseudoContinuous.__PseudoContinuous_1 Bool)
(declare-var PseudoContinuous.__PseudoContinuous_3 Real)
(declare-var PseudoContinuous.__PseudoContinuous_4 Real)
(declare-rel PseudoContinuous_reset (Real Bool Real Bool))
(declare-rel PseudoContinuous_step (Real Real Bool Real Bool Real Bool))

(rule (=> 
  (and 
       (= PseudoContinuous.__PseudoContinuous_2_m PseudoContinuous.__PseudoContinuous_2_c)
       (= PseudoContinuous.ni_32._arrow._first_m true)
  )
  (PseudoContinuous_reset PseudoContinuous.__PseudoContinuous_2_c
                          PseudoContinuous.ni_32._arrow._first_c
                          PseudoContinuous.__PseudoContinuous_2_m
                          PseudoContinuous.ni_32._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= PseudoContinuous.ni_32._arrow._first_m PseudoContinuous.ni_32._arrow._first_c)
       (and (= PseudoContinuous.__PseudoContinuous_1 (ite PseudoContinuous.ni_32._arrow._first_m true false))
            (= PseudoContinuous.ni_32._arrow._first_x false))
       (and (or (not (= PseudoContinuous.__PseudoContinuous_1 true))
               (= PseudoContinuous.__PseudoContinuous_3 PseudoContinuous.in))
            (or (not (= PseudoContinuous.__PseudoContinuous_1 false))
               (= PseudoContinuous.__PseudoContinuous_3 PseudoContinuous.__PseudoContinuous_2_c))
       )
       (abs_real_fun (- PseudoContinuous.in PseudoContinuous.__PseudoContinuous_3)
                     PseudoContinuous.__PseudoContinuous_4)
       (= PseudoContinuous.out (<= PseudoContinuous.__PseudoContinuous_4 PseudoContinuous.inc))
       (= PseudoContinuous.__PseudoContinuous_2_x PseudoContinuous.in)
       )
  (PseudoContinuous_step PseudoContinuous.in
                         PseudoContinuous.inc
                         PseudoContinuous.out
                         PseudoContinuous.__PseudoContinuous_2_c
                         PseudoContinuous.ni_32._arrow._first_c
                         PseudoContinuous.__PseudoContinuous_2_x
                         PseudoContinuous.ni_32._arrow._first_x)
))

; LONGITUDINAL_CONTROLLER
(declare-var LONGITUDINAL_CONTROLLER.FPACmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.AltEng_1_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER.FpaEng_1_1 Bool)
(declare-var LONGITUDINAL_CONTROLLER.AltCmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.Alt_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.thetaDeg_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.qDeg_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.GsKts_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.hdot_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.VT_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.Gamma_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.CAS_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.ElevStick_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.d_out1 Real)
(declare-var LONGITUDINAL_CONTROLLER.d_out2 Real)
(declare-var LONGITUDINAL_CONTROLLER.t_out3 Real)
(declare-var LONGITUDINAL_CONTROLLER.t_out4 Real)
(declare-var LONGITUDINAL_CONTROLLER.d_out4 Real)
(declare-var LONGITUDINAL_CONTROLLER.t_out5 Real)
(declare-var LONGITUDINAL_CONTROLLER.d_out5 Real)
(declare-var LONGITUDINAL_CONTROLLER.t_out6 Real)
(declare-var LONGITUDINAL_CONTROLLER.d_out6 Real)
(declare-var LONGITUDINAL_CONTROLLER.alt_cmd_out Real)
(declare-var LONGITUDINAL_CONTROLLER.pitch_cmd_out Real)
(declare-var LONGITUDINAL_CONTROLLER.ElevCmd_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x Bool)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x Real)
(declare-var LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x Bool)
(declare-var LONGITUDINAL_CONTROLLER.AltitudeControl_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.EngageORzero_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.FPAControl_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.ManualOverride_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.PitchInnerLoop_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.Sum7_1_1 Real)
(declare-var LONGITUDINAL_CONTROLLER.k2_1_1 Real)
(declare-rel LONGITUDINAL_CONTROLLER_reset (Real Bool Bool Real Real Bool Bool Real Real Bool Real Bool Bool Real Real Bool Bool Real Real Bool))
(declare-rel LONGITUDINAL_CONTROLLER_step (Real Bool Bool Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Real Real Bool Bool Real Real Bool Real Bool Bool Real Real Bool Bool Real Real Bool))

(rule (=> 
  (and 
       
       (LONGITUDINAL_CONTROLLER_AltitudeControl_reset LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                                      LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                                      LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                                      LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                                      LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                                      LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                                      LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                                      LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m)
       (LONGITUDINAL_CONTROLLER_FPAControl_reset LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m)
       (LONGITUDINAL_CONTROLLER_PitchInnerLoop_reset LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                                     LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                                     LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                                     LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m)
  )
  (LONGITUDINAL_CONTROLLER_reset LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                 LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                 LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                 LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                 LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                 LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (and (= LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c)
            (= LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c)
            (= LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c)
            (= LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c)
            )
       (LONGITUDINAL_CONTROLLER_AltitudeControl_step LONGITUDINAL_CONTROLLER.AltEng_1_1
                                                     LONGITUDINAL_CONTROLLER.AltCmd_1_1
                                                     LONGITUDINAL_CONTROLLER.Alt_1_1
                                                     LONGITUDINAL_CONTROLLER.GsKts_1_1
                                                     LONGITUDINAL_CONTROLLER.hdot_1_1
                                                     LONGITUDINAL_CONTROLLER.d_out1
                                                     LONGITUDINAL_CONTROLLER.d_out2
                                                     LONGITUDINAL_CONTROLLER.AltitudeControl_1_1
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
                                                     LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x)
       (= LONGITUDINAL_CONTROLLER.k2_1_1 (* 1.0000000000 LONGITUDINAL_CONTROLLER.AltitudeControl_1_1))
       (= LONGITUDINAL_CONTROLLER.Sum7_1_1 (+ LONGITUDINAL_CONTROLLER.FPACmd_1_1 LONGITUDINAL_CONTROLLER.k2_1_1))
       (and (= LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c)
            (= LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c)
            (= LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c)
            (= LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c)
            )
       (LONGITUDINAL_CONTROLLER_FPAControl_step LONGITUDINAL_CONTROLLER.FpaEng_1_1
                                                LONGITUDINAL_CONTROLLER.Sum7_1_1
                                                LONGITUDINAL_CONTROLLER.Gamma_1_1
                                                LONGITUDINAL_CONTROLLER.thetaDeg_1_1
                                                LONGITUDINAL_CONTROLLER.VT_1_1
                                                LONGITUDINAL_CONTROLLER.FPAControl_1_1
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
                                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x)
       (and (or (not (= (or (>= LONGITUDINAL_CONTROLLER.Sum7_1_1 LONGITUDINAL_CONTROLLER.FPACmd_1_1) (not (>= LONGITUDINAL_CONTROLLER.Sum7_1_1 LONGITUDINAL_CONTROLLER.FPACmd_1_1))) true))
               (and (or (not (= (or (>= LONGITUDINAL_CONTROLLER.k2_1_1 0.0) (not (>= LONGITUDINAL_CONTROLLER.k2_1_1 0.0))) true))
                       (= LONGITUDINAL_CONTROLLER.pitch_cmd_out LONGITUDINAL_CONTROLLER.FPAControl_1_1))
                    (or (not (= (or (>= LONGITUDINAL_CONTROLLER.k2_1_1 0.0) (not (>= LONGITUDINAL_CONTROLLER.k2_1_1 0.0))) false))
                       (= LONGITUDINAL_CONTROLLER.pitch_cmd_out 0.0))
               ))
            (or (not (= (or (>= LONGITUDINAL_CONTROLLER.Sum7_1_1 LONGITUDINAL_CONTROLLER.FPACmd_1_1) (not (>= LONGITUDINAL_CONTROLLER.Sum7_1_1 LONGITUDINAL_CONTROLLER.FPACmd_1_1))) false))
               (= LONGITUDINAL_CONTROLLER.pitch_cmd_out 0.0))
       )
       (= LONGITUDINAL_CONTROLLER.alt_cmd_out LONGITUDINAL_CONTROLLER.AltitudeControl_1_1)
       (and (= LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c)
            (= LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c)
            )
       (LONGITUDINAL_CONTROLLER_PitchInnerLoop_step (* 1.0000000000 LONGITUDINAL_CONTROLLER.FPAControl_1_1)
                                                    LONGITUDINAL_CONTROLLER.thetaDeg_1_1
                                                    LONGITUDINAL_CONTROLLER.qDeg_1_1
                                                    LONGITUDINAL_CONTROLLER.CAS_1_1
                                                    LONGITUDINAL_CONTROLLER.t_out3
                                                    LONGITUDINAL_CONTROLLER.t_out4
                                                    LONGITUDINAL_CONTROLLER.d_out4
                                                    LONGITUDINAL_CONTROLLER.t_out5
                                                    LONGITUDINAL_CONTROLLER.d_out5
                                                    LONGITUDINAL_CONTROLLER.t_out6
                                                    LONGITUDINAL_CONTROLLER.d_out6
                                                    LONGITUDINAL_CONTROLLER.PitchInnerLoop_1_1
                                                    LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                                    LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                                    LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
                                                    LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x)
       (LONGITUDINAL_CONTROLLER_EngageORzero_fun (or LONGITUDINAL_CONTROLLER.AltEng_1_1 LONGITUDINAL_CONTROLLER.FpaEng_1_1)
                                                 LONGITUDINAL_CONTROLLER.PitchInnerLoop_1_1
                                                 LONGITUDINAL_CONTROLLER.EngageORzero_1_1)
       (LONGITUDINAL_CONTROLLER_ManualOverride_fun LONGITUDINAL_CONTROLLER.ElevStick_1_1
                                                   LONGITUDINAL_CONTROLLER.EngageORzero_1_1
                                                   LONGITUDINAL_CONTROLLER.ManualOverride_1_1)
       (= LONGITUDINAL_CONTROLLER.ElevCmd_1_1 LONGITUDINAL_CONTROLLER.ManualOverride_1_1)
       )
  (LONGITUDINAL_CONTROLLER_step LONGITUDINAL_CONTROLLER.FPACmd_1_1
                                LONGITUDINAL_CONTROLLER.AltEng_1_1
                                LONGITUDINAL_CONTROLLER.FpaEng_1_1
                                LONGITUDINAL_CONTROLLER.AltCmd_1_1
                                LONGITUDINAL_CONTROLLER.Alt_1_1
                                LONGITUDINAL_CONTROLLER.thetaDeg_1_1
                                LONGITUDINAL_CONTROLLER.qDeg_1_1
                                LONGITUDINAL_CONTROLLER.GsKts_1_1
                                LONGITUDINAL_CONTROLLER.hdot_1_1
                                LONGITUDINAL_CONTROLLER.VT_1_1
                                LONGITUDINAL_CONTROLLER.Gamma_1_1
                                LONGITUDINAL_CONTROLLER.CAS_1_1
                                LONGITUDINAL_CONTROLLER.ElevStick_1_1
                                LONGITUDINAL_CONTROLLER.d_out1
                                LONGITUDINAL_CONTROLLER.d_out2
                                LONGITUDINAL_CONTROLLER.t_out3
                                LONGITUDINAL_CONTROLLER.t_out4
                                LONGITUDINAL_CONTROLLER.d_out4
                                LONGITUDINAL_CONTROLLER.t_out5
                                LONGITUDINAL_CONTROLLER.d_out5
                                LONGITUDINAL_CONTROLLER.t_out6
                                LONGITUDINAL_CONTROLLER.d_out6
                                LONGITUDINAL_CONTROLLER.alt_cmd_out
                                LONGITUDINAL_CONTROLLER.pitch_cmd_out
                                LONGITUDINAL_CONTROLLER.ElevCmd_1_1
                                LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
                                LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
                                LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
                                LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x)
))

; MODE_LOGIC
(declare-var MODE_LOGIC.HeadMode_1_1 Real)
(declare-var MODE_LOGIC.ailStick_1_1 Real)
(declare-var MODE_LOGIC.elevStick_1_1 Real)
(declare-var MODE_LOGIC.AltMode_1_1 Real)
(declare-var MODE_LOGIC.FPAMode_1_1 Real)
(declare-var MODE_LOGIC.ATMode_1_1 Real)
(declare-var MODE_LOGIC.AltCmd_1_1 Real)
(declare-var MODE_LOGIC.Altitude_1_1 Real)
(declare-var MODE_LOGIC.CAS_1_1 Real)
(declare-var MODE_LOGIC.CASCmdMCP_1_1 Real)
(declare-var MODE_LOGIC.HeadEng_1_1 Bool)
(declare-var MODE_LOGIC.AltEng_2_1 Bool)
(declare-var MODE_LOGIC.FPAEng_3_1 Bool)
(declare-var MODE_LOGIC.ATEng_4_1 Bool)
(declare-var MODE_LOGIC.CASCmd_5_1 Real)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c Real)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m Real)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x Bool)
(declare-var MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x Real)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x Bool)
(declare-var MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x Bool)
(declare-var MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x Bool)
(declare-var MODE_LOGIC.AltAndFPAMode_1_1 Bool)
(declare-var MODE_LOGIC.AltAndFPAMode_2_1 Bool)
(declare-var MODE_LOGIC.HeadingMode_1_1 Bool)
(declare-var MODE_LOGIC.LogicalOperator_1_1 Bool)
(declare-var MODE_LOGIC.SpeedMode_1_1 Bool)
(declare-var MODE_LOGIC.SpeedMode_2_1 Real)
(declare-rel MODE_LOGIC_reset (Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool))
(declare-rel MODE_LOGIC_step (Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Real Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       
       (MODE_LOGIC_AltAndFPAMode_reset MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                                       MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
       (MODE_LOGIC_SpeedMode_reset MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m)
       (MODE_LOGIC_HeadingMode_reset MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                                     MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                                     MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                                     MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                                     MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                                     MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m)
  )
  (MODE_LOGIC_reset MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                    MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                    MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (= MODE_LOGIC.LogicalOperator_1_1 (or (not (= MODE_LOGIC.ailStick_1_1 0.0000000000)) (not (= MODE_LOGIC.elevStick_1_1 0.0000000000))))
       (and (= MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c)
            (= MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c)
            (= MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c)
            (= MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c)
            (= MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c)
            )
       (MODE_LOGIC_AltAndFPAMode_step MODE_LOGIC.FPAMode_1_1
                                      MODE_LOGIC.LogicalOperator_1_1
                                      MODE_LOGIC.Altitude_1_1
                                      MODE_LOGIC.AltCmd_1_1
                                      MODE_LOGIC.AltMode_1_1
                                      MODE_LOGIC.AltAndFPAMode_1_1
                                      MODE_LOGIC.AltAndFPAMode_2_1
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x
                                      MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
       (and (= MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c)
            (= MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c)
            (= MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c)
            (= MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c)
            )
       (MODE_LOGIC_SpeedMode_step MODE_LOGIC.ATMode_1_1
                                  0.0000000000
                                  MODE_LOGIC.AltAndFPAMode_2_1
                                  MODE_LOGIC.CAS_1_1
                                  MODE_LOGIC.CASCmdMCP_1_1
                                  MODE_LOGIC.SpeedMode_1_1
                                  MODE_LOGIC.SpeedMode_2_1
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x
                                  MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x)
       (and (= MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c)
            (= MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c)
            (= MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c)
            )
       (MODE_LOGIC_HeadingMode_step MODE_LOGIC.HeadMode_1_1
                                    MODE_LOGIC.LogicalOperator_1_1
                                    MODE_LOGIC.HeadingMode_1_1
                                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
                                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x
                                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x
                                    MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x)
       (= MODE_LOGIC.HeadEng_1_1 MODE_LOGIC.HeadingMode_1_1)
       (= MODE_LOGIC.FPAEng_3_1 MODE_LOGIC.AltAndFPAMode_2_1)
       (= MODE_LOGIC.CASCmd_5_1 MODE_LOGIC.SpeedMode_2_1)
       (= MODE_LOGIC.AltEng_2_1 MODE_LOGIC.AltAndFPAMode_1_1)
       (= MODE_LOGIC.ATEng_4_1 MODE_LOGIC.SpeedMode_1_1)
       )
  (MODE_LOGIC_step MODE_LOGIC.HeadMode_1_1
                   MODE_LOGIC.ailStick_1_1
                   MODE_LOGIC.elevStick_1_1
                   MODE_LOGIC.AltMode_1_1
                   MODE_LOGIC.FPAMode_1_1
                   MODE_LOGIC.ATMode_1_1
                   MODE_LOGIC.AltCmd_1_1
                   MODE_LOGIC.Altitude_1_1
                   MODE_LOGIC.CAS_1_1
                   MODE_LOGIC.CASCmdMCP_1_1
                   MODE_LOGIC.HeadEng_1_1
                   MODE_LOGIC.AltEng_2_1
                   MODE_LOGIC.FPAEng_3_1
                   MODE_LOGIC.ATEng_4_1
                   MODE_LOGIC.CASCmd_5_1
                   MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                   MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                   MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                   MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x
                   MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x
                   MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x
                   MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x
                   MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
))

; logic
(declare-var logic.in_head_engage Bool)
(declare-var logic.in_alt_engage Bool)
(declare-var logic.in_fpa_engage Bool)
(declare-var logic.in_disengage Bool)
(declare-var logic.in_alt Real)
(declare-var logic.in_alt_target Real)
(declare-var logic.out_head_engaged Bool)
(declare-var logic.out_alt_engaged Bool)
(declare-var logic.out_fpa_engaged Bool)
(declare-var logic.alt_wag_170 Bool)
(declare-var logic.alt_tag_210 Bool)
(declare-var logic.fpa_wag_180 Bool)
(declare-var logic.fpa_tag_210 Bool)
(declare-var logic.OK1 Bool)
(declare-var logic.OK2 Bool)
(declare-var logic.OK3 Bool)
(declare-var logic.OK4 Bool)
(declare-var logic.OK5 Bool)
(declare-var logic.OK6 Bool)
(declare-var logic.OK7 Bool)
(declare-var logic.OK8 Bool)
(declare-var logic.OK9 Bool)
(declare-var logic.OK10 Bool)
(declare-var logic.ni_24.logic_heading.__logic_heading_11_c Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_14_c Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_3_c Bool)
(declare-var logic.ni_24.logic_heading.__logic_heading_5_c Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_8_c Int)
(declare-var logic.ni_24.logic_heading.ni_33.duration.__duration_2_c Int)
(declare-var logic.ni_24.logic_heading.ni_33.duration.__duration_4_c Bool)
(declare-var logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_34._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_35._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_36._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_37._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_38.fall.__fall_2_c Bool)
(declare-var logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_39.fall.__fall_2_c Bool)
(declare-var logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_41._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_42.since.__since_2_c Int)
(declare-var logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_43.since.__since_2_c Int)
(declare-var logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_44.since.__since_2_c Int)
(declare-var logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.ni_45.since.__since_2_c Int)
(declare-var logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c Bool)
(declare-var logic.ni_24.logic_heading.__logic_heading_11_m Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_14_m Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_3_m Bool)
(declare-var logic.ni_24.logic_heading.__logic_heading_5_m Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_8_m Int)
(declare-var logic.ni_24.logic_heading.ni_33.duration.__duration_2_m Int)
(declare-var logic.ni_24.logic_heading.ni_33.duration.__duration_4_m Bool)
(declare-var logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_34._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_35._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_36._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_37._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_38.fall.__fall_2_m Bool)
(declare-var logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_39.fall.__fall_2_m Bool)
(declare-var logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_41._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_42.since.__since_2_m Int)
(declare-var logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_43.since.__since_2_m Int)
(declare-var logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_44.since.__since_2_m Int)
(declare-var logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.ni_45.since.__since_2_m Int)
(declare-var logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m Bool)
(declare-var logic.ni_24.logic_heading.__logic_heading_11_x Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_14_x Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_3_x Bool)
(declare-var logic.ni_24.logic_heading.__logic_heading_5_x Int)
(declare-var logic.ni_24.logic_heading.__logic_heading_8_x Int)
(declare-var logic.ni_24.logic_heading.ni_33.duration.__duration_2_x Int)
(declare-var logic.ni_24.logic_heading.ni_33.duration.__duration_4_x Bool)
(declare-var logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_34._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_35._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_36._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_37._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_38.fall.__fall_2_x Bool)
(declare-var logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_39.fall.__fall_2_x Bool)
(declare-var logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_41._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_42.since.__since_2_x Int)
(declare-var logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_43.since.__since_2_x Int)
(declare-var logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_44.since.__since_2_x Int)
(declare-var logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_24.logic_heading.ni_45.since.__since_2_x Int)
(declare-var logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x Int)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x Int)
(declare-var logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x Bool)
(declare-rel logic_reset (Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool))
(declare-rel logic_step (Bool Bool Bool Bool Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       
       (logic_alt_fpa_reset logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
                            logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
                            logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
                            logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
                            logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
                            logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
                            logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
                            logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
                            logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
                            logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
                            logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
                            logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
                            logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
                            logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
                            logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
                            logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
                            logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
                            logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
                            logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
                            logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
                            logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                            logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                            logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                            logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m)
       (logic_heading_reset logic.ni_24.logic_heading.__logic_heading_11_c
                            logic.ni_24.logic_heading.__logic_heading_14_c
                            logic.ni_24.logic_heading.__logic_heading_3_c
                            logic.ni_24.logic_heading.__logic_heading_5_c
                            logic.ni_24.logic_heading.__logic_heading_8_c
                            logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
                            logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
                            logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
                            logic.ni_24.logic_heading.ni_34._arrow._first_c
                            logic.ni_24.logic_heading.ni_35._arrow._first_c
                            logic.ni_24.logic_heading.ni_36._arrow._first_c
                            logic.ni_24.logic_heading.ni_37._arrow._first_c
                            logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
                            logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
                            logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
                            logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
                            logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
                            logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
                            logic.ni_24.logic_heading.ni_41._arrow._first_c
                            logic.ni_24.logic_heading.ni_42.since.__since_2_c
                            logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
                            logic.ni_24.logic_heading.ni_43.since.__since_2_c
                            logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
                            logic.ni_24.logic_heading.ni_44.since.__since_2_c
                            logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
                            logic.ni_24.logic_heading.ni_45.since.__since_2_c
                            logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
                            logic.ni_24.logic_heading.__logic_heading_11_m
                            logic.ni_24.logic_heading.__logic_heading_14_m
                            logic.ni_24.logic_heading.__logic_heading_3_m
                            logic.ni_24.logic_heading.__logic_heading_5_m
                            logic.ni_24.logic_heading.__logic_heading_8_m
                            logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
                            logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
                            logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
                            logic.ni_24.logic_heading.ni_34._arrow._first_m
                            logic.ni_24.logic_heading.ni_35._arrow._first_m
                            logic.ni_24.logic_heading.ni_36._arrow._first_m
                            logic.ni_24.logic_heading.ni_37._arrow._first_m
                            logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
                            logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
                            logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
                            logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
                            logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                            logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                            logic.ni_24.logic_heading.ni_41._arrow._first_m
                            logic.ni_24.logic_heading.ni_42.since.__since_2_m
                            logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
                            logic.ni_24.logic_heading.ni_43.since.__since_2_m
                            logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
                            logic.ni_24.logic_heading.ni_44.since.__since_2_m
                            logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
                            logic.ni_24.logic_heading.ni_45.since.__since_2_m
                            logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m)
  )
  (logic_reset logic.ni_24.logic_heading.__logic_heading_11_c
               logic.ni_24.logic_heading.__logic_heading_14_c
               logic.ni_24.logic_heading.__logic_heading_3_c
               logic.ni_24.logic_heading.__logic_heading_5_c
               logic.ni_24.logic_heading.__logic_heading_8_c
               logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
               logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
               logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
               logic.ni_24.logic_heading.ni_34._arrow._first_c
               logic.ni_24.logic_heading.ni_35._arrow._first_c
               logic.ni_24.logic_heading.ni_36._arrow._first_c
               logic.ni_24.logic_heading.ni_37._arrow._first_c
               logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
               logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
               logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
               logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
               logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
               logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
               logic.ni_24.logic_heading.ni_41._arrow._first_c
               logic.ni_24.logic_heading.ni_42.since.__since_2_c
               logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
               logic.ni_24.logic_heading.ni_43.since.__since_2_c
               logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
               logic.ni_24.logic_heading.ni_44.since.__since_2_c
               logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
               logic.ni_24.logic_heading.ni_45.since.__since_2_c
               logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
               logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
               logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
               logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
               logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
               logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
               logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
               logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
               logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
               logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
               logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
               logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
               logic.ni_24.logic_heading.__logic_heading_11_m
               logic.ni_24.logic_heading.__logic_heading_14_m
               logic.ni_24.logic_heading.__logic_heading_3_m
               logic.ni_24.logic_heading.__logic_heading_5_m
               logic.ni_24.logic_heading.__logic_heading_8_m
               logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
               logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
               logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
               logic.ni_24.logic_heading.ni_34._arrow._first_m
               logic.ni_24.logic_heading.ni_35._arrow._first_m
               logic.ni_24.logic_heading.ni_36._arrow._first_m
               logic.ni_24.logic_heading.ni_37._arrow._first_m
               logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
               logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
               logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
               logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
               logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
               logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
               logic.ni_24.logic_heading.ni_41._arrow._first_m
               logic.ni_24.logic_heading.ni_42.since.__since_2_m
               logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
               logic.ni_24.logic_heading.ni_43.since.__since_2_m
               logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
               logic.ni_24.logic_heading.ni_44.since.__since_2_m
               logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
               logic.ni_24.logic_heading.ni_45.since.__since_2_m
               logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
               logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
               logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
               logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
               logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
               logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
               logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
               logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
               logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
               logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
               logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
               logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
               logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (and (= logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c)
            (= logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c)
            (= logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c)
            (= logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c)
            (= logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c)
            (= logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c)
            (= logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c)
            (= logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c)
            (= logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c)
            (= logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c)
            )
       (logic_alt_fpa_step logic.in_alt_engage
                           logic.in_fpa_engage
                           logic.in_disengage
                           logic.in_alt
                           logic.in_alt_target
                           logic.out_alt_engaged
                           logic.out_fpa_engaged
                           logic.alt_wag_170
                           logic.alt_tag_210
                           logic.fpa_wag_180
                           logic.fpa_tag_210
                           logic.OK4
                           logic.OK5
                           logic.OK6
                           logic.OK7
                           logic.OK8
                           logic.OK9
                           logic.OK10
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
                           logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
                           logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
                           logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
                           logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
                           logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
                           logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
                           logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
                           logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                           logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                           logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                           logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x
                           logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x
                           logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x
                           logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x
                           logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x
                           logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x
                           logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x
                           logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x
                           logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x
                           logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x
                           logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x
                           logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x
                           logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x)
       (and (= logic.ni_24.logic_heading.__logic_heading_11_m logic.ni_24.logic_heading.__logic_heading_11_c)
            (= logic.ni_24.logic_heading.__logic_heading_14_m logic.ni_24.logic_heading.__logic_heading_14_c)
            (= logic.ni_24.logic_heading.__logic_heading_3_m logic.ni_24.logic_heading.__logic_heading_3_c)
            (= logic.ni_24.logic_heading.__logic_heading_5_m logic.ni_24.logic_heading.__logic_heading_5_c)
            (= logic.ni_24.logic_heading.__logic_heading_8_m logic.ni_24.logic_heading.__logic_heading_8_c)
            (= logic.ni_24.logic_heading.ni_33.duration.__duration_2_m logic.ni_24.logic_heading.ni_33.duration.__duration_2_c)
            (= logic.ni_24.logic_heading.ni_33.duration.__duration_4_m logic.ni_24.logic_heading.ni_33.duration.__duration_4_c)
            (= logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_34._arrow._first_m logic.ni_24.logic_heading.ni_34._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_35._arrow._first_m logic.ni_24.logic_heading.ni_35._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_36._arrow._first_m logic.ni_24.logic_heading.ni_36._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_37._arrow._first_m logic.ni_24.logic_heading.ni_37._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_38.fall.__fall_2_m logic.ni_24.logic_heading.ni_38.fall.__fall_2_c)
            (= logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_39.fall.__fall_2_m logic.ni_24.logic_heading.ni_39.fall.__fall_2_c)
            (= logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_41._arrow._first_m logic.ni_24.logic_heading.ni_41._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_42.since.__since_2_m logic.ni_24.logic_heading.ni_42.since.__since_2_c)
            (= logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_43.since.__since_2_m logic.ni_24.logic_heading.ni_43.since.__since_2_c)
            (= logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_44.since.__since_2_m logic.ni_24.logic_heading.ni_44.since.__since_2_c)
            (= logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c)
            (= logic.ni_24.logic_heading.ni_45.since.__since_2_m logic.ni_24.logic_heading.ni_45.since.__since_2_c)
            (= logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c)
            )
       (logic_heading_step logic.in_head_engage
                           logic.in_disengage
                           logic.out_head_engaged
                           logic.OK1
                           logic.OK2
                           logic.OK3
                           logic.ni_24.logic_heading.__logic_heading_11_m
                           logic.ni_24.logic_heading.__logic_heading_14_m
                           logic.ni_24.logic_heading.__logic_heading_3_m
                           logic.ni_24.logic_heading.__logic_heading_5_m
                           logic.ni_24.logic_heading.__logic_heading_8_m
                           logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
                           logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
                           logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
                           logic.ni_24.logic_heading.ni_34._arrow._first_m
                           logic.ni_24.logic_heading.ni_35._arrow._first_m
                           logic.ni_24.logic_heading.ni_36._arrow._first_m
                           logic.ni_24.logic_heading.ni_37._arrow._first_m
                           logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
                           logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
                           logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
                           logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
                           logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                           logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                           logic.ni_24.logic_heading.ni_41._arrow._first_m
                           logic.ni_24.logic_heading.ni_42.since.__since_2_m
                           logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
                           logic.ni_24.logic_heading.ni_43.since.__since_2_m
                           logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
                           logic.ni_24.logic_heading.ni_44.since.__since_2_m
                           logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
                           logic.ni_24.logic_heading.ni_45.since.__since_2_m
                           logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
                           logic.ni_24.logic_heading.__logic_heading_11_x
                           logic.ni_24.logic_heading.__logic_heading_14_x
                           logic.ni_24.logic_heading.__logic_heading_3_x
                           logic.ni_24.logic_heading.__logic_heading_5_x
                           logic.ni_24.logic_heading.__logic_heading_8_x
                           logic.ni_24.logic_heading.ni_33.duration.__duration_2_x
                           logic.ni_24.logic_heading.ni_33.duration.__duration_4_x
                           logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x
                           logic.ni_24.logic_heading.ni_34._arrow._first_x
                           logic.ni_24.logic_heading.ni_35._arrow._first_x
                           logic.ni_24.logic_heading.ni_36._arrow._first_x
                           logic.ni_24.logic_heading.ni_37._arrow._first_x
                           logic.ni_24.logic_heading.ni_38.fall.__fall_2_x
                           logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x
                           logic.ni_24.logic_heading.ni_39.fall.__fall_2_x
                           logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x
                           logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x
                           logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x
                           logic.ni_24.logic_heading.ni_41._arrow._first_x
                           logic.ni_24.logic_heading.ni_42.since.__since_2_x
                           logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x
                           logic.ni_24.logic_heading.ni_43.since.__since_2_x
                           logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x
                           logic.ni_24.logic_heading.ni_44.since.__since_2_x
                           logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x
                           logic.ni_24.logic_heading.ni_45.since.__since_2_x
                           logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x)
       )
  (logic_step logic.in_head_engage
              logic.in_alt_engage
              logic.in_fpa_engage
              logic.in_disengage
              logic.in_alt
              logic.in_alt_target
              logic.out_head_engaged
              logic.out_alt_engaged
              logic.out_fpa_engaged
              logic.alt_wag_170
              logic.alt_tag_210
              logic.fpa_wag_180
              logic.fpa_tag_210
              logic.OK1
              logic.OK2
              logic.OK3
              logic.OK4
              logic.OK5
              logic.OK6
              logic.OK7
              logic.OK8
              logic.OK9
              logic.OK10
              logic.ni_24.logic_heading.__logic_heading_11_c
              logic.ni_24.logic_heading.__logic_heading_14_c
              logic.ni_24.logic_heading.__logic_heading_3_c
              logic.ni_24.logic_heading.__logic_heading_5_c
              logic.ni_24.logic_heading.__logic_heading_8_c
              logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
              logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
              logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
              logic.ni_24.logic_heading.ni_34._arrow._first_c
              logic.ni_24.logic_heading.ni_35._arrow._first_c
              logic.ni_24.logic_heading.ni_36._arrow._first_c
              logic.ni_24.logic_heading.ni_37._arrow._first_c
              logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
              logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
              logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
              logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
              logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
              logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
              logic.ni_24.logic_heading.ni_41._arrow._first_c
              logic.ni_24.logic_heading.ni_42.since.__since_2_c
              logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
              logic.ni_24.logic_heading.ni_43.since.__since_2_c
              logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
              logic.ni_24.logic_heading.ni_44.since.__since_2_c
              logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
              logic.ni_24.logic_heading.ni_45.since.__since_2_c
              logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
              logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
              logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
              logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
              logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
              logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
              logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
              logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
              logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
              logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
              logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
              logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
              logic.ni_24.logic_heading.__logic_heading_11_x
              logic.ni_24.logic_heading.__logic_heading_14_x
              logic.ni_24.logic_heading.__logic_heading_3_x
              logic.ni_24.logic_heading.__logic_heading_5_x
              logic.ni_24.logic_heading.__logic_heading_8_x
              logic.ni_24.logic_heading.ni_33.duration.__duration_2_x
              logic.ni_24.logic_heading.ni_33.duration.__duration_4_x
              logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x
              logic.ni_24.logic_heading.ni_34._arrow._first_x
              logic.ni_24.logic_heading.ni_35._arrow._first_x
              logic.ni_24.logic_heading.ni_36._arrow._first_x
              logic.ni_24.logic_heading.ni_37._arrow._first_x
              logic.ni_24.logic_heading.ni_38.fall.__fall_2_x
              logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x
              logic.ni_24.logic_heading.ni_39.fall.__fall_2_x
              logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x
              logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x
              logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x
              logic.ni_24.logic_heading.ni_41._arrow._first_x
              logic.ni_24.logic_heading.ni_42.since.__since_2_x
              logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x
              logic.ni_24.logic_heading.ni_43.since.__since_2_x
              logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x
              logic.ni_24.logic_heading.ni_44.since.__since_2_x
              logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x
              logic.ni_24.logic_heading.ni_45.since.__since_2_x
              logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x
              logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x
              logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x
              logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x
              logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x
              logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x
              logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x
              logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x
              logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x
              logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x
              logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x
              logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x
              logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x)
))

; longitudinal
(declare-var longitudinal.in_alt_engaged Bool)
(declare-var longitudinal.in_fpa_engaged Bool)
(declare-var longitudinal.in_alt Real)
(declare-var longitudinal.in_alt_target Real)
(declare-var longitudinal.in_hdot Real)
(declare-var longitudinal.in_fpa Real)
(declare-var longitudinal.in_fpa_target Real)
(declare-var longitudinal.in_pitch Real)
(declare-var longitudinal.in_speed Real)
(declare-var longitudinal.in_gskts Real)
(declare-var longitudinal.in_cas Real)
(declare-var longitudinal.in_elev Real)
(declare-var longitudinal.out_alt Real)
(declare-var longitudinal.out_pitch Real)
(declare-var longitudinal.out_elev Real)
(declare-var longitudinal.OK11 Bool)
(declare-var longitudinal.OK12 Bool)
(declare-var longitudinal.OK13 Bool)
(declare-var longitudinal.OK14 Bool)
(declare-var longitudinal.OK15 Bool)
(declare-var longitudinal.__longitudinal_10_c Bool)
(declare-var longitudinal.__longitudinal_12_c Bool)
(declare-var longitudinal.__longitudinal_14_c Real)
(declare-var longitudinal.__longitudinal_16_c Real)
(declare-var longitudinal.__longitudinal_17_c Real)
(declare-var longitudinal.__longitudinal_18_c Real)
(declare-var longitudinal.__longitudinal_2_c Bool)
(declare-var longitudinal.__longitudinal_20_c Real)
(declare-var longitudinal.__longitudinal_24_c Real)
(declare-var longitudinal.__longitudinal_25_c Real)
(declare-var longitudinal.__longitudinal_28_c Real)
(declare-var longitudinal.__longitudinal_31_c Real)
(declare-var longitudinal.__longitudinal_34_c Bool)
(declare-var longitudinal.__longitudinal_4_c Bool)
(declare-var longitudinal.__longitudinal_6_c Bool)
(declare-var longitudinal.__longitudinal_8_c Bool)
(declare-var longitudinal.ni_14._arrow._first_c Bool)
(declare-var longitudinal.ni_15.fall.__fall_2_c Bool)
(declare-var longitudinal.ni_15.fall.ni_80._arrow._first_c Bool)
(declare-var longitudinal.ni_16._arrow._first_c Bool)
(declare-var longitudinal.ni_17._arrow._first_c Bool)
(declare-var longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var longitudinal.ni_23._arrow._first_c Bool)
(declare-var longitudinal.__longitudinal_10_m Bool)
(declare-var longitudinal.__longitudinal_12_m Bool)
(declare-var longitudinal.__longitudinal_14_m Real)
(declare-var longitudinal.__longitudinal_16_m Real)
(declare-var longitudinal.__longitudinal_17_m Real)
(declare-var longitudinal.__longitudinal_18_m Real)
(declare-var longitudinal.__longitudinal_2_m Bool)
(declare-var longitudinal.__longitudinal_20_m Real)
(declare-var longitudinal.__longitudinal_24_m Real)
(declare-var longitudinal.__longitudinal_25_m Real)
(declare-var longitudinal.__longitudinal_28_m Real)
(declare-var longitudinal.__longitudinal_31_m Real)
(declare-var longitudinal.__longitudinal_34_m Bool)
(declare-var longitudinal.__longitudinal_4_m Bool)
(declare-var longitudinal.__longitudinal_6_m Bool)
(declare-var longitudinal.__longitudinal_8_m Bool)
(declare-var longitudinal.ni_14._arrow._first_m Bool)
(declare-var longitudinal.ni_15.fall.__fall_2_m Bool)
(declare-var longitudinal.ni_15.fall.ni_80._arrow._first_m Bool)
(declare-var longitudinal.ni_16._arrow._first_m Bool)
(declare-var longitudinal.ni_17._arrow._first_m Bool)
(declare-var longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var longitudinal.ni_23._arrow._first_m Bool)
(declare-var longitudinal.__longitudinal_10_x Bool)
(declare-var longitudinal.__longitudinal_12_x Bool)
(declare-var longitudinal.__longitudinal_14_x Real)
(declare-var longitudinal.__longitudinal_16_x Real)
(declare-var longitudinal.__longitudinal_17_x Real)
(declare-var longitudinal.__longitudinal_18_x Real)
(declare-var longitudinal.__longitudinal_2_x Bool)
(declare-var longitudinal.__longitudinal_20_x Real)
(declare-var longitudinal.__longitudinal_24_x Real)
(declare-var longitudinal.__longitudinal_25_x Real)
(declare-var longitudinal.__longitudinal_28_x Real)
(declare-var longitudinal.__longitudinal_31_x Real)
(declare-var longitudinal.__longitudinal_34_x Bool)
(declare-var longitudinal.__longitudinal_4_x Bool)
(declare-var longitudinal.__longitudinal_6_x Bool)
(declare-var longitudinal.__longitudinal_8_x Bool)
(declare-var longitudinal.ni_14._arrow._first_x Bool)
(declare-var longitudinal.ni_15.fall.__fall_2_x Bool)
(declare-var longitudinal.ni_15.fall.ni_80._arrow._first_x Bool)
(declare-var longitudinal.ni_16._arrow._first_x Bool)
(declare-var longitudinal.ni_17._arrow._first_x Bool)
(declare-var longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var longitudinal.ni_23._arrow._first_x Bool)
(declare-var longitudinal.__longitudinal_1 Bool)
(declare-var longitudinal.__longitudinal_11 Bool)
(declare-var longitudinal.__longitudinal_13 Bool)
(declare-var longitudinal.__longitudinal_15 Real)
(declare-var longitudinal.__longitudinal_19 Bool)
(declare-var longitudinal.__longitudinal_21 Bool)
(declare-var longitudinal.__longitudinal_22 Bool)
(declare-var longitudinal.__longitudinal_23 Bool)
(declare-var longitudinal.__longitudinal_26 Bool)
(declare-var longitudinal.__longitudinal_27 Bool)
(declare-var longitudinal.__longitudinal_29 Real)
(declare-var longitudinal.__longitudinal_3 Bool)
(declare-var longitudinal.__longitudinal_30 Real)
(declare-var longitudinal.__longitudinal_32 Real)
(declare-var longitudinal.__longitudinal_33 Real)
(declare-var longitudinal.__longitudinal_35 Bool)
(declare-var longitudinal.__longitudinal_5 Bool)
(declare-var longitudinal.__longitudinal_7 Bool)
(declare-var longitudinal.__longitudinal_9 Bool)
(declare-var longitudinal.fpa_target_stable Bool)
(declare-var longitudinal.fpa_tgt_delta_lb Real)
(declare-var longitudinal.fpa_tgt_delta_ub Real)
(declare-var longitudinal.fpa_tgt_lb Real)
(declare-var longitudinal.fpa_tgt_ub Real)
(declare-var longitudinal.lowering Bool)
(declare-var longitudinal.pc1 Bool)
(declare-var longitudinal.pc2 Bool)
(declare-var longitudinal.pc3 Bool)
(declare-var longitudinal.pc4 Bool)
(declare-var longitudinal.pc5 Bool)
(declare-var longitudinal.pre_out_pitch Real)
(declare-var longitudinal.rising Bool)
(declare-rel longitudinal_reset (Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool))
(declare-rel longitudinal_step (Bool Bool Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool))

(rule (=> 
  (and 
       (= longitudinal.__longitudinal_10_m longitudinal.__longitudinal_10_c)
       (= longitudinal.__longitudinal_12_m longitudinal.__longitudinal_12_c)
       (= longitudinal.__longitudinal_14_m longitudinal.__longitudinal_14_c)
       (= longitudinal.__longitudinal_16_m longitudinal.__longitudinal_16_c)
       (= longitudinal.__longitudinal_17_m longitudinal.__longitudinal_17_c)
       (= longitudinal.__longitudinal_18_m longitudinal.__longitudinal_18_c)
       (= longitudinal.__longitudinal_2_m longitudinal.__longitudinal_2_c)
       (= longitudinal.__longitudinal_20_m longitudinal.__longitudinal_20_c)
       (= longitudinal.__longitudinal_24_m longitudinal.__longitudinal_24_c)
       (= longitudinal.__longitudinal_25_m longitudinal.__longitudinal_25_c)
       (= longitudinal.__longitudinal_28_m longitudinal.__longitudinal_28_c)
       (= longitudinal.__longitudinal_31_m longitudinal.__longitudinal_31_c)
       (= longitudinal.__longitudinal_34_m longitudinal.__longitudinal_34_c)
       (= longitudinal.__longitudinal_4_m longitudinal.__longitudinal_4_c)
       (= longitudinal.__longitudinal_6_m longitudinal.__longitudinal_6_c)
       (= longitudinal.__longitudinal_8_m longitudinal.__longitudinal_8_c)
       (= longitudinal.ni_23._arrow._first_m true)
       (PseudoContinuous_reset longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
                               longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
                               longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                               longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m)
       (PseudoContinuous_reset longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
                               longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
                               longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                               longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m)
       (PseudoContinuous_reset longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
                               longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
                               longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                               longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m)
       (PseudoContinuous_reset longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
                               longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
                               longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                               longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m)
       (PseudoContinuous_reset longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
                               longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
                               longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                               longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m)
       (= longitudinal.ni_17._arrow._first_m true)
       (= longitudinal.ni_16._arrow._first_m true)
       (fall_reset longitudinal.ni_15.fall.__fall_2_c
                   longitudinal.ni_15.fall.ni_80._arrow._first_c
                   longitudinal.ni_15.fall.__fall_2_m
                   longitudinal.ni_15.fall.ni_80._arrow._first_m)
       (= longitudinal.ni_14._arrow._first_m true)
  )
  (longitudinal_reset longitudinal.__longitudinal_10_c
                      longitudinal.__longitudinal_12_c
                      longitudinal.__longitudinal_14_c
                      longitudinal.__longitudinal_16_c
                      longitudinal.__longitudinal_17_c
                      longitudinal.__longitudinal_18_c
                      longitudinal.__longitudinal_2_c
                      longitudinal.__longitudinal_20_c
                      longitudinal.__longitudinal_24_c
                      longitudinal.__longitudinal_25_c
                      longitudinal.__longitudinal_28_c
                      longitudinal.__longitudinal_31_c
                      longitudinal.__longitudinal_34_c
                      longitudinal.__longitudinal_4_c
                      longitudinal.__longitudinal_6_c
                      longitudinal.__longitudinal_8_c
                      longitudinal.ni_14._arrow._first_c
                      longitudinal.ni_15.fall.__fall_2_c
                      longitudinal.ni_15.fall.ni_80._arrow._first_c
                      longitudinal.ni_16._arrow._first_c
                      longitudinal.ni_17._arrow._first_c
                      longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
                      longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
                      longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
                      longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
                      longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
                      longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
                      longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
                      longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
                      longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
                      longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
                      longitudinal.ni_23._arrow._first_c
                      longitudinal.__longitudinal_10_m
                      longitudinal.__longitudinal_12_m
                      longitudinal.__longitudinal_14_m
                      longitudinal.__longitudinal_16_m
                      longitudinal.__longitudinal_17_m
                      longitudinal.__longitudinal_18_m
                      longitudinal.__longitudinal_2_m
                      longitudinal.__longitudinal_20_m
                      longitudinal.__longitudinal_24_m
                      longitudinal.__longitudinal_25_m
                      longitudinal.__longitudinal_28_m
                      longitudinal.__longitudinal_31_m
                      longitudinal.__longitudinal_34_m
                      longitudinal.__longitudinal_4_m
                      longitudinal.__longitudinal_6_m
                      longitudinal.__longitudinal_8_m
                      longitudinal.ni_14._arrow._first_m
                      longitudinal.ni_15.fall.__fall_2_m
                      longitudinal.ni_15.fall.ni_80._arrow._first_m
                      longitudinal.ni_16._arrow._first_m
                      longitudinal.ni_17._arrow._first_m
                      longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                      longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
                      longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                      longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
                      longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                      longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
                      longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                      longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
                      longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                      longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
                      longitudinal.ni_23._arrow._first_m)
))

; Step rule with Assertions 
(rule (=> 
  (and 
  (and (= longitudinal.ni_23._arrow._first_m longitudinal.ni_23._arrow._first_c)
       (and (= longitudinal.__longitudinal_1 (ite longitudinal.ni_23._arrow._first_m true false))
            (= longitudinal.ni_23._arrow._first_x false))
       (and (or (not (= longitudinal.__longitudinal_1 false))
               (and (= longitudinal.pre_out_pitch longitudinal.__longitudinal_18_c)
                    (= longitudinal.fpa_tgt_delta_lb (* (+ (- 3.2) longitudinal.__longitudinal_16_c) 0.34))
                    ))
            (or (not (= longitudinal.__longitudinal_1 true))
               (and (= longitudinal.pre_out_pitch longitudinal.out_pitch)
                    (= longitudinal.fpa_tgt_delta_lb 0.0)
                    ))
       )
       (= longitudinal.fpa_tgt_lb (+ longitudinal.in_fpa_target longitudinal.fpa_tgt_delta_lb))
       (and (or (not (= longitudinal.__longitudinal_1 true))
               (= longitudinal.__longitudinal_15 0.0))
            (or (not (= longitudinal.__longitudinal_1 false))
               (= longitudinal.__longitudinal_15 (- longitudinal.in_fpa longitudinal.__longitudinal_14_c)))
       )
       (= longitudinal.rising (or (or (> longitudinal.out_pitch longitudinal.in_pitch) (>= longitudinal.out_pitch longitudinal.pre_out_pitch)) (<= longitudinal.fpa_tgt_lb (+ longitudinal.in_fpa longitudinal.__longitudinal_15))))
       (and (= longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c)
            (= longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c)
            )
       (PseudoContinuous_step longitudinal.in_cas
                              1.0
                              longitudinal.pc5
                              longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                              longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
                              longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x
                              longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x)
       (and (= longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c)
            (= longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c)
            )
       (PseudoContinuous_step longitudinal.in_gskts
                              1.0
                              longitudinal.pc4
                              longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                              longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
                              longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x
                              longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x)
       (and (= longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c)
            (= longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c)
            )
       (PseudoContinuous_step longitudinal.in_speed
                              1.0
                              longitudinal.pc3
                              longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                              longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
                              longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x
                              longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x)
       (and (= longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c)
            (= longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c)
            )
       (PseudoContinuous_step longitudinal.in_hdot
                              1.0
                              longitudinal.pc2
                              longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                              longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
                              longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x
                              longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x)
       (and (= longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c)
            (= longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c)
            )
       (PseudoContinuous_step longitudinal.in_alt
                              1.0
                              longitudinal.pc1
                              longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                              longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
                              longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x
                              longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x)
       (and (or (not (= longitudinal.__longitudinal_1 true))
               (= longitudinal.fpa_tgt_delta_ub 0.0))
            (or (not (= longitudinal.__longitudinal_1 false))
               (= longitudinal.fpa_tgt_delta_ub (* (+ 3.2 longitudinal.__longitudinal_16_c) 0.34)))
       )
       (= longitudinal.fpa_tgt_ub (+ longitudinal.in_fpa_target longitudinal.fpa_tgt_delta_ub))
       (= longitudinal.lowering (or (or (< longitudinal.out_pitch longitudinal.in_pitch) (<= longitudinal.out_pitch longitudinal.pre_out_pitch)) (>= longitudinal.fpa_tgt_ub (+ longitudinal.in_fpa longitudinal.__longitudinal_15))))
       (and (or (not (= longitudinal.__longitudinal_1 false))
               (and (= longitudinal.fpa_target_stable (= longitudinal.in_fpa_target longitudinal.__longitudinal_17_c))
                    (= longitudinal.__longitudinal_9 longitudinal.__longitudinal_8_c)
                    ))
            (or (not (= longitudinal.__longitudinal_1 true))
               (and (= longitudinal.fpa_target_stable true)
                    (= longitudinal.__longitudinal_9 false)
                    ))
       )
       (= longitudinal.__longitudinal_8_x longitudinal.in_fpa_engaged)
       (and (or (not (= longitudinal.__longitudinal_1 true))
               (= longitudinal.__longitudinal_7 false))
            (or (not (= longitudinal.__longitudinal_1 false))
               (= longitudinal.__longitudinal_7 longitudinal.__longitudinal_6_c))
       )
       (= longitudinal.__longitudinal_6_x (>= longitudinal.fpa_tgt_ub longitudinal.in_fpa))
       (and (or (not (= longitudinal.__longitudinal_1 true))
               (= longitudinal.__longitudinal_5 false))
            (or (not (= longitudinal.__longitudinal_1 false))
               (= longitudinal.__longitudinal_5 longitudinal.__longitudinal_4_c))
       )
       (= longitudinal.__longitudinal_4_x (= longitudinal.in_pitch longitudinal.out_pitch))
       (= longitudinal.ni_17._arrow._first_m longitudinal.ni_17._arrow._first_c)
       (and (= longitudinal.__longitudinal_27 (ite longitudinal.ni_17._arrow._first_m true false))
            (= longitudinal.ni_17._arrow._first_x false))
       (and (or (not (= longitudinal.__longitudinal_27 true))
               (= longitudinal.__longitudinal_32 0.0))
            (or (not (= longitudinal.__longitudinal_27 false))
               (= longitudinal.__longitudinal_32 (- longitudinal.in_pitch longitudinal.__longitudinal_31_c)))
       )
       (abs_real_fun longitudinal.__longitudinal_32
                     longitudinal.__longitudinal_33)
       (and (or (not (= longitudinal.__longitudinal_27 true))
               (= longitudinal.__longitudinal_29 0.0))
            (or (not (= longitudinal.__longitudinal_27 false))
               (= longitudinal.__longitudinal_29 longitudinal.__longitudinal_28_c))
       )
       (abs_real_fun longitudinal.__longitudinal_29
                     longitudinal.__longitudinal_30)
       (and (or (not (= longitudinal.__longitudinal_27 true))
               (= longitudinal.__longitudinal_35 true))
            (or (not (= longitudinal.__longitudinal_27 false))
               (= longitudinal.__longitudinal_35 (=> longitudinal.__longitudinal_34_c (<= longitudinal.__longitudinal_33 longitudinal.__longitudinal_30))))
       )
       (= longitudinal.__longitudinal_34_x longitudinal.in_fpa_engaged)
       (= longitudinal.__longitudinal_31_x longitudinal.out_pitch)
       (and (or (not (= longitudinal.__longitudinal_1 true))
               (= longitudinal.__longitudinal_3 false))
            (or (not (= longitudinal.__longitudinal_1 false))
               (= longitudinal.__longitudinal_3 longitudinal.__longitudinal_2_c))
       )
       (= longitudinal.__longitudinal_28_x (- longitudinal.in_pitch longitudinal.out_pitch))
       (= longitudinal.ni_16._arrow._first_m longitudinal.ni_16._arrow._first_c)
       (and (= longitudinal.__longitudinal_23 (ite longitudinal.ni_16._arrow._first_m true false))
            (= longitudinal.ni_16._arrow._first_x false))
       (and (or (not (= longitudinal.__longitudinal_23 true))
               (= longitudinal.__longitudinal_26 true))
            (or (not (= longitudinal.__longitudinal_23 false))
               (= longitudinal.__longitudinal_26 (and (and (= (> longitudinal.in_pitch longitudinal.__longitudinal_25_c) (> longitudinal.in_fpa longitudinal.__longitudinal_24_c)) (= (< longitudinal.in_pitch longitudinal.__longitudinal_25_c) (< longitudinal.in_fpa longitudinal.__longitudinal_24_c))) (= (= longitudinal.in_pitch longitudinal.__longitudinal_25_c) (= longitudinal.in_fpa longitudinal.__longitudinal_24_c)))))
       )
       (= longitudinal.__longitudinal_25_x longitudinal.in_pitch)
       (= longitudinal.__longitudinal_24_x longitudinal.in_fpa)
       (and (= longitudinal.ni_15.fall.__fall_2_m longitudinal.ni_15.fall.__fall_2_c)
            (= longitudinal.ni_15.fall.ni_80._arrow._first_m longitudinal.ni_15.fall.ni_80._arrow._first_c)
            )
       (fall_step longitudinal.in_alt_engaged
                  longitudinal.__longitudinal_22
                  longitudinal.ni_15.fall.__fall_2_m
                  longitudinal.ni_15.fall.ni_80._arrow._first_m
                  longitudinal.ni_15.fall.__fall_2_x
                  longitudinal.ni_15.fall.ni_80._arrow._first_x)
       (= longitudinal.ni_14._arrow._first_m longitudinal.ni_14._arrow._first_c)
       (and (= longitudinal.__longitudinal_19 (ite longitudinal.ni_14._arrow._first_m true false))
            (= longitudinal.ni_14._arrow._first_x false))
       (and (or (not (= longitudinal.__longitudinal_19 true))
               (= longitudinal.__longitudinal_21 true))
            (or (not (= longitudinal.__longitudinal_19 false))
               (= longitudinal.__longitudinal_21 (= longitudinal.in_alt (+ longitudinal.in_hdot longitudinal.__longitudinal_20_c))))
       )
       (= longitudinal.__longitudinal_20_x longitudinal.in_alt)
       (= longitudinal.__longitudinal_2_x longitudinal.lowering)
       (= longitudinal.__longitudinal_18_x longitudinal.out_pitch)
       (= longitudinal.__longitudinal_17_x longitudinal.in_fpa_target)
       (= longitudinal.__longitudinal_16_x longitudinal.in_hdot)
       (= longitudinal.__longitudinal_14_x longitudinal.in_fpa)
       (and (or (not (= longitudinal.__longitudinal_1 true))
               (= longitudinal.__longitudinal_13 false))
            (or (not (= longitudinal.__longitudinal_1 false))
               (= longitudinal.__longitudinal_13 longitudinal.__longitudinal_12_c))
       )
       (= longitudinal.__longitudinal_12_x (<= longitudinal.fpa_tgt_lb longitudinal.in_fpa))
       (and (or (not (= longitudinal.__longitudinal_1 true))
               (= longitudinal.__longitudinal_11 false))
            (or (not (= longitudinal.__longitudinal_1 false))
               (= longitudinal.__longitudinal_11 longitudinal.__longitudinal_10_c))
       )
       (= longitudinal.__longitudinal_10_x longitudinal.rising)
       (= longitudinal.OK15 (=> (and (and (and (and (not (not (= longitudinal.in_elev 0.0))) (or (not (= longitudinal.in_elev 0.0)) (<= longitudinal.in_cas 500.0))) longitudinal.in_fpa_engaged) longitudinal.__longitudinal_9) (< longitudinal.fpa_tgt_ub longitudinal.in_fpa)) (or (or (or longitudinal.__longitudinal_7 longitudinal.__longitudinal_5) longitudinal.lowering) longitudinal.__longitudinal_3)))
       (= longitudinal.OK14 (=> (and (and (and (and (not (not (= longitudinal.in_elev 0.0))) (or (not (= longitudinal.in_elev 0.0)) (<= longitudinal.in_cas 500.0))) longitudinal.in_fpa_engaged) longitudinal.__longitudinal_9) (> longitudinal.fpa_tgt_lb longitudinal.in_fpa)) (or (or (or longitudinal.__longitudinal_13 longitudinal.__longitudinal_5) longitudinal.rising) longitudinal.__longitudinal_11)))
       (= longitudinal.OK13 (=> (and (and (and (not (not (= longitudinal.in_elev 0.0))) (or (not (= longitudinal.in_elev 0.0)) (<= longitudinal.in_cas 500.0))) (not longitudinal.in_fpa_engaged)) (not longitudinal.in_alt_engaged)) (= longitudinal.out_elev 0.0)))
       (= longitudinal.OK12 (=> (and (not (not (= longitudinal.in_elev 0.0))) (> longitudinal.in_cas 500.0)) (= longitudinal.out_elev 0.0)))
       (= longitudinal.OK11 (=> (not (= longitudinal.in_elev 0.0)) (= longitudinal.out_elev longitudinal.in_elev)))
       )
 (and longitudinal.__longitudinal_35
      longitudinal.__longitudinal_26
      (=> longitudinal.__longitudinal_22 (not longitudinal.in_fpa_engaged))
      (not (and longitudinal.in_fpa_engaged longitudinal.in_alt_engaged))
      (=> (or longitudinal.in_fpa_engaged longitudinal.in_alt_engaged) (= longitudinal.in_fpa_target 0.0))
      (=> (or longitudinal.in_fpa_engaged longitudinal.in_alt_engaged) (>= longitudinal.in_alt_target 0.0))
      (=> (or longitudinal.in_fpa_engaged longitudinal.in_alt_engaged) (>= longitudinal.in_cas 100.0))
      (=> (or longitudinal.in_fpa_engaged longitudinal.in_alt_engaged) (>= longitudinal.in_gskts 100.0))
      (=> (or longitudinal.in_fpa_engaged longitudinal.in_alt_engaged) (>= longitudinal.in_speed 100.0))
      longitudinal.__longitudinal_21
      longitudinal.pc5
      longitudinal.pc4
      longitudinal.pc3
      longitudinal.pc2
      longitudinal.pc1
      (>= longitudinal.in_cas 0.0)
      (>= longitudinal.in_gskts 0.0)
      (>= longitudinal.in_speed 0.0) ))
(longitudinal_step longitudinal.in_alt_engaged longitudinal.in_fpa_engaged longitudinal.in_alt longitudinal.in_alt_target longitudinal.in_hdot longitudinal.in_fpa longitudinal.in_fpa_target longitudinal.in_pitch longitudinal.in_speed longitudinal.in_gskts longitudinal.in_cas longitudinal.in_elev longitudinal.out_alt longitudinal.out_pitch longitudinal.out_elev longitudinal.OK11 longitudinal.OK12 longitudinal.OK13 longitudinal.OK14 longitudinal.OK15 longitudinal.__longitudinal_10_c longitudinal.__longitudinal_12_c longitudinal.__longitudinal_14_c longitudinal.__longitudinal_16_c longitudinal.__longitudinal_17_c longitudinal.__longitudinal_18_c longitudinal.__longitudinal_2_c longitudinal.__longitudinal_20_c longitudinal.__longitudinal_24_c longitudinal.__longitudinal_25_c longitudinal.__longitudinal_28_c longitudinal.__longitudinal_31_c longitudinal.__longitudinal_34_c longitudinal.__longitudinal_4_c longitudinal.__longitudinal_6_c longitudinal.__longitudinal_8_c longitudinal.ni_14._arrow._first_c longitudinal.ni_15.fall.__fall_2_c longitudinal.ni_15.fall.ni_80._arrow._first_c longitudinal.ni_16._arrow._first_c longitudinal.ni_17._arrow._first_c longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c longitudinal.ni_23._arrow._first_c longitudinal.__longitudinal_10_x longitudinal.__longitudinal_12_x longitudinal.__longitudinal_14_x longitudinal.__longitudinal_16_x longitudinal.__longitudinal_17_x longitudinal.__longitudinal_18_x longitudinal.__longitudinal_2_x longitudinal.__longitudinal_20_x longitudinal.__longitudinal_24_x longitudinal.__longitudinal_25_x longitudinal.__longitudinal_28_x longitudinal.__longitudinal_31_x longitudinal.__longitudinal_34_x longitudinal.__longitudinal_4_x longitudinal.__longitudinal_6_x longitudinal.__longitudinal_8_x longitudinal.ni_14._arrow._first_x longitudinal.ni_15.fall.__fall_2_x longitudinal.ni_15.fall.ni_80._arrow._first_x longitudinal.ni_16._arrow._first_x longitudinal.ni_17._arrow._first_x longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x longitudinal.ni_23._arrow._first_x)
))

; Mode_plus_Longitudinal
(declare-var Mode_plus_Longitudinal.head_mode Real)
(declare-var Mode_plus_Longitudinal.ail_stick_in Real)
(declare-var Mode_plus_Longitudinal.elev_stick_in Real)
(declare-var Mode_plus_Longitudinal.alt_mode Real)
(declare-var Mode_plus_Longitudinal.fpa_mode Real)
(declare-var Mode_plus_Longitudinal.at_mode Real)
(declare-var Mode_plus_Longitudinal.alt_cmd_in Real)
(declare-var Mode_plus_Longitudinal.alt_in Real)
(declare-var Mode_plus_Longitudinal.cas_in Real)
(declare-var Mode_plus_Longitudinal.cas_cmd Real)
(declare-var Mode_plus_Longitudinal.pitch_in Real)
(declare-var Mode_plus_Longitudinal.qbdegf_1_1 Real)
(declare-var Mode_plus_Longitudinal.gskts_in Real)
(declare-var Mode_plus_Longitudinal.hdot_in Real)
(declare-var Mode_plus_Longitudinal.speed_in Real)
(declare-var Mode_plus_Longitudinal.fpa_in Real)
(declare-var Mode_plus_Longitudinal.fpa_cmd_in Real)
(declare-var Mode_plus_Longitudinal.d_out1 Real)
(declare-var Mode_plus_Longitudinal.d_out2 Real)
(declare-var Mode_plus_Longitudinal.t_out3 Real)
(declare-var Mode_plus_Longitudinal.t_out4 Real)
(declare-var Mode_plus_Longitudinal.d_out4 Real)
(declare-var Mode_plus_Longitudinal.t_out5 Real)
(declare-var Mode_plus_Longitudinal.d_out5 Real)
(declare-var Mode_plus_Longitudinal.t_out6 Real)
(declare-var Mode_plus_Longitudinal.d_out6 Real)
(declare-var Mode_plus_Longitudinal.head_eng_out Bool)
(declare-var Mode_plus_Longitudinal.alt_eng_out Bool)
(declare-var Mode_plus_Longitudinal.at_eng_out Bool)
(declare-var Mode_plus_Longitudinal.fpa_eng_out Bool)
(declare-var Mode_plus_Longitudinal.cas_cmd_out Real)
(declare-var Mode_plus_Longitudinal.alt_cmd_out Real)
(declare-var Mode_plus_Longitudinal.pitch_cmd_out Real)
(declare-var Mode_plus_Longitudinal.elev_cmd_out Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c Bool)
(declare-var Mode_plus_Longitudinal.ni_12.fall.__fall_2_c Bool)
(declare-var Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c Real)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m Bool)
(declare-var Mode_plus_Longitudinal.ni_12.fall.__fall_2_m Bool)
(declare-var Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m Real)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x Bool)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x Real)
(declare-var Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x Bool)
(declare-var Mode_plus_Longitudinal.ni_12.fall.__fall_2_x Bool)
(declare-var Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x Real)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x Bool)
(declare-var Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x Bool)
(declare-var Mode_plus_Longitudinal.fall_out Bool)
(declare-rel Mode_plus_Longitudinal_reset (Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool))
(declare-rel Mode_plus_Longitudinal_step (Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Real Real Real Real Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       
       (MODE_LOGIC_reset Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                         Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
       (fall_reset Mode_plus_Longitudinal.ni_12.fall.__fall_2_c
                   Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c
                   Mode_plus_Longitudinal.ni_12.fall.__fall_2_m
                   Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m)
       (LONGITUDINAL_CONTROLLER_reset Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                      Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m)
  )
  (Mode_plus_Longitudinal_reset Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                Mode_plus_Longitudinal.ni_12.fall.__fall_2_c
                                Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m
                                Mode_plus_Longitudinal.ni_12.fall.__fall_2_m
                                Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                                Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
))

; Step rule with Assertions 
(rule (=> 
  (and 
  (and (and (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c)
            (= Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c)
            )
       (MODE_LOGIC_step Mode_plus_Longitudinal.head_mode
                        Mode_plus_Longitudinal.ail_stick_in
                        Mode_plus_Longitudinal.elev_stick_in
                        Mode_plus_Longitudinal.alt_mode
                        Mode_plus_Longitudinal.fpa_mode
                        Mode_plus_Longitudinal.at_mode
                        Mode_plus_Longitudinal.alt_cmd_in
                        Mode_plus_Longitudinal.alt_in
                        Mode_plus_Longitudinal.cas_in
                        Mode_plus_Longitudinal.cas_cmd
                        Mode_plus_Longitudinal.head_eng_out
                        Mode_plus_Longitudinal.alt_eng_out
                        Mode_plus_Longitudinal.fpa_eng_out
                        Mode_plus_Longitudinal.at_eng_out
                        Mode_plus_Longitudinal.cas_cmd_out
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x
                        Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
       (and (= Mode_plus_Longitudinal.ni_12.fall.__fall_2_m Mode_plus_Longitudinal.ni_12.fall.__fall_2_c)
            (= Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c)
            )
       (fall_step Mode_plus_Longitudinal.alt_eng_out
                  Mode_plus_Longitudinal.fall_out
                  Mode_plus_Longitudinal.ni_12.fall.__fall_2_m
                  Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m
                  Mode_plus_Longitudinal.ni_12.fall.__fall_2_x
                  Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x)
       (and (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c)
            (= Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c)
            )
       (LONGITUDINAL_CONTROLLER_step Mode_plus_Longitudinal.fpa_cmd_in
                                     Mode_plus_Longitudinal.alt_eng_out
                                     Mode_plus_Longitudinal.fpa_eng_out
                                     Mode_plus_Longitudinal.alt_cmd_in
                                     Mode_plus_Longitudinal.alt_in
                                     Mode_plus_Longitudinal.pitch_in
                                     Mode_plus_Longitudinal.qbdegf_1_1
                                     Mode_plus_Longitudinal.gskts_in
                                     Mode_plus_Longitudinal.hdot_in
                                     Mode_plus_Longitudinal.speed_in
                                     Mode_plus_Longitudinal.fpa_in
                                     Mode_plus_Longitudinal.cas_in
                                     Mode_plus_Longitudinal.elev_stick_in
                                     Mode_plus_Longitudinal.d_out1
                                     Mode_plus_Longitudinal.d_out2
                                     Mode_plus_Longitudinal.t_out3
                                     Mode_plus_Longitudinal.t_out4
                                     Mode_plus_Longitudinal.d_out4
                                     Mode_plus_Longitudinal.t_out5
                                     Mode_plus_Longitudinal.d_out5
                                     Mode_plus_Longitudinal.t_out6
                                     Mode_plus_Longitudinal.d_out6
                                     Mode_plus_Longitudinal.alt_cmd_out
                                     Mode_plus_Longitudinal.pitch_cmd_out
                                     Mode_plus_Longitudinal.elev_cmd_out
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
                                     Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x)
       )
 (=> Mode_plus_Longitudinal.fall_out (not Mode_plus_Longitudinal.fpa_eng_out)))
(Mode_plus_Longitudinal_step Mode_plus_Longitudinal.head_mode Mode_plus_Longitudinal.ail_stick_in Mode_plus_Longitudinal.elev_stick_in Mode_plus_Longitudinal.alt_mode Mode_plus_Longitudinal.fpa_mode Mode_plus_Longitudinal.at_mode Mode_plus_Longitudinal.alt_cmd_in Mode_plus_Longitudinal.alt_in Mode_plus_Longitudinal.cas_in Mode_plus_Longitudinal.cas_cmd Mode_plus_Longitudinal.pitch_in Mode_plus_Longitudinal.qbdegf_1_1 Mode_plus_Longitudinal.gskts_in Mode_plus_Longitudinal.hdot_in Mode_plus_Longitudinal.speed_in Mode_plus_Longitudinal.fpa_in Mode_plus_Longitudinal.fpa_cmd_in Mode_plus_Longitudinal.d_out1 Mode_plus_Longitudinal.d_out2 Mode_plus_Longitudinal.t_out3 Mode_plus_Longitudinal.t_out4 Mode_plus_Longitudinal.d_out4 Mode_plus_Longitudinal.t_out5 Mode_plus_Longitudinal.d_out5 Mode_plus_Longitudinal.t_out6 Mode_plus_Longitudinal.d_out6 Mode_plus_Longitudinal.head_eng_out Mode_plus_Longitudinal.alt_eng_out Mode_plus_Longitudinal.at_eng_out Mode_plus_Longitudinal.fpa_eng_out Mode_plus_Longitudinal.cas_cmd_out Mode_plus_Longitudinal.alt_cmd_out Mode_plus_Longitudinal.pitch_cmd_out Mode_plus_Longitudinal.elev_cmd_out Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c Mode_plus_Longitudinal.ni_12.fall.__fall_2_c Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x Mode_plus_Longitudinal.ni_12.fall.__fall_2_x Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
))

; logic_longitudinal
(declare-var logic_longitudinal.in_head_engage Bool)
(declare-var logic_longitudinal.in_alt_engage Bool)
(declare-var logic_longitudinal.in_fpa_engage Bool)
(declare-var logic_longitudinal.in_alt Real)
(declare-var logic_longitudinal.in_alt_target Real)
(declare-var logic_longitudinal.in_hdot Real)
(declare-var logic_longitudinal.in_fpa Real)
(declare-var logic_longitudinal.in_fpa_target Real)
(declare-var logic_longitudinal.in_pitch Real)
(declare-var logic_longitudinal.in_speed Real)
(declare-var logic_longitudinal.in_gskts Real)
(declare-var logic_longitudinal.in_cas Real)
(declare-var logic_longitudinal.in_elev Real)
(declare-var logic_longitudinal.in_ail Real)
(declare-var logic_longitudinal.out_head_engaged Bool)
(declare-var logic_longitudinal.out_alt_engaged Bool)
(declare-var logic_longitudinal.out_fpa_engaged Bool)
(declare-var logic_longitudinal.out_alt Real)
(declare-var logic_longitudinal.out_pitch Real)
(declare-var logic_longitudinal.out_elev Real)
(declare-var logic_longitudinal.OK1 Bool)
(declare-var logic_longitudinal.OK2 Bool)
(declare-var logic_longitudinal.OK3 Bool)
(declare-var logic_longitudinal.OK4 Bool)
(declare-var logic_longitudinal.OK5 Bool)
(declare-var logic_longitudinal.OK6 Bool)
(declare-var logic_longitudinal.OK7 Bool)
(declare-var logic_longitudinal.OK8 Bool)
(declare-var logic_longitudinal.OK9 Bool)
(declare-var logic_longitudinal.OK10 Bool)
(declare-var logic_longitudinal.OK11 Bool)
(declare-var logic_longitudinal.OK12 Bool)
(declare-var logic_longitudinal.OK13 Bool)
(declare-var logic_longitudinal.OK14 Bool)
(declare-var logic_longitudinal.OK15 Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x Int)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x Bool)
(declare-var logic_longitudinal.alt_tag_210 Bool)
(declare-var logic_longitudinal.alt_wag_170 Bool)
(declare-var logic_longitudinal.fpa_tag_210 Bool)
(declare-var logic_longitudinal.fpa_wag_180 Bool)
(declare-rel logic_longitudinal_reset (Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool))
(declare-rel logic_longitudinal_step (Bool Bool Bool Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool))

(rule (=> 
  (and 
       
       (longitudinal_reset logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c
                           logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m
                           logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
                           logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m)
       (logic_reset logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                    logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m)
  )
  (logic_longitudinal_reset logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c
                            logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c
                            logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
                            logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
                            logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
                            logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
                            logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
                            logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
                            logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                            logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m
                            logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m
                            logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m
                            logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                            logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                            logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                            logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                            logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                            logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
                            logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (and (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c)
            (= logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c)
            )
       (logic_step logic_longitudinal.in_head_engage
                   logic_longitudinal.in_alt_engage
                   logic_longitudinal.in_fpa_engage
                   (or (not (= logic_longitudinal.in_elev 0.0)) (not (= logic_longitudinal.in_ail 0.0)))
                   logic_longitudinal.in_alt
                   logic_longitudinal.in_alt_target
                   logic_longitudinal.out_head_engaged
                   logic_longitudinal.out_alt_engaged
                   logic_longitudinal.out_fpa_engaged
                   logic_longitudinal.alt_wag_170
                   logic_longitudinal.alt_tag_210
                   logic_longitudinal.fpa_wag_180
                   logic_longitudinal.fpa_tag_210
                   logic_longitudinal.OK1
                   logic_longitudinal.OK2
                   logic_longitudinal.OK3
                   logic_longitudinal.OK4
                   logic_longitudinal.OK5
                   logic_longitudinal.OK6
                   logic_longitudinal.OK7
                   logic_longitudinal.OK8
                   logic_longitudinal.OK9
                   logic_longitudinal.OK10
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x
                   logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x)
       (and (= logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c)
            (= logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c)
            (= logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c)
            )
       (longitudinal_step (or logic_longitudinal.alt_wag_170 logic_longitudinal.alt_tag_210)
                          (or logic_longitudinal.fpa_wag_180 logic_longitudinal.fpa_tag_210)
                          logic_longitudinal.in_alt
                          logic_longitudinal.in_alt_target
                          logic_longitudinal.in_hdot
                          logic_longitudinal.in_fpa
                          logic_longitudinal.in_fpa_target
                          logic_longitudinal.in_pitch
                          logic_longitudinal.in_speed
                          logic_longitudinal.in_gskts
                          logic_longitudinal.in_cas
                          logic_longitudinal.in_elev
                          logic_longitudinal.out_alt
                          logic_longitudinal.out_pitch
                          logic_longitudinal.out_elev
                          logic_longitudinal.OK11
                          logic_longitudinal.OK12
                          logic_longitudinal.OK13
                          logic_longitudinal.OK14
                          logic_longitudinal.OK15
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m
                          logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m
                          logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                          logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                          logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                          logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                          logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                          logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x
                          logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x
                          logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x
                          logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x
                          logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x
                          logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x
                          logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x
                          logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x
                          logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x
                          logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x)
       )
  (logic_longitudinal_step logic_longitudinal.in_head_engage
                           logic_longitudinal.in_alt_engage
                           logic_longitudinal.in_fpa_engage
                           logic_longitudinal.in_alt
                           logic_longitudinal.in_alt_target
                           logic_longitudinal.in_hdot
                           logic_longitudinal.in_fpa
                           logic_longitudinal.in_fpa_target
                           logic_longitudinal.in_pitch
                           logic_longitudinal.in_speed
                           logic_longitudinal.in_gskts
                           logic_longitudinal.in_cas
                           logic_longitudinal.in_elev
                           logic_longitudinal.in_ail
                           logic_longitudinal.out_head_engaged
                           logic_longitudinal.out_alt_engaged
                           logic_longitudinal.out_fpa_engaged
                           logic_longitudinal.out_alt
                           logic_longitudinal.out_pitch
                           logic_longitudinal.out_elev
                           logic_longitudinal.OK1
                           logic_longitudinal.OK2
                           logic_longitudinal.OK3
                           logic_longitudinal.OK4
                           logic_longitudinal.OK5
                           logic_longitudinal.OK6
                           logic_longitudinal.OK7
                           logic_longitudinal.OK8
                           logic_longitudinal.OK9
                           logic_longitudinal.OK10
                           logic_longitudinal.OK11
                           logic_longitudinal.OK12
                           logic_longitudinal.OK13
                           logic_longitudinal.OK14
                           logic_longitudinal.OK15
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c
                           logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
                           logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x
                           logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x
                           logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x
                           logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x
                           logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x
                           logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x
                           logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x
                           logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x
                           logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x
                           logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x
                           logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x)
))

; temporal_divid
(declare-var temporal_divid.num Real)
(declare-var temporal_divid.den Real)
(declare-var temporal_divid.res Real)
(declare-var temporal_divid.out Real)
(declare-var temporal_divid.__temporal_divid_12_c Real)
(declare-var temporal_divid.__temporal_divid_14_c Real)
(declare-var temporal_divid.__temporal_divid_16_c Real)
(declare-var temporal_divid.__temporal_divid_19_c Real)
(declare-var temporal_divid.__temporal_divid_21_c Real)
(declare-var temporal_divid.__temporal_divid_23_c Real)
(declare-var temporal_divid.__temporal_divid_26_c Real)
(declare-var temporal_divid.__temporal_divid_28_c Real)
(declare-var temporal_divid.__temporal_divid_30_c Real)
(declare-var temporal_divid.__temporal_divid_33_c Real)
(declare-var temporal_divid.__temporal_divid_35_c Real)
(declare-var temporal_divid.__temporal_divid_37_c Real)
(declare-var temporal_divid.__temporal_divid_40_c Real)
(declare-var temporal_divid.__temporal_divid_42_c Real)
(declare-var temporal_divid.__temporal_divid_44_c Real)
(declare-var temporal_divid.__temporal_divid_47_c Real)
(declare-var temporal_divid.__temporal_divid_49_c Real)
(declare-var temporal_divid.__temporal_divid_5_c Real)
(declare-var temporal_divid.__temporal_divid_51_c Real)
(declare-var temporal_divid.__temporal_divid_7_c Real)
(declare-var temporal_divid.__temporal_divid_9_c Real)
(declare-var temporal_divid.ni_2._arrow._first_c Bool)
(declare-var temporal_divid.ni_3._arrow._first_c Bool)
(declare-var temporal_divid.ni_4._arrow._first_c Bool)
(declare-var temporal_divid.ni_5._arrow._first_c Bool)
(declare-var temporal_divid.ni_6._arrow._first_c Bool)
(declare-var temporal_divid.ni_7._arrow._first_c Bool)
(declare-var temporal_divid.ni_8._arrow._first_c Bool)
(declare-var temporal_divid.__temporal_divid_12_m Real)
(declare-var temporal_divid.__temporal_divid_14_m Real)
(declare-var temporal_divid.__temporal_divid_16_m Real)
(declare-var temporal_divid.__temporal_divid_19_m Real)
(declare-var temporal_divid.__temporal_divid_21_m Real)
(declare-var temporal_divid.__temporal_divid_23_m Real)
(declare-var temporal_divid.__temporal_divid_26_m Real)
(declare-var temporal_divid.__temporal_divid_28_m Real)
(declare-var temporal_divid.__temporal_divid_30_m Real)
(declare-var temporal_divid.__temporal_divid_33_m Real)
(declare-var temporal_divid.__temporal_divid_35_m Real)
(declare-var temporal_divid.__temporal_divid_37_m Real)
(declare-var temporal_divid.__temporal_divid_40_m Real)
(declare-var temporal_divid.__temporal_divid_42_m Real)
(declare-var temporal_divid.__temporal_divid_44_m Real)
(declare-var temporal_divid.__temporal_divid_47_m Real)
(declare-var temporal_divid.__temporal_divid_49_m Real)
(declare-var temporal_divid.__temporal_divid_5_m Real)
(declare-var temporal_divid.__temporal_divid_51_m Real)
(declare-var temporal_divid.__temporal_divid_7_m Real)
(declare-var temporal_divid.__temporal_divid_9_m Real)
(declare-var temporal_divid.ni_2._arrow._first_m Bool)
(declare-var temporal_divid.ni_3._arrow._first_m Bool)
(declare-var temporal_divid.ni_4._arrow._first_m Bool)
(declare-var temporal_divid.ni_5._arrow._first_m Bool)
(declare-var temporal_divid.ni_6._arrow._first_m Bool)
(declare-var temporal_divid.ni_7._arrow._first_m Bool)
(declare-var temporal_divid.ni_8._arrow._first_m Bool)
(declare-var temporal_divid.__temporal_divid_12_x Real)
(declare-var temporal_divid.__temporal_divid_14_x Real)
(declare-var temporal_divid.__temporal_divid_16_x Real)
(declare-var temporal_divid.__temporal_divid_19_x Real)
(declare-var temporal_divid.__temporal_divid_21_x Real)
(declare-var temporal_divid.__temporal_divid_23_x Real)
(declare-var temporal_divid.__temporal_divid_26_x Real)
(declare-var temporal_divid.__temporal_divid_28_x Real)
(declare-var temporal_divid.__temporal_divid_30_x Real)
(declare-var temporal_divid.__temporal_divid_33_x Real)
(declare-var temporal_divid.__temporal_divid_35_x Real)
(declare-var temporal_divid.__temporal_divid_37_x Real)
(declare-var temporal_divid.__temporal_divid_40_x Real)
(declare-var temporal_divid.__temporal_divid_42_x Real)
(declare-var temporal_divid.__temporal_divid_44_x Real)
(declare-var temporal_divid.__temporal_divid_47_x Real)
(declare-var temporal_divid.__temporal_divid_49_x Real)
(declare-var temporal_divid.__temporal_divid_5_x Real)
(declare-var temporal_divid.__temporal_divid_51_x Real)
(declare-var temporal_divid.__temporal_divid_7_x Real)
(declare-var temporal_divid.__temporal_divid_9_x Real)
(declare-var temporal_divid.ni_2._arrow._first_x Bool)
(declare-var temporal_divid.ni_3._arrow._first_x Bool)
(declare-var temporal_divid.ni_4._arrow._first_x Bool)
(declare-var temporal_divid.ni_5._arrow._first_x Bool)
(declare-var temporal_divid.ni_6._arrow._first_x Bool)
(declare-var temporal_divid.ni_7._arrow._first_x Bool)
(declare-var temporal_divid.ni_8._arrow._first_x Bool)
(declare-var temporal_divid.__temporal_divid_10 Bool)
(declare-var temporal_divid.__temporal_divid_11 Bool)
(declare-var temporal_divid.__temporal_divid_13 Bool)
(declare-var temporal_divid.__temporal_divid_15 Bool)
(declare-var temporal_divid.__temporal_divid_17 Bool)
(declare-var temporal_divid.__temporal_divid_18 Bool)
(declare-var temporal_divid.__temporal_divid_20 Bool)
(declare-var temporal_divid.__temporal_divid_22 Bool)
(declare-var temporal_divid.__temporal_divid_24 Bool)
(declare-var temporal_divid.__temporal_divid_25 Bool)
(declare-var temporal_divid.__temporal_divid_27 Bool)
(declare-var temporal_divid.__temporal_divid_29 Bool)
(declare-var temporal_divid.__temporal_divid_31 Bool)
(declare-var temporal_divid.__temporal_divid_32 Bool)
(declare-var temporal_divid.__temporal_divid_34 Bool)
(declare-var temporal_divid.__temporal_divid_36 Bool)
(declare-var temporal_divid.__temporal_divid_38 Bool)
(declare-var temporal_divid.__temporal_divid_39 Bool)
(declare-var temporal_divid.__temporal_divid_4 Bool)
(declare-var temporal_divid.__temporal_divid_41 Bool)
(declare-var temporal_divid.__temporal_divid_43 Bool)
(declare-var temporal_divid.__temporal_divid_45 Bool)
(declare-var temporal_divid.__temporal_divid_46 Bool)
(declare-var temporal_divid.__temporal_divid_48 Bool)
(declare-var temporal_divid.__temporal_divid_50 Bool)
(declare-var temporal_divid.__temporal_divid_52 Bool)
(declare-var temporal_divid.__temporal_divid_6 Bool)
(declare-var temporal_divid.__temporal_divid_8 Bool)
(declare-var temporal_divid.abs_den Real)
(declare-var temporal_divid.abs_num Real)
(declare-var temporal_divid.abs_res Real)
(declare-rel temporal_divid_reset (Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool))
(declare-rel temporal_divid_step (Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       (= temporal_divid.__temporal_divid_12_m temporal_divid.__temporal_divid_12_c)
       (= temporal_divid.__temporal_divid_14_m temporal_divid.__temporal_divid_14_c)
       (= temporal_divid.__temporal_divid_16_m temporal_divid.__temporal_divid_16_c)
       (= temporal_divid.__temporal_divid_19_m temporal_divid.__temporal_divid_19_c)
       (= temporal_divid.__temporal_divid_21_m temporal_divid.__temporal_divid_21_c)
       (= temporal_divid.__temporal_divid_23_m temporal_divid.__temporal_divid_23_c)
       (= temporal_divid.__temporal_divid_26_m temporal_divid.__temporal_divid_26_c)
       (= temporal_divid.__temporal_divid_28_m temporal_divid.__temporal_divid_28_c)
       (= temporal_divid.__temporal_divid_30_m temporal_divid.__temporal_divid_30_c)
       (= temporal_divid.__temporal_divid_33_m temporal_divid.__temporal_divid_33_c)
       (= temporal_divid.__temporal_divid_35_m temporal_divid.__temporal_divid_35_c)
       (= temporal_divid.__temporal_divid_37_m temporal_divid.__temporal_divid_37_c)
       (= temporal_divid.__temporal_divid_40_m temporal_divid.__temporal_divid_40_c)
       (= temporal_divid.__temporal_divid_42_m temporal_divid.__temporal_divid_42_c)
       (= temporal_divid.__temporal_divid_44_m temporal_divid.__temporal_divid_44_c)
       (= temporal_divid.__temporal_divid_47_m temporal_divid.__temporal_divid_47_c)
       (= temporal_divid.__temporal_divid_49_m temporal_divid.__temporal_divid_49_c)
       (= temporal_divid.__temporal_divid_5_m temporal_divid.__temporal_divid_5_c)
       (= temporal_divid.__temporal_divid_51_m temporal_divid.__temporal_divid_51_c)
       (= temporal_divid.__temporal_divid_7_m temporal_divid.__temporal_divid_7_c)
       (= temporal_divid.__temporal_divid_9_m temporal_divid.__temporal_divid_9_c)
       (= temporal_divid.ni_8._arrow._first_m true)
       (= temporal_divid.ni_7._arrow._first_m true)
       (= temporal_divid.ni_6._arrow._first_m true)
       (= temporal_divid.ni_5._arrow._first_m true)
       (= temporal_divid.ni_4._arrow._first_m true)
       (= temporal_divid.ni_3._arrow._first_m true)
       (= temporal_divid.ni_2._arrow._first_m true)
  )
  (temporal_divid_reset temporal_divid.__temporal_divid_12_c
                        temporal_divid.__temporal_divid_14_c
                        temporal_divid.__temporal_divid_16_c
                        temporal_divid.__temporal_divid_19_c
                        temporal_divid.__temporal_divid_21_c
                        temporal_divid.__temporal_divid_23_c
                        temporal_divid.__temporal_divid_26_c
                        temporal_divid.__temporal_divid_28_c
                        temporal_divid.__temporal_divid_30_c
                        temporal_divid.__temporal_divid_33_c
                        temporal_divid.__temporal_divid_35_c
                        temporal_divid.__temporal_divid_37_c
                        temporal_divid.__temporal_divid_40_c
                        temporal_divid.__temporal_divid_42_c
                        temporal_divid.__temporal_divid_44_c
                        temporal_divid.__temporal_divid_47_c
                        temporal_divid.__temporal_divid_49_c
                        temporal_divid.__temporal_divid_5_c
                        temporal_divid.__temporal_divid_51_c
                        temporal_divid.__temporal_divid_7_c
                        temporal_divid.__temporal_divid_9_c
                        temporal_divid.ni_2._arrow._first_c
                        temporal_divid.ni_3._arrow._first_c
                        temporal_divid.ni_4._arrow._first_c
                        temporal_divid.ni_5._arrow._first_c
                        temporal_divid.ni_6._arrow._first_c
                        temporal_divid.ni_7._arrow._first_c
                        temporal_divid.ni_8._arrow._first_c
                        temporal_divid.__temporal_divid_12_m
                        temporal_divid.__temporal_divid_14_m
                        temporal_divid.__temporal_divid_16_m
                        temporal_divid.__temporal_divid_19_m
                        temporal_divid.__temporal_divid_21_m
                        temporal_divid.__temporal_divid_23_m
                        temporal_divid.__temporal_divid_26_m
                        temporal_divid.__temporal_divid_28_m
                        temporal_divid.__temporal_divid_30_m
                        temporal_divid.__temporal_divid_33_m
                        temporal_divid.__temporal_divid_35_m
                        temporal_divid.__temporal_divid_37_m
                        temporal_divid.__temporal_divid_40_m
                        temporal_divid.__temporal_divid_42_m
                        temporal_divid.__temporal_divid_44_m
                        temporal_divid.__temporal_divid_47_m
                        temporal_divid.__temporal_divid_49_m
                        temporal_divid.__temporal_divid_5_m
                        temporal_divid.__temporal_divid_51_m
                        temporal_divid.__temporal_divid_7_m
                        temporal_divid.__temporal_divid_9_m
                        temporal_divid.ni_2._arrow._first_m
                        temporal_divid.ni_3._arrow._first_m
                        temporal_divid.ni_4._arrow._first_m
                        temporal_divid.ni_5._arrow._first_m
                        temporal_divid.ni_6._arrow._first_m
                        temporal_divid.ni_7._arrow._first_m
                        temporal_divid.ni_8._arrow._first_m)
))

; Step rule with Assertions 
(rule (=> 
  (and 
  (and (= temporal_divid.out temporal_divid.res)
       (and (or (not (= (< temporal_divid.res 0.0) true))
               (= temporal_divid.abs_res (- temporal_divid.res)))
            (or (not (= (< temporal_divid.res 0.0) false))
               (= temporal_divid.abs_res temporal_divid.res))
       )
       (and (or (not (= (< temporal_divid.num 0.0) true))
               (= temporal_divid.abs_num (- temporal_divid.num)))
            (or (not (= (< temporal_divid.num 0.0) false))
               (= temporal_divid.abs_num temporal_divid.num))
       )
       (and (or (not (= (< temporal_divid.den 0.0) true))
               (= temporal_divid.abs_den (- temporal_divid.den)))
            (or (not (= (< temporal_divid.den 0.0) false))
               (= temporal_divid.abs_den temporal_divid.den))
       )
       (= temporal_divid.ni_8._arrow._first_m temporal_divid.ni_8._arrow._first_c)
       (and (= temporal_divid.__temporal_divid_4 (ite temporal_divid.ni_8._arrow._first_m true false))
            (= temporal_divid.ni_8._arrow._first_x false))
       (and (or (not (= temporal_divid.__temporal_divid_4 true))
               (= temporal_divid.__temporal_divid_10 false))
            (or (not (= temporal_divid.__temporal_divid_4 false))
               (= temporal_divid.__temporal_divid_10 (> temporal_divid.num temporal_divid.__temporal_divid_9_c)))
       )
       (= temporal_divid.__temporal_divid_9_x temporal_divid.num)
       (and (or (not (= temporal_divid.__temporal_divid_4 true))
               (= temporal_divid.__temporal_divid_8 false))
            (or (not (= temporal_divid.__temporal_divid_4 false))
               (= temporal_divid.__temporal_divid_8 (= temporal_divid.den temporal_divid.__temporal_divid_7_c)))
       )
       (= temporal_divid.__temporal_divid_7_x temporal_divid.den)
       (and (or (not (= temporal_divid.__temporal_divid_4 true))
               (= temporal_divid.__temporal_divid_6 false))
            (or (not (= temporal_divid.__temporal_divid_4 false))
               (= temporal_divid.__temporal_divid_6 (> temporal_divid.res temporal_divid.__temporal_divid_5_c)))
       )
       (= temporal_divid.ni_7._arrow._first_m temporal_divid.ni_7._arrow._first_c)
       (and (= temporal_divid.__temporal_divid_46 (ite temporal_divid.ni_7._arrow._first_m true false))
            (= temporal_divid.ni_7._arrow._first_x false))
       (and (or (not (= temporal_divid.__temporal_divid_46 true))
               (= temporal_divid.__temporal_divid_52 false))
            (or (not (= temporal_divid.__temporal_divid_46 false))
               (= temporal_divid.__temporal_divid_52 (= temporal_divid.num temporal_divid.__temporal_divid_51_c)))
       )
       (= temporal_divid.__temporal_divid_51_x temporal_divid.num)
       (and (or (not (= temporal_divid.__temporal_divid_46 true))
               (= temporal_divid.__temporal_divid_50 false))
            (or (not (= temporal_divid.__temporal_divid_46 false))
               (= temporal_divid.__temporal_divid_50 (= temporal_divid.den temporal_divid.__temporal_divid_49_c)))
       )
       (= temporal_divid.__temporal_divid_5_x temporal_divid.res)
       (= temporal_divid.__temporal_divid_49_x temporal_divid.den)
       (and (or (not (= temporal_divid.__temporal_divid_46 true))
               (= temporal_divid.__temporal_divid_48 false))
            (or (not (= temporal_divid.__temporal_divid_46 false))
               (= temporal_divid.__temporal_divid_48 (= temporal_divid.res temporal_divid.__temporal_divid_47_c)))
       )
       (= temporal_divid.__temporal_divid_47_x temporal_divid.res)
       (= temporal_divid.ni_6._arrow._first_m temporal_divid.ni_6._arrow._first_c)
       (and (= temporal_divid.__temporal_divid_39 (ite temporal_divid.ni_6._arrow._first_m true false))
            (= temporal_divid.ni_6._arrow._first_x false))
       (and (or (not (= temporal_divid.__temporal_divid_39 true))
               (= temporal_divid.__temporal_divid_45 false))
            (or (not (= temporal_divid.__temporal_divid_39 false))
               (= temporal_divid.__temporal_divid_45 (< temporal_divid.num temporal_divid.__temporal_divid_44_c)))
       )
       (= temporal_divid.__temporal_divid_44_x temporal_divid.num)
       (and (or (not (= temporal_divid.__temporal_divid_39 true))
               (= temporal_divid.__temporal_divid_43 false))
            (or (not (= temporal_divid.__temporal_divid_39 false))
               (= temporal_divid.__temporal_divid_43 (> temporal_divid.den temporal_divid.__temporal_divid_42_c)))
       )
       (= temporal_divid.__temporal_divid_42_x temporal_divid.den)
       (and (or (not (= temporal_divid.__temporal_divid_39 true))
               (= temporal_divid.__temporal_divid_41 false))
            (or (not (= temporal_divid.__temporal_divid_39 false))
               (= temporal_divid.__temporal_divid_41 (< temporal_divid.res temporal_divid.__temporal_divid_40_c)))
       )
       (= temporal_divid.__temporal_divid_40_x temporal_divid.res)
       (= temporal_divid.ni_5._arrow._first_m temporal_divid.ni_5._arrow._first_c)
       (and (= temporal_divid.__temporal_divid_32 (ite temporal_divid.ni_5._arrow._first_m true false))
            (= temporal_divid.ni_5._arrow._first_x false))
       (and (or (not (= temporal_divid.__temporal_divid_32 true))
               (= temporal_divid.__temporal_divid_38 false))
            (or (not (= temporal_divid.__temporal_divid_32 false))
               (= temporal_divid.__temporal_divid_38 (= temporal_divid.num temporal_divid.__temporal_divid_37_c)))
       )
       (= temporal_divid.__temporal_divid_37_x temporal_divid.num)
       (and (or (not (= temporal_divid.__temporal_divid_32 true))
               (= temporal_divid.__temporal_divid_36 false))
            (or (not (= temporal_divid.__temporal_divid_32 false))
               (= temporal_divid.__temporal_divid_36 (> temporal_divid.den temporal_divid.__temporal_divid_35_c)))
       )
       (= temporal_divid.__temporal_divid_35_x temporal_divid.den)
       (and (or (not (= temporal_divid.__temporal_divid_32 true))
               (= temporal_divid.__temporal_divid_34 false))
            (or (not (= temporal_divid.__temporal_divid_32 false))
               (= temporal_divid.__temporal_divid_34 (or (< temporal_divid.res temporal_divid.__temporal_divid_33_c) (= temporal_divid.res 0.0))))
       )
       (= temporal_divid.__temporal_divid_33_x temporal_divid.res)
       (= temporal_divid.ni_4._arrow._first_m temporal_divid.ni_4._arrow._first_c)
       (and (= temporal_divid.__temporal_divid_25 (ite temporal_divid.ni_4._arrow._first_m true false))
            (= temporal_divid.ni_4._arrow._first_x false))
       (and (or (not (= temporal_divid.__temporal_divid_25 true))
               (= temporal_divid.__temporal_divid_31 false))
            (or (not (= temporal_divid.__temporal_divid_25 false))
               (= temporal_divid.__temporal_divid_31 (< temporal_divid.num temporal_divid.__temporal_divid_30_c)))
       )
       (= temporal_divid.__temporal_divid_30_x temporal_divid.num)
       (and (or (not (= temporal_divid.__temporal_divid_25 true))
               (= temporal_divid.__temporal_divid_29 false))
            (or (not (= temporal_divid.__temporal_divid_25 false))
               (= temporal_divid.__temporal_divid_29 (= temporal_divid.den temporal_divid.__temporal_divid_28_c)))
       )
       (= temporal_divid.__temporal_divid_28_x temporal_divid.den)
       (and (or (not (= temporal_divid.__temporal_divid_25 true))
               (= temporal_divid.__temporal_divid_27 false))
            (or (not (= temporal_divid.__temporal_divid_25 false))
               (= temporal_divid.__temporal_divid_27 (< temporal_divid.res temporal_divid.__temporal_divid_26_c)))
       )
       (= temporal_divid.__temporal_divid_26_x temporal_divid.res)
       (= temporal_divid.ni_3._arrow._first_m temporal_divid.ni_3._arrow._first_c)
       (and (= temporal_divid.__temporal_divid_18 (ite temporal_divid.ni_3._arrow._first_m true false))
            (= temporal_divid.ni_3._arrow._first_x false))
       (and (or (not (= temporal_divid.__temporal_divid_18 true))
               (= temporal_divid.__temporal_divid_24 false))
            (or (not (= temporal_divid.__temporal_divid_18 false))
               (= temporal_divid.__temporal_divid_24 (> temporal_divid.num temporal_divid.__temporal_divid_23_c)))
       )
       (= temporal_divid.__temporal_divid_23_x temporal_divid.num)
       (and (or (not (= temporal_divid.__temporal_divid_18 true))
               (= temporal_divid.__temporal_divid_22 false))
            (or (not (= temporal_divid.__temporal_divid_18 false))
               (= temporal_divid.__temporal_divid_22 (< temporal_divid.den temporal_divid.__temporal_divid_21_c)))
       )
       (= temporal_divid.__temporal_divid_21_x temporal_divid.den)
       (and (or (not (= temporal_divid.__temporal_divid_18 true))
               (= temporal_divid.__temporal_divid_20 false))
            (or (not (= temporal_divid.__temporal_divid_18 false))
               (= temporal_divid.__temporal_divid_20 (> temporal_divid.res temporal_divid.__temporal_divid_19_c)))
       )
       (= temporal_divid.__temporal_divid_19_x temporal_divid.res)
       (= temporal_divid.ni_2._arrow._first_m temporal_divid.ni_2._arrow._first_c)
       (and (= temporal_divid.__temporal_divid_11 (ite temporal_divid.ni_2._arrow._first_m true false))
            (= temporal_divid.ni_2._arrow._first_x false))
       (and (or (not (= temporal_divid.__temporal_divid_11 true))
               (= temporal_divid.__temporal_divid_17 false))
            (or (not (= temporal_divid.__temporal_divid_11 false))
               (= temporal_divid.__temporal_divid_17 (= temporal_divid.num temporal_divid.__temporal_divid_16_c)))
       )
       (= temporal_divid.__temporal_divid_16_x temporal_divid.num)
       (and (or (not (= temporal_divid.__temporal_divid_11 true))
               (= temporal_divid.__temporal_divid_15 false))
            (or (not (= temporal_divid.__temporal_divid_11 false))
               (= temporal_divid.__temporal_divid_15 (< temporal_divid.den temporal_divid.__temporal_divid_14_c)))
       )
       (= temporal_divid.__temporal_divid_14_x temporal_divid.den)
       (and (or (not (= temporal_divid.__temporal_divid_11 true))
               (= temporal_divid.__temporal_divid_13 false))
            (or (not (= temporal_divid.__temporal_divid_11 false))
               (= temporal_divid.__temporal_divid_13 (or (> temporal_divid.res temporal_divid.__temporal_divid_12_c) (= temporal_divid.res 0.0))))
       )
       (= temporal_divid.__temporal_divid_12_x temporal_divid.res)
       )
 (and (=> (and temporal_divid.__temporal_divid_52 temporal_divid.__temporal_divid_50) temporal_divid.__temporal_divid_48)
      (=> (and temporal_divid.__temporal_divid_45 temporal_divid.__temporal_divid_43) temporal_divid.__temporal_divid_41)
      (=> (and temporal_divid.__temporal_divid_38 temporal_divid.__temporal_divid_36) temporal_divid.__temporal_divid_34)
      (=> (and temporal_divid.__temporal_divid_31 temporal_divid.__temporal_divid_29) temporal_divid.__temporal_divid_27)
      (=> (and temporal_divid.__temporal_divid_24 temporal_divid.__temporal_divid_22) temporal_divid.__temporal_divid_20)
      (=> (and temporal_divid.__temporal_divid_17 temporal_divid.__temporal_divid_15) temporal_divid.__temporal_divid_13)
      (=> (and temporal_divid.__temporal_divid_10 temporal_divid.__temporal_divid_8) temporal_divid.__temporal_divid_6)
      (= (= temporal_divid.res (- 1.0)) (= temporal_divid.num (- temporal_divid.den)))
      (= (= temporal_divid.res 1.0) (= temporal_divid.num temporal_divid.den))
      (= (<= temporal_divid.abs_res temporal_divid.abs_num) (or (>= temporal_divid.abs_den 1.0) (= temporal_divid.num 0.0)))
      (= (>= temporal_divid.abs_res temporal_divid.abs_num) (or (<= temporal_divid.abs_den 1.0) (= temporal_divid.num 0.0)))
      (= (< temporal_divid.res 0.0) (or (and (> temporal_divid.num 0.0) (< temporal_divid.den 0.0)) (and (< temporal_divid.num 0.0) (> temporal_divid.den 0.0))))
      (= (> temporal_divid.res 0.0) (or (and (> temporal_divid.num 0.0) (> temporal_divid.den 0.0)) (and (< temporal_divid.num 0.0) (< temporal_divid.den 0.0))))
      (= (= temporal_divid.num 0.0) (= temporal_divid.res 0.0))
      (= (= temporal_divid.res (- temporal_divid.num)) (or (= temporal_divid.den (- 1.0)) (= temporal_divid.num 0.0)))
      (= (= temporal_divid.res temporal_divid.num) (or (= temporal_divid.den 1.0) (= temporal_divid.num 0.0)))
      (not (= temporal_divid.den 0.0)) ))
(temporal_divid_step temporal_divid.num temporal_divid.den temporal_divid.res temporal_divid.out temporal_divid.__temporal_divid_12_c temporal_divid.__temporal_divid_14_c temporal_divid.__temporal_divid_16_c temporal_divid.__temporal_divid_19_c temporal_divid.__temporal_divid_21_c temporal_divid.__temporal_divid_23_c temporal_divid.__temporal_divid_26_c temporal_divid.__temporal_divid_28_c temporal_divid.__temporal_divid_30_c temporal_divid.__temporal_divid_33_c temporal_divid.__temporal_divid_35_c temporal_divid.__temporal_divid_37_c temporal_divid.__temporal_divid_40_c temporal_divid.__temporal_divid_42_c temporal_divid.__temporal_divid_44_c temporal_divid.__temporal_divid_47_c temporal_divid.__temporal_divid_49_c temporal_divid.__temporal_divid_5_c temporal_divid.__temporal_divid_51_c temporal_divid.__temporal_divid_7_c temporal_divid.__temporal_divid_9_c temporal_divid.ni_2._arrow._first_c temporal_divid.ni_3._arrow._first_c temporal_divid.ni_4._arrow._first_c temporal_divid.ni_5._arrow._first_c temporal_divid.ni_6._arrow._first_c temporal_divid.ni_7._arrow._first_c temporal_divid.ni_8._arrow._first_c temporal_divid.__temporal_divid_12_x temporal_divid.__temporal_divid_14_x temporal_divid.__temporal_divid_16_x temporal_divid.__temporal_divid_19_x temporal_divid.__temporal_divid_21_x temporal_divid.__temporal_divid_23_x temporal_divid.__temporal_divid_26_x temporal_divid.__temporal_divid_28_x temporal_divid.__temporal_divid_30_x temporal_divid.__temporal_divid_33_x temporal_divid.__temporal_divid_35_x temporal_divid.__temporal_divid_37_x temporal_divid.__temporal_divid_40_x temporal_divid.__temporal_divid_42_x temporal_divid.__temporal_divid_44_x temporal_divid.__temporal_divid_47_x temporal_divid.__temporal_divid_49_x temporal_divid.__temporal_divid_5_x temporal_divid.__temporal_divid_51_x temporal_divid.__temporal_divid_7_x temporal_divid.__temporal_divid_9_x temporal_divid.ni_2._arrow._first_x temporal_divid.ni_3._arrow._first_x temporal_divid.ni_4._arrow._first_x temporal_divid.ni_5._arrow._first_x temporal_divid.ni_6._arrow._first_x temporal_divid.ni_7._arrow._first_x temporal_divid.ni_8._arrow._first_x)
))

; top
(declare-var top.head_mode Real)
(declare-var top.ail_stick_in Real)
(declare-var top.elev_stick_in Real)
(declare-var top.alt_mode Real)
(declare-var top.fpa_mode Real)
(declare-var top.at_mode Real)
(declare-var top.alt_cmd_in Real)
(declare-var top.alt_in Real)
(declare-var top.cas_in Real)
(declare-var top.cas_cmd Real)
(declare-var top.pitch_in Real)
(declare-var top.qbdegf_1_1 Real)
(declare-var top.gskts_in Real)
(declare-var top.hdot_in Real)
(declare-var top.speed_in Real)
(declare-var top.fpa_in Real)
(declare-var top.fpa_cmd_in Real)
(declare-var top.d_out1 Real)
(declare-var top.d_out2 Real)
(declare-var top.t_out3 Real)
(declare-var top.t_out4 Real)
(declare-var top.d_out4 Real)
(declare-var top.t_out5 Real)
(declare-var top.d_out5 Real)
(declare-var top.t_out6 Real)
(declare-var top.d_out6 Real)
(declare-var top.OK1 Bool)
(declare-var top.OK2 Bool)
(declare-var top.OK3 Bool)
(declare-var top.OK4 Bool)
(declare-var top.OK5 Bool)
(declare-var top.OK6 Bool)
(declare-var top.OK7 Bool)
(declare-var top.OK8 Bool)
(declare-var top.OK9 Bool)
(declare-var top.OK10 Bool)
(declare-var top.OK11 Bool)
(declare-var top.OK12 Bool)
(declare-var top.OK13 Bool)
(declare-var top.OK14 Bool)
(declare-var top.OK15 Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x Int)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x Real)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x Bool)
(declare-var top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x Real)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x Bool)
(declare-var top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x Bool)
(declare-var top.alt_cmd_out Real)
(declare-var top.alt_eng_out Bool)
(declare-var top.at_eng_out Bool)
(declare-var top.cas_cmd_out Real)
(declare-var top.elev_cmd_out Real)
(declare-var top.fpa_eng_out Bool)
(declare-var top.head_eng_out Bool)
(declare-var top.pitch_cmd_out Real)
(declare-rel top_reset (Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool))
(declare-rel top_step (Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool))

(rule (=> 
  (and 
       
       (Mode_plus_Longitudinal_reset top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                                     top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
       (logic_longitudinal_reset top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
                                 top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m)
  )
  (top_reset top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
             top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c
             top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
             top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
             top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
             top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m
             top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_m
             top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
             top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
))

; Step rule 
(rule (=> 
  (and (and (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_m top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c)
            (= top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c)
            )
       (Mode_plus_Longitudinal_step top.head_mode
                                    top.ail_stick_in
                                    top.elev_stick_in
                                    top.alt_mode
                                    top.fpa_mode
                                    top.at_mode
                                    top.alt_cmd_in
                                    top.alt_in
                                    top.cas_in
                                    top.cas_cmd
                                    top.pitch_in
                                    top.qbdegf_1_1
                                    top.gskts_in
                                    top.hdot_in
                                    top.speed_in
                                    top.fpa_in
                                    top.fpa_cmd_in
                                    top.d_out1
                                    top.d_out2
                                    top.t_out3
                                    top.t_out4
                                    top.d_out4
                                    top.t_out5
                                    top.d_out5
                                    top.t_out6
                                    top.d_out6
                                    top.head_eng_out
                                    top.alt_eng_out
                                    top.at_eng_out
                                    top.fpa_eng_out
                                    top.cas_cmd_out
                                    top.alt_cmd_out
                                    top.pitch_cmd_out
                                    top.elev_cmd_out
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x
                                    top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
       (and (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c)
            (= top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c)
            )
       (logic_longitudinal_step (not (= top.head_mode 0.0))
                                (not (= top.alt_mode 0.0))
                                (not (= top.fpa_mode 0.0))
                                top.alt_in
                                top.alt_cmd_in
                                top.hdot_in
                                top.fpa_in
                                top.fpa_cmd_in
                                top.pitch_in
                                top.speed_in
                                top.gskts_in
                                top.cas_in
                                top.elev_stick_in
                                top.ail_stick_in
                                top.head_eng_out
                                top.alt_eng_out
                                top.fpa_eng_out
                                top.alt_cmd_out
                                top.pitch_cmd_out
                                top.elev_cmd_out
                                top.OK1
                                top.OK2
                                top.OK3
                                top.OK4
                                top.OK5
                                top.OK6
                                top.OK7
                                top.OK8
                                top.OK9
                                top.OK10
                                top.OK11
                                top.OK12
                                top.OK13
                                top.OK14
                                top.OK15
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x
                                top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x)
       )
  (top_step top.head_mode
            top.ail_stick_in
            top.elev_stick_in
            top.alt_mode
            top.fpa_mode
            top.at_mode
            top.alt_cmd_in
            top.alt_in
            top.cas_in
            top.cas_cmd
            top.pitch_in
            top.qbdegf_1_1
            top.gskts_in
            top.hdot_in
            top.speed_in
            top.fpa_in
            top.fpa_cmd_in
            top.d_out1
            top.d_out2
            top.t_out3
            top.t_out4
            top.d_out4
            top.t_out5
            top.d_out5
            top.t_out6
            top.d_out6
            top.OK1
            top.OK2
            top.OK3
            top.OK4
            top.OK5
            top.OK6
            top.OK7
            top.OK8
            top.OK9
            top.OK10
            top.OK11
            top.OK12
            top.OK13
            top.OK14
            top.OK15
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c
            top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c
            top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x
            top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x
            top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x
            top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x
            top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x
            top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x
            top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
))

; Collecting semantics for node top

(declare-rel MAIN (Int Int Bool Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Int Bool Int Bool Int Bool Int Int Int Int Int Bool Int Int Bool Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Bool Int Bool Bool Int Bool Bool Bool Bool Bool Bool Bool Bool Real Real Real Real Bool Real Real Real Real Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Real Bool Real Bool Real Bool Real Bool Real Bool Bool Real Bool Bool Real Real Bool Bool Real Real Bool Bool Bool Bool Bool Bool Real Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool Bool))
; Initial set: Reset(c,m) + One Step(m,x) 
(declare-rel INIT_STATE ())
(rule INIT_STATE)
(rule (=> 
  (and INIT_STATE
       (top_reset top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_m top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m)
       (top_step top.head_mode top.ail_stick_in top.elev_stick_in top.alt_mode top.fpa_mode top.at_mode top.alt_cmd_in top.alt_in top.cas_in top.cas_cmd top.pitch_in top.qbdegf_1_1 top.gskts_in top.hdot_in top.speed_in top.fpa_in top.fpa_cmd_in top.d_out1 top.d_out2 top.t_out3 top.t_out4 top.d_out4 top.t_out5 top.d_out5 top.t_out6 top.d_out6 top.OK1 top.OK2 top.OK3 top.OK4 top.OK5 top.OK6 top.OK7 top.OK8 top.OK9 top.OK10 top.OK11 top.OK12 top.OK13 top.OK14 top.OK15 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_m top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_m top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_m top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_m top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_m top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_m top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
  )
  (MAIN top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x top.OK1 top.OK2 top.OK3 top.OK4 top.OK5 top.OK6 top.OK7 top.OK8 top.OK9 top.OK10 top.OK11 top.OK12 top.OK13 top.OK14 top.OK15)
))

; Inductive def
(declare-var dummytop.OK1 Bool)
 (declare-var dummytop.OK2 Bool)
 (declare-var dummytop.OK3 Bool)
 (declare-var dummytop.OK4 Bool)
 (declare-var dummytop.OK5 Bool)
 (declare-var dummytop.OK6 Bool)
 (declare-var dummytop.OK7 Bool)
 (declare-var dummytop.OK8 Bool)
 (declare-var dummytop.OK9 Bool)
 (declare-var dummytop.OK10 Bool)
 (declare-var dummytop.OK11 Bool)
 (declare-var dummytop.OK12 Bool)
 (declare-var dummytop.OK13 Bool)
 (declare-var dummytop.OK14 Bool)
 (declare-var dummytop.OK15 Bool)
(rule (=> 
  (and (MAIN top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c dummytop.OK1 dummytop.OK2 dummytop.OK3 dummytop.OK4 dummytop.OK5 dummytop.OK6 dummytop.OK7 dummytop.OK8 dummytop.OK9 dummytop.OK10 dummytop.OK11 dummytop.OK12 dummytop.OK13 dummytop.OK14 dummytop.OK15)
       (top_step top.head_mode top.ail_stick_in top.elev_stick_in top.alt_mode top.fpa_mode top.at_mode top.alt_cmd_in top.alt_in top.cas_in top.cas_cmd top.pitch_in top.qbdegf_1_1 top.gskts_in top.hdot_in top.speed_in top.fpa_in top.fpa_cmd_in top.d_out1 top.d_out2 top.t_out3 top.t_out4 top.d_out4 top.t_out5 top.d_out5 top.t_out6 top.d_out6 top.OK1 top.OK2 top.OK3 top.OK4 top.OK5 top.OK6 top.OK7 top.OK8 top.OK9 top.OK10 top.OK11 top.OK12 top.OK13 top.OK14 top.OK15 top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_c top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_c top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_c top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_c top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_c top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_c top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x)
  )
  (MAIN top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x top.OK1 top.OK2 top.OK3 top.OK4 top.OK5 top.OK6 top.OK7 top.OK8 top.OK9 top.OK10 top.OK11 top.OK12 top.OK13 top.OK14 top.OK15)
))

; Property def
(declare-rel ERR ())
(rule (=> 
  (and (not (and top.OK1
                 top.OK2
                 top.OK3
                 top.OK4
                 top.OK5
                 top.OK6
                 top.OK7
                 top.OK8
                 top.OK9
                 top.OK10
                 top.OK11
                 top.OK12
                 top.OK13
                 top.OK14
                 top.OK15
       ))
       (MAIN top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_11_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_14_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_3_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_5_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.__logic_heading_8_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_33.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_34._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_35._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_36._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_37._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_38.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_39.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_40.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_41._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_42.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_43.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_44.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_24.logic_heading.ni_45.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_12_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_17_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_22_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_27_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_32_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.__logic_alt_fpa_7_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.__duration_4_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_46.duration.ni_81._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_47.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_48._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_49.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_50.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_51._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_52.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_53.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_54._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_55.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_56.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_57._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_58.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_59.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_60._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_61.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_62.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_63.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_64.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_65.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_66.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_67._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.__since_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_68.since.ni_78._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_69.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.__has_been_true_reset_2_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_70.has_been_true_reset.ni_79._arrow._first_x top.ni_0.logic_longitudinal.ni_10.logic.ni_25.logic_alt_fpa.ni_71._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_10_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_12_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_14_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_16_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_17_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_18_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_20_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_24_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_25_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_28_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_31_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_34_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_4_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_6_x top.ni_0.logic_longitudinal.ni_9.longitudinal.__longitudinal_8_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_14._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.__fall_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_15.fall.ni_80._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_16._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_17._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_18.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_19.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_20.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_21.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.__PseudoContinuous_2_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_22.PseudoContinuous.ni_32._arrow._first_x top.ni_0.logic_longitudinal.ni_9.longitudinal.ni_23._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.__LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc_2_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_29.LONGITUDINAL_CONTROLLER_PitchInnerLoop.ni_75.LONGITUDINAL_CONTROLLER_PitchInnerLoop_TransferFunc.ni_82._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.__LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_30.LONGITUDINAL_CONTROLLER_FPAControl.ni_76.LONGITUDINAL_CONTROLLER_FPAControl_integrator_reset.ni_83._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_4_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_5_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.__LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit_6_x top.ni_1.Mode_plus_Longitudinal.ni_11.LONGITUDINAL_CONTROLLER.ni_31.LONGITUDINAL_CONTROLLER_AltitudeControl.ni_77.LONGITUDINAL_CONTROLLER_AltitudeControl_VariableRateLimit.ni_84._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.__fall_2_x top.ni_1.Mode_plus_Longitudinal.ni_12.fall.ni_80._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.__MODE_LOGIC_HeadingMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_26.MODE_LOGIC_HeadingMode.ni_73._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.__MODE_LOGIC_SpeedMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_27.MODE_LOGIC_SpeedMode.ni_72._arrow._first_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_2_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_3_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_4_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.__MODE_LOGIC_AltAndFPAMode_5_x top.ni_1.Mode_plus_Longitudinal.ni_13.MODE_LOGIC.ni_28.MODE_LOGIC_AltAndFPAMode.ni_74._arrow._first_x top.OK1 top.OK2 top.OK3 top.OK4 top.OK5 top.OK6 top.OK7 top.OK8 top.OK9 top.OK10 top.OK11 top.OK12 top.OK13 top.OK14 top.OK15))
  ERR))
(query ERR)
